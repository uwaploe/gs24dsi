// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/billion/main.h $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi_dsl.h"
#include "24dsi_utils.h"



// prototypes *****************************************************************

int	billion_read(int fd);



#endif
