// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/billion/billion.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// #defines *******************************************************************

#define	_1B		(1000L * 1000L * 1000L)
#define	_1M		(1024L * 1024L)



// variables *******************************************************************

static	u32	_buffer[_1M];



/******************************************************************************
*
*	Function:	_read_data
*
*	Purpose:
*
*		Read a billion samples.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _read_data(int fd)
{
	int		errs	= 0;
	long	get;
	long	got;
	long	samples	= 0;

	gsc_label("Reading");

	for (;;)
	{
		get	= _1B - samples;

		if (get > (sizeof(_buffer) / 4))
			get	= sizeof(_buffer) / 4;

		got	= dsi_dsl_read(fd, _buffer, get);

		if (got < 0)
		{
			errs	= 1;
			printf(	"FAIL <---  (read error, errno %d)\n", errno);
			break;
		}

		if (got != get)
		{
			errs	= 1;
			printf(	"FAIL <---  (total %ld, requested %ld samples, got %ld samples)\n",
					(long) samples,
					(long) get,
					(long) got);
			break;
		}

		samples	+= got;

		if (samples >= _1B)
			break;
	}

	if (errs == 0)
		printf("PASS  (read %ld samples)\n", samples);

	return(errs);
}



/******************************************************************************
*
*	Function:	billion_read
*
*	Purpose:
*
*		Configure the board, then capture data to a file.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int billion_read(int fd)
{
	int	errs	= 0;

	errs	+= dsi_config_ai(fd, -1, 1, -1, 200000);
	errs	+= _read_data(fd);

	return(errs);
}



