// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/id/id.c $
// $Rev: 32513 $
// $Date: 2015-07-04 15:22:22 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "main.h"



//*****************************************************************************
static void _local_label_string(const char* str)
{
	int	len;

	printf("%s", str);
	len	= 7 - (int) strlen(str);

	for (; len > 0; len--)
		printf(" ");
}



//*****************************************************************************
static void _local_label_long(long l)
{
	char	buf[32];

	sprintf(buf, "%ld", l);
	_local_label_string(buf);
}



//*****************************************************************************
static int _yes_no_support(int fd, const char* title, s32 id)
{
	int	errs;
	s32	supported;

	gsc_label(title);
	errs	= dsi_query(fd, -1, 0, id, &supported);

	if (errs)
	{
	}
	else if (supported < 0)
	{
		errs	= 1;
		printf("FAIL <---  (query response error: %ld)\n", (long) supported);
	}
	else
	{
		printf("%s\n", supported ? "Yes" : "No");
	}

	return(errs);
}



//*****************************************************************************
static int _voltage_ranges(int fd)
{
	u32	bcfgr;
	int	errs		= 0;
	s32	low_power;
	s32	range;		//
	s32	range_2;	// BCTRL D2-D3
	s32	range_17;	// BCFGR D17-D18

	gsc_label("Voltage Ranges");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_LOW_POWER, &low_power);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D2_3_RANGE, &range_2);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D17_18_RANGE, &range_17);
	errs	+= dsi_reg_read(fd, DSI_GSC_BCFGR, &bcfgr);

	if (errs)
	{
	}
	else if (range_2)
	{
		// The voltage range is configurable.

		if (low_power)
			printf("+-5V, +-2.5V\n");
		else
			printf("+-10V, +-5V, +-2.5V\n");
	}
	else if (range_17)
	{
		// The voltage range is fixed.
		range	= GSC_FIELD_DECODE(bcfgr, 18, 17);

		switch (range)
		{
			default:

				errs	= 1;
				printf("ERROR: %s, line %d\n",__FILE__, __LINE__);
				break;

			case 2:	printf("+-2.5V\n");	break;
			case 1:	printf("+-5V\n");	break;
			case 0:	printf("+-10V\n");	break;
		}
	}
	else
	{
		errs++;
		printf(	"FAIL <---  (Internal error: %s, line %d)\n",
				__FILE__,
				__LINE__);
	}

	return(errs);
}



//*****************************************************************************
static int _input_modes(int fd)
{
	s32	config;
	int	errs	= 0;

	gsc_label("Input Modes");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D0_1_IN_MODE, &config);

	if (errs)
		;
	else if (config)
		printf("Differential, Zero Test, +Vref Test\n");
	else
		printf("Differential\n");

	return(errs);
}



//*****************************************************************************
static int _channel_qty(int fd)
{
	s32	channel_qty;
	int	errs	= 0;

	gsc_label("A/D Channels");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_QTY, &channel_qty);

	if (errs)
	{
	}
	else if (channel_qty <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. qty response error: %ld)\n",
				__LINE__,
				(long) channel_qty);
	}
	else
	{
		_local_label_long(channel_qty);
		printf("(24/20/18/16-bit resolution)\n");
	}

	return(errs);
}



//*****************************************************************************
static int _channel_groups(int fd)
{
	s32	chans;
	int	errs	= 0;
	s32	groups;

	gsc_label("Channel Groups");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_QTY, &chans);

	if (errs)
	{
	}
	else if (groups <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. groups response error: %ld)\n",
				__LINE__,
				(long) groups);
	}
	else if (chans <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. chans response error: %ld)\n",
				__LINE__,
				(long) chans);
	}
	else
	{
		_local_label_long(groups);
		printf("(%ld channels per group)\n", (long) chans / groups);
	}

	return(errs);
}



//*****************************************************************************
static int _rate_generators(int fd)
{
	int	errs	= 0;
	s32	rate_gens;

	gsc_label("Rate Generators");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &rate_gens);

	if (errs)
	{
	}
	else if (rate_gens <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. qty response error: %ld)\n",
				__LINE__,
				(long) rate_gens);
	}
	else if (rate_gens > 1)
	{
		_local_label_long(rate_gens);
		printf("(A-%c)\n", (int) 'A' + rate_gens - 1);
	}
	else
	{
		_local_label_long(rate_gens);
		printf("(internal rate generator, or A)\n");
	}

	return(errs);
}



//*****************************************************************************
static int _frequency_control(int fd)
{
	int	errs;
	s32	pll;

	gsc_label("Frequency Control");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);

	if (errs)
	{
	}
	else if (pll < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. pll response error: %ld)\n",
				__LINE__,
				(long) pll);
	}
	else if (pll)
	{
		printf("PLL Support\n");
	}
	else
	{
		printf("Legacy Support\n");
	}

	return(errs);
}



//*****************************************************************************
static int _fgen_range(int fd)
{
	int	errs	= 0;
	s32	max;
	s32	min;

	gsc_label("Fgen Range");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FGEN_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FGEN_MIN, &min);

	if (errs)
	{
	}
	else if (max <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf(" Hz\n");
	}

	return(errs);
}



//*****************************************************************************
static int _nrate_range(int fd)
{
	int	errs	= 0;
	s32	mask;
	s32	max;
	s32	min;
	s32	pll;

	gsc_label("Nrate Range");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MASK, &mask);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NRATE_MIN, &min);

	if (errs)
	{
	}
	else if (pll < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. pll response error: %ld)\n",
				__LINE__,
				(long) pll);
	}
	else if (pll)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (mask < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask response error: %ld)\n",
				__LINE__,
				(long) mask);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else if (max > mask)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask range error: mask 0x%lX, max %ld)\n",
				__LINE__,
				(long) mask,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("  (mask 0x%lX)\n", (long) mask);
	}

	return(errs);
}



//*****************************************************************************
static int _nvco_range(int fd)
{
	int	errs	= 0;
	s32	mask;
	s32	max;
	s32	min;
	s32	pll;

	gsc_label("Nvco Range");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MASK, &mask);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NVCO_MIN, &min);

	if (errs)
	{
	}
	else if (pll < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. pll response error: %ld)\n",
				__LINE__,
				(long) pll);
	}
	else if (pll == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (mask < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask response error: %ld)\n",
				__LINE__,
				(long) mask);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else if (max > mask)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask range error: mask 0x%lX, max %ld)\n",
				__LINE__,
				(long) mask,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("  (mask 0x%lX)\n", (long) mask);
	}

	return(errs);
}



//*****************************************************************************
static int _nref_range(int fd)
{
	int	errs	= 0;
	s32	mask;
	s32	max;
	s32	min;
	s32	pll;

	gsc_label("Nref Range");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MASK, &mask);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &min);

	if (errs)
	{
	}
	else if (pll < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. pll response error: %ld)\n",
				__LINE__,
				(long) pll);
	}
	else if (pll == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (mask < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask response error: %ld)\n",
				__LINE__,
				(long) mask);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else if (max > mask)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask range error: mask 0x%lX, max %ld)\n",
				__LINE__,
				(long) mask,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("  (mask 0x%lX)\n", (long) mask);
	}

	return(errs);
}



//*****************************************************************************
static int _ndiv_range(int fd)
{
	int	errs	= 0;
	s32	mask;
	s32	max;
	s32	min;

	gsc_label("Ndiv Range");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MASK, &mask);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (mask < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask response error: %ld)\n",
				__LINE__,
				(long) mask);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else if (max > mask)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. mask range error: mask 0x%lX, max %ld)\n",
				__LINE__,
				(long) mask,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf("  (mask 0x%lX)\n", (long) mask);
	}

	return(errs);
}



//*****************************************************************************
static int _fsamp_range(int fd)
{
	int	errs	= 0;
	s32	max;			// Sample Rate Maximum
	s32	min;			// Sample Rate Minimum

	gsc_label("Sample Rates");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FSAMP_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FSAMP_MIN, &min);

	if (errs)
	{
	}
	else if (max < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. max response error: %ld)\n",
				__LINE__,
				(long) max);
	}
	else if (min < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. min response error: %ld)\n",
				__LINE__,
				(long) min);
	}
	else if (min >= max)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. range error: %ld-%ld)\n",
				__LINE__,
				(long) min,
				(long) max);
	}
	else
	{
		gsc_label_long_comma(min);
		printf(" to ");
		gsc_label_long_comma(max);
		printf(" S/S/C\n");
	}

	return(errs);
}



//*****************************************************************************
static int _fifo_size(int fd)
{
	int	errs	= 0;
	s32	size;			// FIFO Size in samples

	gsc_label("FIFO Size");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_FIFO_SIZE, &size);

	if (errs)
	{
	}
	else if (size <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. size response error: %ld)\n",
				__LINE__,
				(long) size);
	}
	else
	{
		gsc_label_long_comma(size);
		printf(" Samples Deep");

		if ((size % 1024) == 0)
			printf("  (%ldK)", (long) size / 1024);

		printf("\n");
	}

	return(errs);
}



//*****************************************************************************
static int _initialize(int fd)
{
	int	errs	= 0;
	s32	ms;

	gsc_label("Initialize Period");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_INIT_MS, &ms);

	if (errs)
	{
	}
	else if (ms <= 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. ms response error: %ld)\n",
				__LINE__,
				(long) ms);
	}
	else
	{
		gsc_label_long_comma(ms);
		printf(" ms maximum\n");
	}

	return(errs);
}



//*****************************************************************************
static int _auto_calibrate(int fd)
{
	int	errs	= 0;
	s32	ms;

	gsc_label("Auto-Calibrate Period");

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_AUTO_CAL_MS, &ms);

	if (errs)
	{
	}
	else if (ms < 0)
	{
		errs	= 1;
		printf(	"FAIL <---  (%d. ms response error: %ld)\n",
				__LINE__,
				(long) ms);
	}
	else if (ms == 0)
	{
		printf("Auto-Calibration not supported\n");
	}
	else
	{
		gsc_label_long_comma(ms);
		printf(" ms maximum\n");
	}

	return(errs);
}



//*****************************************************************************
static int _id_board_bcfgr(int fd)
{
	int				errs	= 0;
	gsc_reg_def_t	list[2];
	gsc_reg_def_t*	ptr;

	memset(list, 0, sizeof(list));
	printf("\n");

	ptr	= (gsc_reg_def_t*) dsi_reg_get_def_id(DSI_GSC_BCFGR);

	if (ptr)
	{
		list[0]	= ptr[0];
		errs	= gsc_reg_list(fd, list, 1, dsi_reg_read);
	}
	else
	{
		errs++;
		printf(	"FAIL <---  (%d. dsi_reg_get_def_id, DSI_GSC_BCFGR)\n",
				__LINE__);
	}

	return(errs);
}



//*****************************************************************************
static int _register_map(int fd)
{
	int	errs;

	printf("\n");
	errs	= dsi_reg_list(fd, 0);
	return(errs);
}



/******************************************************************************
*
*	Function:	id_device
*
*		Identify the device and its features.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int id_device(int fd)
{
	int	errs	= 0;

	gsc_label("Board Features");
	printf("\n");

	gsc_label_level_inc();

	errs	+= _input_modes(fd);
	errs	+= _voltage_ranges(fd);
	errs	+= _channel_qty(fd);
	errs	+= _channel_groups(fd);
	errs	+= _rate_generators(fd);
	errs	+= _frequency_control(fd);
	errs	+= _fgen_range(fd);
	errs	+= _nrate_range(fd);
	errs	+= _nvco_range(fd);
	errs	+= _nref_range(fd);
	errs	+= _ndiv_range(fd);
	errs	+= _fsamp_range(fd);
	errs	+= _fifo_size(fd);
	errs	+= _initialize(fd);
	errs	+= _auto_calibrate(fd);

	errs	+= _yes_no_support(fd, "GPS Support",				DSI_QUERY_GPS_PRESENT);
	errs	+= _yes_no_support(fd, "Low Noise Board",			DSI_QUERY_LOW_NOISE);
	errs	+= _yes_no_support(fd, "Low Power Board",			DSI_QUERY_LOW_POWER);
	errs	+= _yes_no_support(fd, "RCAR & RCBR Present",		DSI_QUERY_RCAR_RCBR);
	errs	+= _yes_no_support(fd, "RFCR Present",				DSI_QUERY_RFCR);
	errs	+= _yes_no_support(fd, "ICMR Present",				DSI_QUERY_REG_ICMR);
	errs	+= _yes_no_support(fd, "Arm Ext. Trig. Support",	DSI_QUERY_D21_EXT_TRIG);
	errs	+= _yes_no_support(fd, "Thresh. Flag Out Support",	DSI_QUERY_D22_THR_FLAG);
	errs	+= _yes_no_support(fd, "Ch Grp 0 Primary in RAR",	DSI_QUERY_CH_GRP_0_RAR);
	errs	+= _yes_no_support(fd, "Invert External Trigger",	DSI_QUERY_D23_INV_EXT_TRIG);

	gsc_label_level_dec();

	errs	+= _id_board_bcfgr(fd);
	errs	+= _register_map(fd);

	return(errs);
}



