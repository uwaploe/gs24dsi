// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/docsrc/24dsi_dsl.h $
// $Rev: 34273 $
// $Date: 2016-01-08 18:20:39 -0600 (Fri, 08 Jan 2016) $

#ifndef __24DSI_DSL_H__
#define __24DSI_DSL_H__

#include "24dsi.h"



// prototypes *****************************************************************

int	dsi_dsl_close(int fd);
int	dsi_dsl_ioctl(int fd, int request, void *arg);
int	dsi_dsl_open(unsigned int board);
int	dsi_dsl_read(int fd, u32* buf, size_t samples);



#endif
