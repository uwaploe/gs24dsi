// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/fsamp/main.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// variables *******************************************************************

static	int	_def_index	= 0;
static	s32	_fref		= -1;
static	s32	_fsamp		= -1;
static	s32	_options	= 0;



//*****************************************************************************
static int _parse_args(int argc, char** argv)
{
	char	buf[64];
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;
	long	l;

	printf("fsamp - Compute Ndiv and Nrate for a desired sample rate\n");
	printf("USAGE: fsamp <-o#> <-r#> <-s#> <index>\n");
	printf("  -o#    Display all compute options within this range of the sample rate.\n");
	printf("  -r#    The reference frequency, Fref, if other than the default.\n");
	printf("  -s#    The desired sample rate, Fsamp, if other than the default.\n");
	printf("  index  The zero based index of the device to access.\n");

	gsc_label("Arguments");
	printf("(%d argument%s)\n", argc - 1, ((argc - 1) > 1) ? "s" : "");
	gsc_label_level_inc();

	for (i = 0; i < argc; i++)
	{
		sprintf(buf, "Argument %d", i);
		gsc_label(buf);
		printf("%s\n", argv[i]);
	}

	gsc_label_level_dec();

	for (i = 1; i < argc; i++)
	{
		if ((argv[i][0] == '-') && (argv[i][1] == 'o') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%ld%c", &l, &c);

			if ((j == 1) && (l > 0))
			{
				_options	= l;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'r') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_fref	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 's') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%ld%c", &l, &c);

			if ((j == 1) && (l > 0))
			{
				_fsamp	= l;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		j	= sscanf(&argv[i][0], "%d%c", &k, &c);

		if ((j == 1) && (k >= 0))
		{
			_def_index	= k;
			continue;
		}

		errs	= 1;
		printf("ERROR: invalid argument: %s\n", argv[i]);
		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_perform_tests
*
*	Purpose:
*
*		Perform the appropriate testing.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _perform_tests(int fd)
{
	int			errs	= 0;
	const char*	psz;
	struct tm*	stm;
	time_t		tt;

	time(&tt);
	stm	= localtime(&tt);
	psz	= asctime(stm);
	gsc_label("Performing Operation");
	printf("%s", psz);

	errs	+= os_id_driver(DSI_BASE_NAME);
	errs	+= dsi_id_board(fd, -1, NULL);

	errs	+= fsamp_compute(fd, _fref, _fsamp, _options);

	return(errs);
}



/******************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
******************************************************************************/

int main(int argc, char *argv[])
{
	int		errs;
	int		fd			= 0;
	long	hours;
	long	mins;
	time_t	now;
	int		qty;
	int		ret			= EXIT_FAILURE;
	long	secs;
	time_t	test;

	for (;;)
	{
		gsc_label_init(24);
		test	= time(NULL);
		errs	= _parse_args(argc, argv);

		if (errs)
			break;

		os_id_host();
		qty	= dsi_count_boards();

		if (qty <= 0)
			break;

		gsc_label("Accessing Board Index");
		printf("%d\n", _def_index);
		fd	= gsc_dev_open(_def_index, DSI_BASE_NAME);

		if (fd == -1)
		{
			errs	= 1;
			printf(	"ERROR: Unable to access device %d.", _def_index);
		}

		if (errs == 0)
		{
			ret		= EXIT_SUCCESS;
			errs	= _perform_tests(fd);
		}

		gsc_dev_close(_def_index, fd);

		now	= time(NULL);

		if (errs)
		{
			printf(	"\nRESULTS: FAIL <---  (%d error%s)",
					errs,
					(errs == 1) ? "" : "s");
		}
		else
		{
			printf("\nRESULTS: PASS");
		}

		secs	= now - test;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(" (duration %ld:%ld:%02ld)\n", hours, mins, secs);
		break;
	}

	return(ret);
}



