// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_irq.c $
// $Rev: 32673 $
// $Date: 2015-07-09 15:56:43 -0500 (Thu, 09 Jul 2015) $

// Linux driver module

#include "main.h"



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
int os_irq_acquire(dev_data_t* dev)
{
	int	ret	= 0;

	if (dev->irq.irq.allotted == 0)
	{
		ret	= IRQ_REQUEST(dev, os_irq_isr);
		dev->irq.irq.allotted	= (ret == 0) ? 1 : 0;
	}

	return(ret);
}
#endif



//*****************************************************************************
#ifndef GSC_IRQ_NOT_USED
void os_irq_release(dev_data_t* dev)
{
	if (dev->irq.irq.allotted)
	{
		dev->irq.irq.allotted	= 0;
		free_irq(dev->pci->irq, dev);
	}
}
#endif


