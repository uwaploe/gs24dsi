// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_pci.c $
// $Rev: 31781 $
// $Date: 2015-06-09 10:27:08 -0500 (Tue, 09 Jun 2015) $

// Linux driver module

#include "main.h"



//*****************************************************************************
void os_pci_clear_master(os_pci_t* pci)
{
	PCI_CLEAR_MASTER(pci);
}



//*****************************************************************************
int os_pci_dev_enable(os_pci_t* pci)
{
	int	ret;

	ret	= PCI_ENABLE_DEVICE(pci);
	return(ret);
}



//*****************************************************************************
void os_pci_dev_disable(os_pci_t* pci)
{
	PCI_DISABLE_DEVICE(pci);
}



//*****************************************************************************
int os_pci_set_master(os_pci_t* pci)
{
	pci_set_master(pci);
	return(0);
}


