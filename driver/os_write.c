// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_write.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

// Linux driver module

#include "main.h"



//*****************************************************************************
ssize_t os_write(
	struct file*	filp,
	const char*		buf,
	size_t			count,
	loff_t*			offp)
{
#if defined(DEV_SUPPORTS_WRITE)

	GSC_ALT_STRUCT_T*	alt;
	ssize_t				ret;

	// Access the device structure.
	alt	= (GSC_ALT_STRUCT_T*) filp->private_data;

	if (alt)
	{
		// Is this blocking or non-blocking I/O.
		alt->tx.non_blocking	= (filp->f_flags & O_NONBLOCK) ? 1 : 0;

		// Call the common code.
		ret	= gsc_write(alt, buf, count);
	}
	else
	{
		// The referenced device doesn't exist.
		ret	= -ENODEV;
	}

	return(ret);
#else
    return (-EPERM);
#endif
}


