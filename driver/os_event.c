// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_event.c $
// $Rev: 33964 $
// $Date: 2015-11-05 18:21:33 -0600 (Thu, 05 Nov 2015) $

// Linux driver module

#include "main.h"



//*****************************************************************************
void os_event_create(os_event_t* evnt)
{
	evnt->condition	= 0;
	memset(&evnt->entry, 0, sizeof(evnt->entry));
	WAIT_QUEUE_HEAD_INIT(&evnt->queue);
	WAIT_QUEUE_ENTRY_INIT(&evnt->entry, current);
	SET_CURRENT_STATE(TASK_INTERRUPTIBLE);
	add_wait_queue(&evnt->queue, &evnt->entry);
}



//*****************************************************************************
void os_event_destroy(os_event_t* evnt)
{
	remove_wait_queue(&evnt->queue, &evnt->entry);
	SET_CURRENT_STATE(TASK_RUNNING);
}



//*****************************************************************************
void os_event_resume(os_event_t* evnt)
{
	EVENT_RESUME_IRQ(&evnt->queue, evnt->condition);
}



//*****************************************************************************
void os_event_wait(os_event_t* evnt, os_time_tick_t timeout)
{
	EVENT_WAIT_IRQ_TO(&evnt->queue, evnt->condition, timeout);
}


