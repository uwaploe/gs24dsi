// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_close.c $
// $Rev: 31781 $
// $Date: 2015-06-09 10:27:08 -0500 (Tue, 09 Jun 2015) $

// Linux driver module

#include "main.h"



//*****************************************************************************
int os_close(struct inode *inode, struct file *fp)
{
	GSC_ALT_STRUCT_T*	alt;
	int					ret;

	for (;;)	// A convenience loop.
	{
		if (fp == NULL)
		{
			// This should never happen.
			ret	= -ENODEV;
			break;
		}

		alt	= (GSC_ALT_STRUCT_T*) fp->private_data;

		if (alt == NULL)
		{
			// The referenced device doesn't exist.
			ret	= -ENODEV;
			break;
		}

		ret	= gsc_close(alt);
		fp->private_data	= NULL;
		os_module_count_dec();
		break;
	}

	return(ret);
}


