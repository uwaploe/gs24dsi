// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_kernel_2_2.c $
// $Rev: 33912 $
// $Date: 2015-10-07 14:18:46 -0500 (Wed, 07 Oct 2015) $

//	Provide special handling for things specific to the 2.2 kernel.

#include <linux/version.h>



#if	(LINUX_VERSION_CODE >= KERNEL_VERSION(2,2,0)) && \
	(LINUX_VERSION_CODE <  KERNEL_VERSION(2,3,0))

#define	MODULE
#include "main.h"



// Tags ***********************************************************************

EXPORT_NO_SYMBOLS;
MODULE_AUTHOR("General Standards Corporation, www.generalstandards.com");
MODULE_DESCRIPTION(DEV_MODEL " driver " GSC_DRIVER_VERSION);
MODULE_SUPPORTED_DEVICE(DEV_MODEL);



/******************************************************************************
*
*	Function:	os_bar_size
*
*	Purpose:
*
*		Get the size of a BAR region.
*
*	Arguments:
*
*		dev		The data structure for our device.
*
*		bar		The index of the BAR region.
*
*	Returned:
*
*		The size of the region in bytes.
*
******************************************************************************/

u32 os_bar_size(dev_data_t* dev, u8 bar)
{
	int	offset	= 0x10 + (4 * bar);
	u32	reg;
	u32	size;

	reg	= os_reg_pci_rx_u32(dev, dev->pci, offset);
	// Use a PCI feature to get the address mask.
	os_reg_pci_tx_u32(dev, dev->pci, offset, 0xFFFFFFFF);
	size	= os_reg_pci_rx_u32(dev, dev->pci, offset);
	// Restore the register.
	os_reg_pci_tx_u32(dev, dev->pci, offset, reg);
	size	= 1 + ~(size & 0xFFFFFFF0);

	return(size);
}



/******************************************************************************
*
*	Function:	os_module_count_dec
*
*	Purpose:
*
*		Decrement the module usage count, as appropriate for the kernel.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void os_module_count_dec(void)
{
	MOD_DEC_USE_COUNT;
}



/******************************************************************************
*
*	Function:	os_module_count_inc
*
*	Purpose:
*
*		Increment the module usage count, as appropriate for the kernel.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		0		All went well.
*		< 0		An appropriate error status.
*
******************************************************************************/

int os_module_count_inc(void)
{
	MOD_INC_USE_COUNT;
	return(0);
}



/******************************************************************************
*
*	Function:	os_irq_isr
*
*	Purpose:
*
*		Service an interrupt.
*
*	Arguments:
*
*		irq		The interrupt number.
*
*		dev_id	The private data we've associated with the IRQ.
*
*		regs	Unused.
*
*	Returned:
*
*		None.
*
******************************************************************************/

#ifndef GSC_IRQ_NOT_USED
void os_irq_isr(int irq, void* dev_id, struct pt_regs* regs)
{
	gsc_irq_isr_common(dev_id, GSC_IRQ_ISR_FLAG_LOCK);
}
#endif



/******************************************************************************
*
*	Function:	_proc_get_info
*
*	Purpose:
*
*		Implement the get_info() service for /proc file system support.
*
*	Arguments:
*
*		page	The data produced is put here.
*
*		start	Records pointer to where the data in "page" begins.
*
*		offset	The offset into the file where we're to begin.
*
*		count	The limit to the amount of data we can produce.
*
*		dummy	A parameter that is unused.
*
*	Returned:
*
*		int		The number of characters written.
*
******************************************************************************/

static int _proc_get_info(
	char*	page,
	char**	start,
	off_t	offset,
	int		count,
	int		dummy)
{
	int	eof;
	int	i;

	i	= os_proc_read(page, start, offset, count, &eof, NULL);
	return(i);
}



/******************************************************************************
*
*	Function:	os_proc_start_detail
*
*	Purpose:
*
*		Initialize use of the /proc file system.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		0		All went well.
*		< 0		An appropriate error status.
*
******************************************************************************/

int os_proc_start_detail(void)
{
	struct proc_dir_entry*	proc;
	int						ret;

	proc	= create_proc_entry(DEV_NAME, S_IRUGO, NULL);

	if (proc)
	{
		ret						= 0;
		gsc_global.proc_enabled	= 1;
		proc->read_proc			= os_proc_read;
		proc->get_info			= _proc_get_info;
	}
	else
	{
		ret	= -ENODEV;
		printk(	"%s: os_proc_start_detail:"
				" create_proc_entry() failure.\n",
				DEV_NAME);
	}

	return(ret);
}



#endif
