// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/driver/close.c $
// $Rev: 32696 $
// $Date: 2015-07-09 17:01:36 -0500 (Thu, 09 Jul 2015) $

#include "main.h"



//*****************************************************************************
int dev_close(dev_data_t* dev)
{
	initialize_ioctl(dev, NULL);
	gsc_wait_close(dev);
	dev_io_close(dev);
	gsc_dma_close(dev, 0);
	gsc_irq_close(dev, 0);
	return(0);
}



