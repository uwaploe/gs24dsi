// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/driver/read.c $
// $Rev: 34709 $
// $Date: 2016-03-18 18:28:58 -0500 (Fri, 18 Mar 2016) $

#include "main.h"



/******************************************************************************
*
*	Function:	dev_read_startup
*
*	Purpose:
*
*		Perform any work or tests needed before initiating a read operation.
*
*	Arguments:
*
*		alt		The device data structure.
*
*	Returned:
*
*		0		All is well.
*		< 0		There was an error.
*
******************************************************************************/

long dev_read_startup(GSC_ALT_STRUCT_T* alt)
{
	dev_data_t* dev		= GSC_ALT_DEV_GET(alt);
	u32			ibcr;
	long		ret		= 0;

	if ((dev->rx.overflow_check) || (dev->rx.underflow_check))
	{
		ibcr	= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_ibcr_32);

		if ((dev->rx.overflow_check) && (ibcr & 0x1000000))
			ret	= -EIO;

		if ((dev->rx.underflow_check) && (ibcr & 0x2000000))
			ret	= -EIO;
	}

	return(ret);
}



/******************************************************************************
*
*	Function:	pio_read_available
*
*	Purpose:
*
*		Check to see how many bytes are available for reading.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		count	What remains of the number of bytes that the application
*				requested.
*
*	Returned:
*
*		>= 0	The number of bytes available for reading.
*		< 0		There was an error.
*
******************************************************************************/

long pio_read_available(dev_data_t* dev, size_t count)
{
	u32	bsr;

	bsr		= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bsr_32);
	count	= 4 * bsr;
	return(count);
}



/******************************************************************************
*
*	Function:	dma_read_available
*
*	Purpose:
*
*		Check to see how many bytes are available for reading.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		count	What remains of the number of bytes that the application
*				requested.
*
*	Returned:
*
*		>= 0	The number of bytes available for reading.
*		< 0		There was an error.
*
******************************************************************************/

long dma_read_available(dev_data_t* dev, size_t count)
{
	u32	available;
	u32	bsr;
	u32	ibcr;
	u32	threshold;

	bsr			= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_bsr_32);
	available	= bsr * 4;

	if (available >= count)
	{
		// The desired amount is available.
	}
	else
	{
		ibcr		= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_ibcr_32);
		threshold	= (ibcr & 0x3FFFF) * 4;

		if (available >= threshold)
			count	= available;
		else
			count	= 0;
	}

	return(count);
}



/******************************************************************************
*
*	Function:	dma_read_work
*
*	Purpose:
*
*		Perform a read of the specified number of bytes.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		mem		Put the data here.
*
*		count	What remains of the number of bytes that the application
*				requested.
*
*		st_end	The system timer count at which we timeout. If this is zero,
*				then we ignore this.
*
*	Returned:
*
*		>= 0	The number of bytes read.
*		< 0		There was an error.
*
******************************************************************************/

long dma_read_work(
	dev_data_t*		dev,
	const os_mem_t*	mem,
	size_t			count,
	os_time_tick_t	st_end)
{
	u32		dpr;
	u32		mode;
	long	qty;
	long	samples	= count / 4;

	if (samples < dev->rx.pio_threshold)
	{
		qty	= gsc_read_pio_work_32_bit(dev, mem, count, st_end);
	}
	else
	{
		mode	= GSC_DMA_MODE_BLOCK_DMA
				| GSC_DMA_MODE_SIZE_32_BITS
				| GSC_DMA_MODE_INPUT_ENABLE
				| GSC_DMA_MODE_BURSTING_LOCAL
				| GSC_DMA_MODE_INTERRUPT_WHEN_DONE
				| GSC_DMA_MODE_LOCAL_ADRESS_CONSTANT
				| GSC_DMA_MODE_PCI_INTERRUPT_ENABLE;

		dpr		= GSC_DMA_DPR_BOARD_TO_HOST
				| GSC_DMA_DPR_END_OF_CHAIN
				| GSC_DMA_DPR_TERMINAL_COUNT_IRQ;

		qty		= gsc_dma_perform(	dev,
									&dev->rx,
									st_end,
									GSC_DMA_CAP_DMA_READ,
									mode,
									dpr,
									mem,
									count);
	}

	return(qty);
}



/******************************************************************************
*
*	Function:	dmdma_read_available
*
*	Purpose:
*
*		Check to see how many bytes are available for reading.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		count	What remains of the number of bytes that the application
*				requested.
*
*	Returned:
*
*		>= 0	The number of bytes available for reading.
*		< 0		There was an error.
*
******************************************************************************/

long dmdma_read_available(dev_data_t* dev, size_t count)
{
	u32	ibcr;
	u32	threshold;

	ibcr		= os_reg_mem_rx_u32(dev, dev->vaddr.gsc_ibcr_32);
	threshold	= ibcr & 0x3FFFF;
	count		= (threshold + 1) * 4;
	return(count);
}



/******************************************************************************
*
*	Function:	dmdma_read_work
*
*	Purpose:
*
*		Perform a read of the specified number of bytes.
*
*	Arguments:
*
*		dev		The device data structure.
*
*		mem		Put the data here.
*
*		count	What remains of the number of bytes that the application
*				requested.
*
*		st_end	The system timer count at which we timeout. If this is zero,
*				then we ignore this.
*
*	Returned:
*
*		>= 0	The number of bytes read.
*		< 0		There was an error.
*
******************************************************************************/

long dmdma_read_work(
	dev_data_t*		dev,
	const os_mem_t*	mem,
	size_t			count,
	os_time_tick_t	st_end)
{
	u32		dpr;
	u32		mode;
	long	qty;
	long	samples	= count / 4;

	if (samples < dev->rx.pio_threshold)
	{
		qty	= gsc_read_pio_work_32_bit(dev, mem, count, st_end);
	}
	else
	{
		mode	= GSC_DMA_MODE_DM_DMA
				| GSC_DMA_MODE_SIZE_32_BITS
				| GSC_DMA_MODE_INPUT_ENABLE
				| GSC_DMA_MODE_BURSTING_LOCAL
				| GSC_DMA_MODE_INTERRUPT_WHEN_DONE
				| GSC_DMA_MODE_LOCAL_ADRESS_CONSTANT
				| GSC_DMA_MODE_PCI_INTERRUPT_ENABLE;

		dpr		= GSC_DMA_DPR_BOARD_TO_HOST
				| GSC_DMA_DPR_END_OF_CHAIN
				| GSC_DMA_DPR_TERMINAL_COUNT_IRQ;

		qty		= gsc_dma_perform(	dev,
									&dev->rx,
									st_end,
									GSC_DMA_CAP_DMDMA_READ,
									mode,
									dpr,
									mem,
									count);
	}

	return(qty);
}


