// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_bar.c $
// $Rev: 33964 $
// $Date: 2015-11-05 18:21:33 -0600 (Thu, 05 Nov 2015) $

// Linux driver module

#include "main.h"



//*****************************************************************************
static int _bar_io_acquire(os_bar_t* bar)
{
	int		ret;
	void*	vp;

	for (;;)	// A convenience loop.
	{
		ret	= REGION_IO_CHECK(bar->phys_adrs, bar->size);

		if (ret)
		{
			// This BAR region is already in use.
			ret	= -EALREADY;
			break;
		}

		vp	= REGION_IO_REQUEST(bar->phys_adrs, bar->size, DEV_NAME);

		if (vp == NULL)
		{
			// This BAR region request failed.
			ret	= -EFAULT;
			break;
		}

		// All went well.
		bar->requested	= 1;
		bar->vaddr		= (VADDR_T) bar->phys_adrs;
		break;
	}

	return(ret);
}




//*****************************************************************************
static int _bar_mem_acquire(os_bar_t* bar)
{
	int		ret;
	void*	vp;

	for (;;)	// A convenience loop.
	{
		if (bar->flags & D0)
		{
			// The region is not memory mapped.
			ret	= -EINVAL;
			break;
		}

		ret	= REGION_MEM_CHECK(bar->phys_adrs, bar->size);

		if (ret)
		{
			// This BAR region is already in use.
			ret	= -EALREADY;
			break;
		}

		bar->requested	= 1;

		vp	= (void*) REGION_MEM_REQUEST(bar->phys_adrs, bar->size, DEV_NAME);

		if (vp == NULL)
		{
			// This BAR region request failed.
			ret	= EFAULT;
			break;
		}

		// All went well.
		bar->requested	= 1;
		bar->vaddr		= (VADDR_T) ioremap(bar->phys_adrs, bar->size);
		break;
	}

	return(ret);
}



//*****************************************************************************
int os_bar_create(dev_data_t* dev, int index, os_bar_t* bar)
{
	int	ret;

	memset(bar, 0,sizeof(os_bar_t));
	bar->dev		= dev;
	bar->index		= index;
	bar->offset		= 0x10 + 4 * index;
	bar->size		= (u32) os_bar_pci_size(dev, index);
	bar->reg		= os_reg_pci_rx_u32(dev, dev->pci, bar->offset);

	if (bar->reg & D0)
	{
		bar->flags		= bar->reg & 0x3;
		bar->io_mapped	= 1;
		bar->phys_adrs	= bar->reg & 0xFFFFFFFC;
		ret				= _bar_io_acquire(bar);
	}
	else
	{
		bar->flags		= bar->reg & 0xF;
		bar->io_mapped	= 0;
		bar->phys_adrs	= bar->reg & 0xFFFFFFF0;
		ret				= _bar_mem_acquire(bar);
	}

	return(ret);
}



//*****************************************************************************
void os_bar_destroy(os_bar_t* bar)
{
	if (bar->requested == 0)
	{
		// There is nothing to release.
	}
	else if (bar->io_mapped)
	{
		REGION_IO_RELEASE(bar->phys_adrs, bar->size);
	}
	else
	{
		// Memory must be unmapped.

		if (bar->vaddr)
			iounmap((void*) bar->vaddr);

		REGION_MEM_RELEASE(bar->phys_adrs, bar->size);
	}

	memset(bar, 0,sizeof(os_bar_t));
}


