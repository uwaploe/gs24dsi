// This software is covered by the GNU GENERAL PUBLIC LICENSE (GPL).
// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/linux/os_reg.c $
// $Rev: 34149 $
// $Date: 2016-01-08 17:02:04 -0600 (Fri, 08 Jan 2016) $

// Linux driver module

#include "main.h"
#include <linux/pci.h>



//*****************************************************************************
u8 os_reg_mem_mx_u8(dev_data_t* dev, VADDR_T va, u8 value, u8 mask)
{
	u8	reg	= 0;
	u8	set;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		reg	= readb(va);
		set	= (reg & ~mask) | (value & mask);
		writeb(set, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(reg);
}



//*****************************************************************************
u16 os_reg_mem_mx_u16(dev_data_t* dev, VADDR_T va, u16 value, u16 mask)
{
	u16	reg	= 0;
	u16	set;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		reg	= readw(va);
		set	= (reg & ~mask) | (value & mask);
		writew(set, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(reg);
}



//*****************************************************************************
u32 os_reg_mem_mx_u32(dev_data_t* dev, VADDR_T va, u32 value, u32 mask)
{
	u32	reg	= 0;
	u32	set;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		reg	= readl(va);
		set	= (reg & ~mask) | (value & mask);
		writel(set, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(reg);
}



//*****************************************************************************
u8 os_reg_mem_rx_u8(dev_data_t* dev, VADDR_T va)
{
	u8	value	= 0;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		value	= readb(va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(value);
}



//*****************************************************************************
u16 os_reg_mem_rx_u16(dev_data_t* dev, VADDR_T va)
{
	u16	value	= 0;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		value	= readw(va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(value);
}



//*****************************************************************************
u32 os_reg_mem_rx_u32(dev_data_t* dev, VADDR_T va)
{
	u32	value	= 0;

	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		value	= readl(va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}

	return(value);
}



//*****************************************************************************
void os_reg_mem_tx_u8(dev_data_t* dev, VADDR_T va, u8 value)
{
	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		writeb(value, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}
}



//*****************************************************************************
void os_reg_mem_tx_u16(dev_data_t* dev, VADDR_T va, u16 value)
{
	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		writew(value, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}
}



//*****************************************************************************
void os_reg_mem_tx_u32(dev_data_t* dev, VADDR_T va, u32 value)
{
	if (va)
	{
		if (dev)
			gsc_irq_access_lock(dev);

		writel(value, va);

		if (dev)
			gsc_irq_access_unlock(dev);
	}
}



//*****************************************************************************
u8 os_reg_pci_mx_u8(dev_data_t* dev, os_pci_t* pci, u16 offset, u8 value, u8 mask)
{
	u8	reg	= 0;
	u8	set;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
	{
		pci_read_config_byte(pci, offset, &reg);
		set	= (reg & ~mask) | (value & mask);
		pci_write_config_byte(pci, offset, set);
	}

	if (dev)
		gsc_irq_access_unlock(dev);

	return(reg);
}



//*****************************************************************************
u16 os_reg_pci_mx_u16(dev_data_t* dev, os_pci_t* pci, u16 offset, u16 value, u16 mask)
{
	u16	reg	= 0;
	u16	set;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
	{
		pci_read_config_word(pci, offset, &reg);
		set	= (reg & ~mask) | (value & mask);
		pci_write_config_word(pci, offset, set);
	}

	if (dev)
		gsc_irq_access_unlock(dev);

	return(reg);
}



//*****************************************************************************
u32 os_reg_pci_mx_u32(dev_data_t* dev, os_pci_t* pci, u16 offset, u32 value, u32 mask)
{
	u32	reg	= 0;
	u32	set;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
	{
		pci_read_config_dword(pci, offset, &reg);
		set	= (reg & ~mask) | (value & mask);
		pci_write_config_dword(pci, offset, set);
	}

	if (dev)
		gsc_irq_access_unlock(dev);

	return(reg);
}



//*****************************************************************************
u8 os_reg_pci_rx_u8(dev_data_t* dev, os_pci_t* pci, u16 offset)
{
	u8	value	= 0;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_read_config_byte(pci, offset, &value);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u16 os_reg_pci_rx_u16(dev_data_t* dev, os_pci_t* pci, u16 offset)
{
	u16	value	= 0;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_read_config_word(pci, offset, &value);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
u32 os_reg_pci_rx_u32(dev_data_t* dev, os_pci_t* pci, u16 offset)
{
	u32	value	= 0;

	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_read_config_dword(pci, offset, &value);

	if (dev)
		gsc_irq_access_unlock(dev);

	return(value);
}



//*****************************************************************************
void os_reg_pci_tx_u8(dev_data_t* dev, os_pci_t* pci, u16 offset, u8 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_write_config_byte(pci, offset, value);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_tx_u16(dev_data_t* dev, os_pci_t* pci, u16 offset, u16 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_write_config_word(pci, offset, value);

	if (dev)
		gsc_irq_access_unlock(dev);
}



//*****************************************************************************
void os_reg_pci_tx_u32(dev_data_t* dev, os_pci_t* pci, u16 offset, u32 value)
{
	if (dev)
		gsc_irq_access_lock(dev);

	if (pci)
		pci_write_config_dword(pci, offset, value);

	if (dev)
		gsc_irq_access_unlock(dev);
}


