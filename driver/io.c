// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/driver/io.c $
// $Rev: 31802 $
// $Date: 2015-06-09 12:11:52 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



/******************************************************************************
*
*	Function:	dev_io_close
*
*	Purpose:
*
*		Cleanup the I/O stuff for the device as it is being closed.
*
*	Arguments:
*
*		dev	The data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void dev_io_close(dev_data_t* dev)
{
	dev->rx.dma_channel	= NULL;
}



/******************************************************************************
*
*	Function:	dev_io_create
*
*	Purpose:
*
*		Perform I/O based initialization as the driver is being loaded.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		0		All went well.
*		< 0		The error status for the problem encounterred.
*
******************************************************************************/

int dev_io_create(dev_data_t* dev)
{
	int	ret;

	dev->rx.bytes_per_sample	= 4;
	dev->rx.io_reg_offset		= GSC_REG_OFFSET(DSI_GSC_IDBR);
	dev->rx.io_reg_vaddr		= dev->vaddr.gsc_idbr_32;

	ret	= gsc_io_create(dev, &dev->rx, dev->cache.fifo_size * 4);
	return(ret);
}



/******************************************************************************
*
*	Function:	dev_io_destroy
*
*	Purpose:
*
*		Perform I/O based cleanup as the driver is being unloaded.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void dev_io_destroy(dev_data_t* dev)
{
	dev_io_close(dev);	// Just in case.
	gsc_io_destroy(dev, &dev->rx);
}



/******************************************************************************
*
*	Function:	dev_io_open
*
*	Purpose:
*
*		Perform I/O based initialization as the device is being opened.
*
*	Arguments:
*
*		dev		The data for the device of interest.
*
*	Returned:
*
*		0		All went well.
*		< 0		The code for the error seen.
*
******************************************************************************/

int dev_io_open(dev_data_t* dev)
{
	dev_io_close(dev);	// Just in case.

	dev->rx.timeout_s		= DSI_IO_TIMEOUT_DEFAULT;
	dev->rx.pio_threshold	= 32;
	dev->rx.io_mode			= DSI_IO_MODE_DEFAULT;
	dev->rx.overflow_check	= DSI_IO_OVERFLOW_DEFAULT;
	dev->rx.underflow_check	= DSI_IO_UNDERFLOW_DEFAULT;

	return(0);
}


