// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/driver/open.c $
// $Rev: 32696 $
// $Date: 2015-07-09 17:01:36 -0500 (Thu, 09 Jul 2015) $

#include "main.h"



//*****************************************************************************
int dev_open(dev_data_t* dev)
{
	int	ret;

	ret	= dev_io_open(dev);

	if (ret == 0)
		ret	= gsc_dma_open(dev, 0);

	if (ret == 0)
		ret	= gsc_irq_open(dev, 0);

	if (ret == 0)
		ret	= initialize_ioctl(dev, NULL);

	if (ret)
		dev_close(dev);

	return(ret);
}


