// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_bar.c $
// $Rev: 33963 $
// $Date: 2015-11-05 18:16:26 -0600 (Thu, 05 Nov 2015) $

#include "main.h"



/******************************************************************************
*
*	Function:	gsc_bar_create
*
*	Purpose:
*
*		Initialize the given structure according the the BAR index given.
*
*	Arguments:
*
*		dev		The structure for this device.
*
*		index	The BAR index to access.
*
*		bar		The structure to initialize.
*
*		mem		Must this BAR be memory mapped?
*
*		io		Must this BAR be I/O mapped?
*
*	Returned:
*
*		0		All went well.
*		< 0		There was a problem and this is the error status.
*
******************************************************************************/

int gsc_bar_create(dev_data_t* dev, int index, os_bar_t* bar, int mem, int io)
{
	int	ret;

	ret	= os_bar_create(dev, index, bar);

	if (ret)
	{
	}
	else if ((bar->phys_adrs == 0) || (bar->size == 0))
	{
		// This region is unmapped.

		if ((mem) && (io))
		{
			// This BAR region must be mapped.
			ret	= -EINVAL;
		}
		else if (mem)
		{
			// This BAR region must be memory mapped.
			ret	= -EINVAL;
		}
		else if (io)
		{
			// This BAR region must be I/O mapped.
			ret	= -EINVAL;
		}
	}
	else if ((io) && (bar->io_mapped))
	{
		// We wanted I/O mapped, and it is.
	}
	else if ((mem) && (bar->io_mapped == 0))
	{
		// We wanted memory mapped, and it is.
	}
	else if ((io) || (mem))
	{
		// The region is not mapped the correct way.
		ret	= -EINVAL;
	}

	if (ret)
		printf("%s: BAR%d access error.\n", DEV_NAME, index);

#if DEV_BAR_SHOW
	printf(	"BAR%d"
			": Reg 0x%08lX"
			", Map %s"
			", Adrs 0x%08lX"
			", Size %4ld"
			", vaddr 0x%lX"
			"\n",
			(int) index,
			(long) bar->reg,
			bar->size ? (bar->io_mapped ? "I/O" : "mem") : "N/A",
			(long) bar->phys_adrs,
			(long) bar->size,
			(long) bar->vaddr);
#endif

	return(ret);
}



/******************************************************************************
*
*	Function:	gsc_bar_destroy
*
*	Purpose:
*
*		Release the given BAR region and its resources.
*
*	Arguments:
*
*		bar		The structure for the BAR to release.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_bar_destroy(os_bar_t* bar)
{
	if (bar)
	{
		os_bar_destroy(bar);
		memset(bar, 0, sizeof(bar[0]));
	}
}


