// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/driver/irq.c $
// $Rev: 31802 $
// $Date: 2015-06-09 12:11:52 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



//*****************************************************************************
void dev_irq_isr_local_handler(dev_data_t* dev)
{
	#define	BCR_IRQ_REQUEST	0x800L

	u32	bcr;
	u32	req;

	bcr	= os_reg_mem_rx_u32(NULL, dev->vaddr.gsc_bctlr_32);

	if (bcr & BCR_IRQ_REQUEST)
	{
		// Clear the interrupt.
		bcr	^= BCR_IRQ_REQUEST;
		os_reg_mem_tx_u32(NULL, dev->vaddr.gsc_bctlr_32, bcr);

		// Resume any waiting threads.
		req	= GSC_FIELD_DECODE(bcr, 10, 8);

		switch (req)
		{
			default:

				gsc_wait_resume_irq_main(dev, GSC_WAIT_MAIN_SPURIOUS);
				break;

			case DSI_IRQ_INIT_DONE:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_INIT_DONE);
				break;

			case DSI_IRQ_AUTO_CAL_DONE:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AUTO_CAL_DONE);
				break;

			case DSI_IRQ_CHAN_READY:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_CHAN_READY);
				break;

			case DSI_IRQ_AIN_BUF_THRESH_L2H:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AIN_BUF_THRESH_L2H);
				break;

			case DSI_IRQ_AIN_BUF_THRESH_H2L:

				gsc_wait_resume_irq_gsc(dev, DSI_WAIT_GSC_AIN_BUF_THRESH_H2L);
				break;
		}
	}
	else
	{
		// We don't know the source of the interrupt.
		gsc_wait_resume_irq_main(dev, GSC_WAIT_MAIN_SPURIOUS);
	}
}



//*****************************************************************************
int dev_irq_create(dev_data_t* dev)
{
	int	ret;

	os_reg_mem_tx_u32(dev, dev->vaddr.gsc_bctlr_32, 0);
	ret	= gsc_irq_create(dev);
	return(ret);
}



//*****************************************************************************
void dev_irq_destroy(dev_data_t* dev)
{
	if (dev->vaddr.gsc_bctlr_32)
		os_reg_mem_tx_u32(dev, dev->vaddr.gsc_bctlr_32, 0);

	gsc_irq_destroy(dev);
}


