// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_open.c $
// $Rev: 21125 $
// $Date: 2013-04-19 13:01:39 -0500 (Fri, 19 Apr 2013) $

#include "main.h"



//*****************************************************************************
int gsc_open(GSC_ALT_STRUCT_T* alt)
{
	int	ret;

	for (;;)	// A convenience loop.
	{
		ret	= os_sem_lock(&alt->sem);

		if (ret)
		{
			// We didn't get the lock.
			break;
		}

		if (alt->in_use)
		{
			// The device is already being used.
			ret	= -EBUSY;
			os_sem_unlock(&alt->sem);
			break;
		}

		// Perform device specific OPEN actions.
		ret	= dev_open(alt);

		if (ret)
		{
			// There was a problem here.
			os_sem_unlock(&alt->sem);
			break;
		}

		alt->in_use	= 1;	// Mark as being open/in use.
		os_sem_unlock(&alt->sem);
		break;
	}

	return(ret);
}


