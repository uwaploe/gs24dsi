// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/driver/gsc_close.c $
// $Rev: 31783 $
// $Date: 2015-06-09 10:42:50 -0500 (Tue, 09 Jun 2015) $

#include "main.h"



//*****************************************************************************
int gsc_close(GSC_ALT_STRUCT_T* alt)
{
	int	ret	= 0;
	int	tmp;

	ret	= os_sem_lock(&alt->sem);
	tmp	= dev_close(alt);
	alt->in_use	= 0;

	if (ret == 0)
	{
		ret	= tmp;
		os_sem_unlock(&alt->sem);
	}

	return(ret);
}


