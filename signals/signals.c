// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/signals/signals.c $
// $Rev: 25254 $
// $Date: 2014-02-26 11:00:03 -0600 (Wed, 26 Feb 2014) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	signals
*
*		Output the clocking signals for a period.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*		seconds	Output the signal for this many seconds.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int signals(int fd, int seconds)
{
	time_t	end		= time(NULL) + seconds;
	int		errs	= 0;
	time_t	now;

	errs	+= dsi_initialize	(fd, -1, 1);
	errs	+= dsi_init_mode	(fd, -1, 1, DSI_INIT_MODE_INITIATOR, NULL);
	errs	+= dsi_xcvr_type	(fd, -1, 1, DSI_XCVR_TYPE_LVDS, NULL);

	// The output clock signal.

	gsc_label("Clock Output");
	printf("(%d seconds)\n", seconds);
	gsc_label_level_inc();
	errs	+= dsi_external_clock_source(fd, -1, 1, DSI_EXT_CLK_SRC_GEN_A, NULL);
	errs	+= dsi_rate_gen_a_nvco		(fd, -1, 1, 117, NULL);
	errs	+= dsi_rate_gen_a_nref		(fd, -1, 1, 125, NULL);
	errs	+= dsi_rate_gen_a_nrate		(fd, -1, 1, 17188, NULL);
	gsc_label_level_dec();

	// The input trigger signal.

	gsc_label("Output Sync");
	printf("(%d seconds)\n", seconds);
	gsc_label_level_inc();
	gsc_label("Working");

	for (; errs == 0;)
	{
		errs	+= dsi_sw_sync(fd, -1, 0);
		now		= time(NULL);

		if (now > end)
			break;
	}

	printf("Done\n");
	gsc_label_level_dec();

	errs	+= dsi_initialize(fd, -1, 1);

	return(errs);
}



