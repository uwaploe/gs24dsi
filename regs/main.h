// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/regs/main.h $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi_dsl.h"
#include "24dsi_utils.h"



// data types *****************************************************************

typedef struct
{
	const char*	name;				// NULL terminates list.
	void		(*func)(int fd);	// NULL terminates list.
} menu_item_t;

typedef struct
{
	const char*			title;
	const menu_item_t*	list;
} menu_t;



// prototypes *****************************************************************

void	main_menu(int fd);
void	menu_call(int fd, const menu_t* menu);
int		menu_select(const menu_t* menu);
void	reg_mod_by_name(int fd);
void	reg_mod_by_offset(int fd);



#endif
