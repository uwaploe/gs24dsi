// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/regs/main.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <stdio.h>
#include <stdlib.h>

#include "main.h"



// variables ******************************************************************

static	int	_def_index	= 0;



//*****************************************************************************
static int _parse_args(int argc, char** argv)
{
	char	buf[64];
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;

	printf("regs - Register Access Application\n");
	printf("USAGE: regs <index>\n");
	printf("  index  The zero based index of the device to access.\n");

	gsc_label("Arguments");
	printf("(%d argument%s)\n", argc - 1, ((argc - 1) > 1) ? "s" : "");
	gsc_label_level_inc();

	for (i = 0; i < argc; i++)
	{
		sprintf(buf, "Argument %d", i);
		gsc_label(buf);
		printf("%s\n", argv[i]);
	}

	gsc_label_level_dec();

	for (i = 1; i < argc; i++)
	{
		j	= sscanf(argv[i], "%d%c", &k, &c);

		if ((j == 1) && (k >= 0))
		{
			_def_index	= k;
			continue;
		}
		else
		{
			errs	= 1;
			printf("ERROR: invalid board selection: %s\n", argv[i]);
			break;
		}
	}

	return(errs);
}



/*****************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
*****************************************************************************/

int main(int argc, char** argv)
{
	int		errs;
	int		fd		= 0;
	int		qty;
	int		ret		= EXIT_FAILURE;

	for (;;)
	{
		gsc_label_init(28);
		errs	= _parse_args(argc, argv);

		if (errs)
			break;

		qty	= dsi_count_boards();

		if (qty <= 0)
			break;

		gsc_label("Accessing Board Index");
		printf("%d\n", _def_index);
		fd	= gsc_dev_open(_def_index, DSI_BASE_NAME);

		if (fd == -1)
		{
			errs	= 1;
			printf(	"ERROR: Unable to access device %d.", _def_index);
		}

		if (errs == 0)
		{
			ret		= EXIT_SUCCESS;
			main_menu(fd);
		}

		gsc_dev_close(_def_index, fd);
		break;
	}

	return(ret);
}



