// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_rate_gen_x_nref.c $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



// variables *******************************************************************

static const int	_nref_ioctl[]	=
{
	DSI_IOCTL_RATE_GEN_A_NREF,
	DSI_IOCTL_RATE_GEN_B_NREF
};



//*****************************************************************************
int dsi_rate_gen_x_nref(int fd, int index, int verbose, int gen, s32 set, s32* get)
{
	char	buf[128];
	int		cmd;
	int		errs	= 0;
	s32		gen_qty;
	s32		max;
	s32		min;
	s32		pll;

	if (verbose)
	{
		sprintf(buf, "Rate Gen %c Nref", 'A' + gen);
		gsc_label_index(buf, index);
	}

	for (;;)	// A convenience loop.
	{
		if ((gen < 0) || (gen > 1))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid rate generator index: %d)\n", gen);

			break;
		}

		cmd		= _nref_ioctl[gen];
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &max);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &min);

		if (errs)
		{
			break;
		}
		else if (gen >= gen_qty)
		{
			if (verbose)
				printf("SKIPPED  (Not supported on this board.)\n");

			break;
		}
		else if (pll == 0)
		{
			if (verbose)
				printf("SKIPPED  (Not supported on this board.)\n");

			break;
		}
		else
		{
			set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
			errs	= dsi_dsl_ioctl(fd, cmd, &set);

			if (verbose)
				printf("%s  (Nref = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
		}

		break;
	}

	if (get)
		get[0]	= set;

	return(errs);
}


//*****************************************************************************
int dsi_rate_gen_x_nref_all(int fd, int index, int verbose, s32 set)
{
	int	errs	= 0;
	int	gen;
	s32	gen_qty;
	s32	pll;

	if (verbose)
		gsc_label_index("Nref", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);

	if (errs)
	{
	}
	else if (pll == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		if (verbose)
			printf("\n");

		gsc_label_level_inc();

		for (gen = 0; gen < gen_qty; gen++)
			errs	+= dsi_rate_gen_x_nref(fd, -1, verbose, gen, set, NULL);

		gsc_label_level_dec();
	}

	return(errs);
}


