// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_xcvr_type.c $
// $Rev: 32479 $
// $Date: 2015-07-04 14:59:18 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_xcvr_type
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_XCVR_TYPE IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_xcvr_type(int fd, int index, int verbose, s32 set, s32* get)
{
	char	buf[128];
	int		errs	= 0;
	s32		xcvr;

	if (verbose)
		gsc_label_index("Transceiver Type", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D20_XCVR, &xcvr);

	if (errs)
	{
	}
	else if (xcvr == 0)
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_XCVR_TYPE, &set);

		switch (set)
		{
			default:

				sprintf(buf, "INVALID: %ld", (long) set);
				break;

			case DSI_XCVR_TYPE_LVDS:

				strcpy(buf, "LVDS");
				break;

			case DSI_XCVR_TYPE_TTL:

				strcpy(buf, "TTL");
				break;
		}

		if (verbose)
			printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);
	}

	if (get)
		get[0]	= set;

	return(errs);
}


