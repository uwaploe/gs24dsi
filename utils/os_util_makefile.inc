# $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_makefile.inc $
# $Rev: 34141 $
# $Date: 2016-01-08 16:53:02 -0600 (Fri, 08 Jan 2016) $

.PHONY: all clean default makefile release

GSC_TARGET			= gsc_utils.a

GSC_OBJ_FILES		:=							\
					./gsc_util_buf_man.o		\
					./gsc_util_close.o			\
					./gsc_util_label.o			\
					./gsc_util_open.o			\
					./gsc_util_reg.o			\
					./gsc_util_reg_pex8111.o	\
					./gsc_util_reg_pex8112.o	\
					./gsc_util_reg_plx9056.o	\
					./gsc_util_reg_plx9060es.o	\
					./gsc_util_reg_plx9080.o	\
					./gsc_util_reg_plx9656.o	\
					./gsc_util_time.o			\
												\
					./os_util_close.o			\
					./os_util_count.o			\
					./os_util_id.o				\
					./os_util_kbd.o				\
					./os_util_open.o			\
					./os_util_sem.o				\
					./os_util_sleep.o			\
					./os_util_thread.o			\
					./os_util_time.o



ECHO	:= ${shell ls /bin/echo 2>/dev/null | wc -l}
ifeq ("${ECHO}","1")
ECHO	:= /bin/echo
else
ECHO	:= echo
endif



${GSC_TARGET}: ${GSC_OBJ_FILES}
	@echo ==== Linking: $@
	@ld -r -o $@ ${GSC_OBJ_FILES}


