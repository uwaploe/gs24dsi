// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_initialize.c $
// $Rev: 25252 $
// $Date: 2014-02-26 10:58:19 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_initialize
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_INITIALIZE IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_initialize(int fd, int index, int verbose)
{
	int	errs	= 0;

	if (verbose)
		gsc_label_index("Initialize", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_INITIALIZE, 0);

	if (verbose)
		printf("%s\n", errs ? "FAIL <---" : "PASS");

	return(errs);
}


