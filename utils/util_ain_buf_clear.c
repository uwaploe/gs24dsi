// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_ain_buf_clear.c $
// $Rev: 32479 $
// $Date: 2015-07-04 14:59:18 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_ain_buf_clear_at_boundary
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_AIN_BUF_CLEAR IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_ain_buf_clear_at_boundary(int fd, int index, int verbose)
{
	int	errs	= 0;
	s32	mode;

	// Use the SW Sync feature to

	if (verbose)
		gsc_label_index("ADC Buffer Clear", index);

	errs	+= dsi_sw_sync_mode(fd, -1, 0, -1, &mode);

	// Clear the buffer with any over or under flow status.
	errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_CLEAR, NULL);

	// Clear the buffer on a scan boundary.
	errs	+= dsi_sw_sync_mode(fd, -1, 0, DSI_SW_SYNC_MODE_SW_SYNC, NULL);
	errs	+= dsi_sw_sync(fd, -1, 0);
	errs	+= dsi_channel_ready(fd, -1, 0);

	errs	+= dsi_sw_sync_mode(fd, -1, 0, DSI_SW_SYNC_MODE_CLR_BUF, NULL);
	errs	+= dsi_sw_sync(fd, -1, 0);
	errs	+= dsi_channel_ready(fd, -1, 0);

	// Restore the SW Sync Mode.
	errs	+= dsi_sw_sync_mode(fd, -1, 0, mode, NULL);

	if (verbose)
		printf("%s  (Cleared on a scan boundary.)\n", errs ? "FAIL <---" : "PASS");

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_ain_buf_clear
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_AIN_BUF_CLEAR IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_ain_buf_clear(int fd, int index, int verbose)
{
	int	errs;

	if (verbose)
		gsc_label_index("ADC Buffer Clear", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_CLEAR, NULL);

	if (verbose)
		printf("%s\n", errs ? "FAIL <---" : "PASS");

	return(errs);
}


