// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_rate_gen_b_nref.c $
// $Rev: 32479 $
// $Date: 2015-07-04 14:59:18 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



//*****************************************************************************
int dsi_rate_gen_b_nref(int fd, int index, int verbose, s32 set, s32* get)
{
	int	errs	= 0;
	s32	gen_qty;
	s32	max;
	s32	min;
	s32	pll;

	if (verbose)
		gsc_label_index("Rate Gen B Nref", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gen_qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &min);

	if (errs)
	{
	}
	else if ((gen_qty < 2) || (pll == 0))
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RATE_GEN_B_NREF, &set);

		if (verbose)
			printf("%s  (Nref = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
	}

	if (get)
		get[0]	= set;

	return(errs);
}


