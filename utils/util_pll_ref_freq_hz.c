// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_pll_ref_freq_hz.c $
// $Rev: 32479 $
// $Date: 2015-07-04 14:59:18 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_pll_ref_freq_hz
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_PLL_REF_FREQ_HZ IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_pll_ref_freq_hz(int fd, int index, int verbose, s32* get)
{
	int	errs;
	s32	set;

	if (verbose)
		gsc_label_index("PLL Reference Freq.", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_PLL_REF_FREQ_HZ, &set);

	if ((errs == 0) && (verbose))
		printf("%s  (%ld Hz)\n", errs ? "FAIL <---" : "PASS", (long) set);

	if (get)
		get[0]	= set;

	return(errs);
}


