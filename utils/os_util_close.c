// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_close.c $
// $Rev: 33914 $
// $Date: 2015-10-07 14:22:35 -0500 (Wed, 07 Oct 2015) $

#include <fcntl.h>
#include <stdio.h>

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	os_close
*
*	Purpose:
*
*		Perform a close on the device with the specified access handle.
*
*	Arguments:
*
*		fd		The file descriptor used to access the device.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void os_close(int fd)
{
	close(fd);
}



