// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_count.c $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#include "24dsi_utils.h"



/******************************************************************************
*
*	Function:	dsi_count_boards
*
*	Purpose:
*
*		Count the number of installed boards.
*
*	Arguments:
*
*		None.
*
*	Returned:
*
*		> 0		The number of boards found.
*		== 0	No boards were found.
*
******************************************************************************/

int dsi_count_boards(void)
{
	int	qty;

	qty	= os_count_boards(DSI_BASE_NAME);
	return(qty);
}



