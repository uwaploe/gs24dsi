// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_utils.h $
// $Rev: 34141 $
// $Date: 2016-01-08 16:53:02 -0600 (Fri, 08 Jan 2016) $

#ifndef __OS_UTILS_H__
#define __OS_UTILS_H__

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <asm/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include "os_common.h"



// data types *****************************************************************

typedef struct
{
	pthread_t	thread;
	char		name[64];
} os_thread_t;

typedef	struct
{
	void*		key;	// struct is valid only if key == & of struct
	sem_t		sem;
} os_sem_t;



// prototypes *****************************************************************

void			os_close(int fd);
int				os_count_boards(const char* base);		// # of devices

int				os_id_driver(const char* base);			// # of errors
void			os_id_host(void);

void			os_kbd_close(void);
void			os_kbd_open(void);
int				os_kbd_hit(void);
int				os_kbd_read(void);

int				os_open(unsigned int index, const char* base);

int				os_sem_create(os_sem_t* sem);
int				os_sem_create_qty(os_sem_t* sem, int cap, int put);
int				os_sem_destroy(os_sem_t* sem);
int				os_sem_lock(os_sem_t* sem);
int				os_sem_unlock(os_sem_t* sem);

void			os_sleep_ms(int ms);

int				os_thread_create(os_thread_t* thread, const char* name, int (*func)(void* arg), void* arg);
int				os_thread_destroy(os_thread_t* thread);
size_t			os_time_delta_ms(void);



#endif
