// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/gsc_util_open.c $
// $Rev: 33913 $
// $Date: 2015-10-07 14:21:02 -0500 (Wed, 07 Oct 2015) $

#include <fcntl.h>
#include <stdio.h>

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	gsc_dev_open
*
*	Purpose:
*
*		Perform an open on the device with the specified index.
*
*	Arguments:
*
*		index	The index of the board to accessed.
*
*		base	The base name for the /proc file entry.
*
*	Returned:
*
*		>= 0	The handle to the board to access.
*		-1		There was a problem. Consult errno.
*
******************************************************************************/

int gsc_dev_open(unsigned int index, const char* base)
{
	char	buf[80];
	int		fd;
	char	name[128];

	sprintf(buf, "Opening #%u", index);
	gsc_label(buf);
	sprintf(name, "/dev/%s.%u", base, index);
	fd	= open(name, O_RDWR);

	if (fd == -1)
		printf("FAIL <---\n");
	else
		printf("PASS\n");

	return(fd);
}


