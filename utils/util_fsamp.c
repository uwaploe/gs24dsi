// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_fsamp.c $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



// #defines *******************************************************************

#define	abs(v)					(((v) < 0) ? (-(v)) : (v))



//*****************************************************************************
static int _dsi_fsamp_compute_legacy(
	int		fd,
	int		index,
	int		verbose,
	int		options,
	s32		fref,
	s32*	fsamp,
	s32*	nrate,
	s32*	ndiv)
{
	char	buf[32];
	long	count		= 0;
	float	delta;
	float	delta_abs;
	float	delta_best;
	int		errs		= 0;
	float	fgen_best;
	float	fgen_hz;
	s32		fgen_max;
	s32		fgen_min;
	s32		fsamp_best;
	s32		fsamp_max;
	s32		fsamp_min;
	float	fsamp_sps;
	s32		fsamp_want	= fsamp[0];
	s32		ndiv_best;
	s32		ndiv_inc;
	s32		ndiv_max;
	s32		ndiv_min;
	s32		nrate_best;
	s32		nrate_inc;
	s32		nrate_max;
	s32		nrate_min;

	gsc_label_level_inc();

	errs	+= dsi_query_nrate_range(fd, -1, 0, &nrate_min, &nrate_max);
	errs	+= dsi_query_fgen_range	(fd, -1, 0, &fgen_min, &fgen_max);
	errs	+= dsi_query_ndiv_range	(fd, -1, 0, &ndiv_min, &ndiv_max);
	errs	+= dsi_query_fsamp_range(fd, -1, 0, &fsamp_min, &fsamp_max);
	errs	+= dsi_fsamp_validate	(fd, -1, 0, fsamp);

	delta_best	= LONG_MAX;
	nrate_best	= nrate_max;
	fgen_best	= DSI_FGEN_LEGACY_HZ(nrate_best);
	ndiv_best	= ndiv_max;
	fsamp_best	= DSI_FSAMP_LEGACY_SPS(nrate_best,ndiv_best);

	for (;;)	// A convenience loop.
	{
		if (errs)
		{
			errs	= 1;

			if (verbose)
				printf("SKIPPED  (errors reported)\n");

			break;
		}

		if (verbose)
			printf("(Legacy mode, command line Nref ignored)\n");

		for (nrate_inc = nrate_min; nrate_inc <= nrate_max; nrate_inc++)
		{
			fgen_hz	= DSI_FGEN_LEGACY_HZ(nrate_inc);

			if ((fgen_hz < fgen_min) || (fgen_hz > fgen_max))
				continue;

			for (ndiv_inc = ndiv_min; ndiv_inc <= ndiv_max; ndiv_inc++)
			{
				fsamp_sps	= DSI_FSAMP_LEGACY_SPS(nrate_inc, ndiv_inc);

				if ((fsamp_sps < fsamp_min) || (fsamp_sps > fsamp_max))
					continue;

				delta		= fsamp_sps - fsamp_want;
				delta_abs	= abs(delta);

				if ((verbose) && (options) && (delta_abs <= options))
				{
					count++;
					sprintf(buf, "Option %ld", count);
					gsc_label(buf);

					printf("Nrate %4ld", (long) nrate_inc);
					printf(", Ndiv %2ld", (long) ndiv_inc);
					printf(", Fsamp %6ld", (long) fsamp_sps);
					printf(", Difference %7.4f\n", delta);
				}

				if (delta_abs >= delta_best)
					continue;			// This result is no better.

				if (delta_best < 0.005)
					continue;			// There's no benefit to be gained.

				nrate_best	= nrate_inc;
				fgen_best	= fgen_hz;
				ndiv_best	= ndiv_inc;
				fsamp_best	= fsamp_sps;
				delta_best	= delta_abs;
			}
		}

		if (verbose)
		{
			if (options)
			{
				gsc_label("Selected");
				printf("\n");
				gsc_label_level_inc();
			}

			gsc_label("Nrate");
			gsc_label_long_comma((long) nrate_best);
			printf("\n");

			gsc_label("Fgen");
			gsc_label_long_comma((long) fgen_best);
			printf(" Hz\n");

			gsc_label("Ndiv");
			gsc_label_long_comma((long) ndiv_best);
			printf("\n");

			gsc_label("Fsamp");
			gsc_label_long_comma((long) fsamp_best);
			printf(" S/S\n");

			gsc_label("Difference");
			delta	= fsamp_best - fsamp_want;
			gsc_label_long_comma((long) delta);
			printf(" S/S\n");

			if (options)
				gsc_label_level_dec();
		}

		break;
	}

	gsc_label_level_dec();

	nrate[0]	= nrate_best;
	ndiv[0]		= ndiv_best;
	fsamp[0]	= fsamp_best;

	return(errs);
}



//*****************************************************************************
static int _dsi_fsamp_compute_pll(
	int		fd,
	int		index,
	int		verbose,
	int		options,
	s32		fref,
	s32*	fsamp,
	s32*	nvco,
	s32*	nref,
	s32*	ndiv)
{
	char	buf[32];
	long	count		= 0;
	float	delta;
	float	delta_abs;
	float	delta_best;
	int		errs		= 0;
	float	fgen_best;
	float	fgen_hz;
	s32		fgen_max;
	s32		fgen_min;
	s32		fsamp_best;
	s32		fsamp_max;
	s32		fsamp_min;
	float	fsamp_sps;
	s32		fsamp_want	= fsamp[0];
	s32		ndiv_best;
	s32		ndiv_inc;
	s32		ndiv_max;
	s32		ndiv_min;
	s32		nref_best;
	s32		nref_inc;
	s32		nref_max;
	s32		nref_min;
	s32		nvco_best;
	s32		nvco_inc;
	s32		nvco_max;
	s32		nvco_min;

	gsc_label_level_inc();

	errs	+= dsi_query_nvco_range	(fd, -1, 0, &nvco_min, &nvco_max);
	errs	+= dsi_query_nref_range	(fd, -1, 0, &nref_min, &nref_max);
	errs	+= dsi_query_fgen_range	(fd, -1, 0, &fgen_min, &fgen_max);
	errs	+= dsi_query_ndiv_range	(fd, -1, 0, &ndiv_min, &ndiv_max);
	errs	+= dsi_query_fsamp_range(fd, -1, 0, &fsamp_min, &fsamp_max);
	errs	+= dsi_fsamp_validate	(fd, -1, 0, fsamp);

	delta_best	= LONG_MAX;
	nvco_best	= nvco_max;
	nref_best	= nref_max;
	fgen_best	= DSI_FGEN_PLL_HZ(fref, nvco_best, nref_best);
	ndiv_best	= ndiv_max;
	fsamp_best	= DSI_FSAMP_PLL_SPS(fref, nvco_best, nref_best, ndiv_best);

	for (;;)	// A convenience loop.
	{
		if (errs)
		{
			errs	= 1;

			if (verbose)
				printf("SKIPPED  (errors reported)\n");

			break;
		}

		if (verbose)
			printf("(PLL mode)\n");

		for (nvco_inc = nvco_min; nvco_inc <= nvco_max; nvco_inc++)
		{
			for (nref_inc = nref_min; nref_inc <= nvco_max; nref_inc++)
			{
				fgen_hz	= DSI_FGEN_PLL_HZ(fref, nvco_inc, nref_inc);

				if ((fgen_hz < fgen_min) || (fgen_hz > fgen_max))
					continue;

				for (ndiv_inc = ndiv_min; ndiv_inc <= ndiv_max; ndiv_inc++)
				{
					fsamp_sps	= DSI_FSAMP_PLL_SPS(fref, nvco_inc, nref_inc, ndiv_inc);

					if ((fsamp_sps < fsamp_min) || (fsamp_sps > fsamp_max))
						continue;

					delta		= fsamp_sps - fsamp_want;
					delta_abs	= abs(delta);

					if ((verbose) && (options) && (delta_abs <= options))
					{
						count++;
						sprintf(buf, "Option %ld", count);
						gsc_label(buf);

						printf("Nvco %4ld", (long) nvco_inc);
						printf(", Nref %4ld", (long) nref_inc);
						printf(", Ndiv %2ld", (long) ndiv_inc);
						printf(", Fsamp %6ld", (long) fsamp_sps);
						printf(", Difference %7.4f\n", delta);
					}

					if (delta_abs >= delta_best)
						continue;			// This result is no better.

					if (delta_best < 0.3)
						continue;			// There's no benefit to be gained.

					nvco_best	= nvco_inc;
					nref_best	= nref_inc;
					fgen_best	= fgen_hz;
					ndiv_best	= ndiv_inc;
					fsamp_best	= fsamp_sps;
					delta_best	= delta_abs;
				}
			}
		}

		if (verbose)
		{
			if (options)
			{
				gsc_label("Selected");
				printf("\n");
				gsc_label_level_inc();
			}

			gsc_label("Fref");
			gsc_label_long_comma((long) fref);
			printf(" Hz\n");

			gsc_label("Nvco");
			gsc_label_long_comma((long) nvco_best);
			printf("\n");

			gsc_label("Nref");
			gsc_label_long_comma((long) nref_best);
			printf("\n");

			gsc_label("Fgen");
			gsc_label_long_comma((long) fgen_best);
			printf(" Hz\n");

			gsc_label("Ndiv");
			gsc_label_long_comma((long) ndiv_best);
			printf("\n");

			gsc_label("Fsamp");
			gsc_label_long_comma((long) fsamp_best);
			printf(" S/S\n");

			gsc_label("Difference");
			delta	= fsamp_want - fsamp_best;
			gsc_label_long_comma((long) delta);
			printf(" S/S\n");

			if (options)
				gsc_label_level_dec();
		}

		break;
	}

	gsc_label_level_dec();

	nvco[0]		= nvco_best;
	nref[0]		= nref_best;
	ndiv[0]		= ndiv_best;
	fsamp[0]	= fsamp_best;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_fsamp_compute
*
*	Purpose:
*
*		Calculate the best values for the specified sample rate.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		options	Display all computed options within this range of the requested
*				sample rate.
*
*		fref	The is the Fref value. This is ignored if this is a legacy
*				board.
*
*		fsamp	The desired sample rate.
*
*		nrate	Put the resulting Nrate value here.
*
*		ndiv	Put the resulting Ndiv value here.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_fsamp_compute(
	int		fd,
	int		index,
	int		verbose,
	s32		options,
	s32		fref,
	s32*	fsamp,
	s32*	nvco,
	s32*	nref,
	s32*	nrate,
	s32*	ndiv)
{
	int	errs		= 0;
	s32	pll;

	if (verbose)
		gsc_label_index("Rate Calculation", index);

	errs	+= dsi_query_frequency_control	(fd, -1, 0, &pll);
	errs	+= dsi_fsamp_validate			(fd, -1, 0, fsamp);

	for (;;)	// A convenience loop.
	{
		if (errs)
		{
			errs	= 1;

			if (verbose)
				printf("SKIPPED  (errors reported)\n");

			break;
		}

		if (pll)
		{
			nrate[0]	= 0;
			errs		= _dsi_fsamp_compute_pll(
							fd,
							index,
							verbose,
							options,
							fref,
							fsamp,
							nvco,
							nref,
							ndiv);
		}
		else
		{
			nvco[0]	= 0;
			nref[0]	= 0;
			ndiv[0]	= 0;
			errs	= _dsi_fsamp_compute_legacy(
						fd,
						index,
						verbose,
						options,
						fref,
						fsamp,
						nrate,
						ndiv);
		}

		break;
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_fsamp_report
*
*	Purpose:
*
*		Determine and report the sample rate for the specified channel.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		chan	The index of the channel of interest.
*
*		fref	The user specified reference frequency.
*
*		sps		Store the sample rate here, if non-NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi_fsamp_report(int fd, int index, int verbose, int chan, s32* fref, s32* sps)
{
	char	buf[64];
	int		c_p_g;		// channels per group
	s32		chans;
	s32		dividers	= 0;
	int		errs		= 0;
	s32		fsamp		= 0;
	int		group;
	s32		groups;
	s32		grp0rar;			// Ch Group 0 has precidence in RAR
	s32		ndiv		= 0;
	s32		nrate		= 0;
	s32		nref		= 0;
	s32		nvco		= 0;
	s32		order;
	s32		pll			= 0;
	s32		src			= -1;

	if (verbose)
	{
		sprintf(buf, "Channel %d", chan);
		gsc_label_index(buf, index);
	}

	for (;;)
	{
		if ((chan < 0) || (chan > 32))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid channel index: %d)\n", chan);

			break;
		}

		errs	+= dsi_channel_order(fd, -1, 0, -1, &order);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CH_GRP_0_RAR, &grp0rar);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_QTY, &chans);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
		errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &dividers);
		c_p_g	= chans / groups;
		group	= chan / c_p_g;
		errs	+= dsi_channel_group_source(fd, -1, 0, group, -1, &src);

		if ((src == DSI_CH_GRP_SRC_ENABLE)	||
			(order == DSI_CHANNEL_ORDER_SYNC))
		{
			// When we get an ENABLE source we must use the channel 0 source.
			// For synchronous ordering we also use the channel 0 source.
			errs	+= dsi_channel_group_source(fd, -1, 0, 0, -1, &src);
		}

		switch (src)
		{
			default:
			case DSI_CH_GRP_SRC_ENABLE:

				errs	= 1;

				if (verbose)
				{
					printf(	"FAIL <---  (invalid logic or source option: %ld)\n",
							(long) src);
				}

				break;

			case DSI_CH_GRP_SRC_GEN_A:

				if (pll)
				{
					errs	+= dsi_rate_gen_x_nvco(fd, -1, 0, 0, -1, &nvco);
					errs	+= dsi_rate_gen_x_nref(fd, -1, 0, 0, -1, &nref);
					errs	+= dsi_fref_compute(fd, -1, 0, fref);
				}
				else
				{
					errs	+= dsi_rate_gen_x_nrate(fd, -1, 0, 0, -1, &nrate);
				}

				errs	+= dsi_rate_gen_x_ndiv(fd, -1, 0, 0, -1, &ndiv);
				break;

			case DSI_CH_GRP_SRC_GEN_B:

				if (pll)
				{
					errs	+= dsi_rate_gen_x_nvco(fd, -1, 0, 1, -1, &nvco);
					errs	+= dsi_rate_gen_x_nref(fd, -1, 0, 1, -1, &nref);
					errs	+= dsi_fref_compute(fd, -1, 0, fref);
				}
				else
				{
					errs	+= dsi_rate_gen_x_nrate(fd, -1, 0, 1, -1, &nrate);
				}

				errs	+= dsi_rate_gen_x_ndiv(fd, -1, 0, 0, -1, &ndiv);
				break;

			case DSI_CH_GRP_SRC_EXTERN:
			case DSI_CH_GRP_SRC_DIR_EXTERN:
			case DSI_CH_GRP_SRC_DISABLE:

				break;
		}

		if (errs)
		{
		}
		else if (src == DSI_CH_GRP_SRC_DISABLE)
		{
			fsamp	= 0;

			if (verbose)
				printf("Disabled\n");
		}
		else if (	(src == DSI_CH_GRP_SRC_EXTERN)	||
					(src == DSI_CH_GRP_SRC_DIR_EXTERN))
		{
			fsamp	= -1;

			if (verbose)
				printf("External Source\n");
		}
		else if (pll)
		{
			fsamp	= DSI_FSAMP_PLL_SPS(fref[0], nvco, nref, ndiv);

			if (verbose)
			{
				gsc_label_long_comma(fsamp);
				printf(" S/S  (Fref ");
				gsc_label_long_comma(fref[0]);
				printf(" Hz, Nvco ");
				gsc_label_long_comma(nvco);
				printf(", Nref ");
				gsc_label_long_comma(nref);
				printf(", Ndiv ");
				gsc_label_long_comma(ndiv);
				printf(")\n");
			}
		}
		else
		{
			fsamp	= DSI_FSAMP_LEGACY_SPS(nrate, ndiv);

			if (verbose)
			{
				gsc_label_long_comma(fsamp);
				printf(" S/S  (Fref ");
				gsc_label_long_comma(fref[0]);
				printf(" Hz, Nrate ");
				gsc_label_long_comma(nrate);
				printf(", Ndiv ");
				gsc_label_long_comma(ndiv);
				printf(")\n");
			}
		}

		break;
	}

	if (sps)
		sps[0]	= fsamp;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_fsamp_report_all
*
*	Purpose:
*
*		Determine and report the sample rate for all channels.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int dsi_fsamp_report_all(int fd, int index, int verbose, s32* fref)
{
	s32	chans;
	int	chan;
	int	errs	= 0;
	s32	sps;
	s32	total	= 0;

	if (verbose)
		gsc_label_index("Sample Rates", index);

	errs	+= dsi_query(fd, index, 0, DSI_QUERY_CHANNEL_QTY, &chans);

	if (errs == 0)
	{
		if (verbose)
			printf("\n");

		gsc_label_level_inc();

		for (chan = 0; chan < chans; chan++)
		{
			errs	+= dsi_fsamp_report(fd, index, verbose, chan, fref, &sps);
			total	+= sps;
		}

		if (verbose)
		{
			gsc_label("Overall Rate");
			gsc_label_long_comma(total);
			printf(" S/S\n");
		}

		gsc_label_level_dec();
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_fsamp_validate
*
*	Purpose:
*
*		Modify the target sample rate, if needed and report the results.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		fsamp	The desired sample rate is given here. We modifiy this if
*				needed.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_fsamp_validate(int fd, int index, int verbose, s32* fsamp)
{
	s32	def;
	int	errs	= 0;
	s32	max;
	s32	min;

	if (verbose)
		gsc_label_index("Fsamp Target", index);

	errs	+= dsi_query_fsamp_default	(fd, -1, 0, &def);
	errs	+= dsi_query_fsamp_range	(fd, -1, 0, &min, &max);

	if (fsamp[0] < 0)
	{
		fsamp[0]	= def;

		if (verbose)
		{
			gsc_label_long_comma((long) fsamp[0]);
			printf(" S/S  (default)\n");
		}
	}
	else if (fsamp[0] < min)
	{
		if (verbose)
		{
			gsc_label_long_comma((long) min);
			printf(" S/S  (");
			gsc_label_long_comma((long) fsamp[0]);
			printf(" S/S is less than the minimum)\n");
		}

		fsamp[0]	= min;
	}
	else if (fsamp[0] > max)
	{
		if (verbose)
		{
			gsc_label_long_comma((long) max);
			printf(" S/S  (");
			gsc_label_long_comma((long) fsamp[0]);
			printf(" S/S is more than the maximum)\n");
		}

		fsamp[0]	= max;
	}
	else
	{
		if (verbose)
		{
			gsc_label_long_comma((long) fsamp[0]);
			printf(" S/S\n");
		}
	}

	return(errs);
}


