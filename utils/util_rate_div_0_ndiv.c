// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_rate_div_0_ndiv.c $
// $Rev: 32479 $
// $Date: 2015-07-04 14:59:18 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



//*****************************************************************************
int dsi_rate_div_0_ndiv(int fd, int index, int verbose, s32 set, s32* get)
{
	int	errs	= 0;
	s32	div_qty;
	s32	groups;
	s32	max;
	s32	min;

	if (verbose)
		gsc_label_index("Rate Divider 0 Ndiv", index);

	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &div_qty);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &min);

	if (errs)
	{
	}
	else if ((div_qty < 1) || (groups < 1))
	{
		if (verbose)
			printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		set		= (set == -1) ? -1 : (set < min) ? min : ((set > max) ? max : set);
		errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RATE_DIV_0_NDIV, &set);

		if (verbose)
			printf("%s  (Ndiv = %ld)\n", errs ? "FAIL <---" : "PASS", (long) set);
	}

	if (get)
		get[0]	= set;

	return(errs);
}


