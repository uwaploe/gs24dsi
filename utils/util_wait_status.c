// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_wait_status.c $
// $Rev: 25252 $
// $Date: 2014-02-26 10:58:19 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_wait_status
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_WAIT_STATUS service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		wait	This is the crieteria to use.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int dsi_wait_status(int fd, int index, int verbose, gsc_wait_t* wait)
{
	int	errs;

	if (verbose)
		gsc_label_index("Wait Status", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_WAIT_STATUS, wait);

	if (verbose)
	{
		printf(	"%s  (%ld awaiting threads)\n",
				errs ? "FAIL <---" : "PASS",
				(long) wait->count);
	}

	return(errs);
}



