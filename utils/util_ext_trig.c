// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_ext_trig.c $
// $Rev: 32099 $
// $Date: 2015-06-16 16:28:47 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_external_trigger
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_EXT_TRIG IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_external_trigger(int fd, int index, int verbose, s32 set, s32* get)
{
	char	buf[128];
	int		errs;

	if (verbose)
		gsc_label_index("Ext. Trigger", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_EXT_TRIG, &set);

	switch (set)
	{
		default:

			sprintf(buf, "INVALID: %ld", (long) set);
			break;

		case DSI_EXT_TRIG_DISARM:

			strcpy(buf, "Disarm");
			break;

		case DSI_EXT_TRIG_ARM:

			strcpy(buf, "Arm");
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


