// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_ch_grp_x_src.c $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#include <stdio.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



// variables *******************************************************************

static const int	_ch_grp_ioctl[]	=
{
	DSI_IOCTL_CH_GRP_0_SRC,
	DSI_IOCTL_CH_GRP_1_SRC,
	DSI_IOCTL_CH_GRP_2_SRC,
	DSI_IOCTL_CH_GRP_3_SRC
};

static const char*	_source[]	=
{
	"Rate Generator A",
	"Rate Generator B",
	"Rate Generator C",
	"Rate Generator D",
	"External",
	"Direct External",
	"Disable",
	"Enable"
};



/******************************************************************************
*
*	Function:	dsi_channel_group_source
*
*	Purpose:
*
*		Set and/or get the Channel Group Source option for the given channel
*		group.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		group	This is the channel group index of interest.
*
*		set		The setting to apply, or -1 to query.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_channel_group_source(int fd, int index, int verbose, int group, s32 set, s32* get)
{
	char	buf[128];
	int		cmd;
	int		errs	= 0;
	s32		groups;
	s32		pri;

	if (verbose)
	{
		sprintf(buf, "Channel Group %d Source", group);
		gsc_label_index(buf, index);
	}

	for (;;)	// A convenience loop.
	{
		if ((group < 0) || (group > 3))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid channel group index: %d)\n", group);

			break;
		}

		if ((set < -1) || (set >= (s32) ARRAY_ELEMENTS(_source)))
		{
			errs	= 1;

			if (verbose)
				printf("FAIL <---  (invalid source option: %d)\n", set);

			break;
		}

		cmd		= _ch_grp_ioctl[group];
		errs	+= dsi_query(fd, index, 0, DSI_QUERY_CHANNEL_GPS, &groups);
		errs	+= dsi_query(fd, index, 0, DSI_QUERY_CH_GRP_0_RAR, &pri);

		if (errs)
		{
			break;
		}
		else if (group >= groups)
		{
			if (verbose)
			{
				printf(	"SKIPPED  "
						"(channel group %ld not supported on this board)\n",
						(long) group);
			}
		}
		else
		{
			if ((set >= 0) && (group) && (pri))
			{
				set		= (set == DSI_CH_GRP_SRC_DISABLE)
						? DSI_CH_GRP_SRC_DISABLE
						: DSI_CH_GRP_SRC_ENABLE;
			}

			errs	= dsi_dsl_ioctl(fd, cmd, &set);

			if (verbose)
				printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", _source[set]);
		}

		break;
	}

	if (get)
		get[0]	= set;

	return(errs);
}



/******************************************************************************
*
*	Function:	dsi_channel_group_source_all
*
*	Purpose:
*
*		Set the Channel Group Source option for all channel groups.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_channel_group_source_all(int fd, int index, int verbose, s32 set)
{
	int	group;
	s32	groups	= 0;
	int	errs	= 0;

	if (verbose)
		gsc_label_index("Channel Groups", index);

	errs	+= dsi_query(fd, index, 0, DSI_QUERY_CHANNEL_GPS, &groups);

	if ((errs == 0) && (verbose))
		printf("\n");

	gsc_label_level_inc();

	for (group = 0; group < groups; group++)
		errs	+= dsi_channel_group_source(fd, -1, verbose, group, set, NULL);

	gsc_label_level_dec();
	return(errs);
}


