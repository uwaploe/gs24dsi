// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/24dsi_utils.h $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#ifndef __24DSI_UTILS_H__
#define __24DSI_UTILS_H__

#include "24dsi.h"
#include "gsc_utils.h"



// #defines *******************************************************************

#define	ARRAY_ELEMENTS(a)	(sizeof((a))/sizeof((a)[0]))



// prototypes *****************************************************************

int	dsi_adc_rate_out				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_ADC_RATE_OUT
int	dsi_ain_buf_clear				(int fd, int index, int verbose);						// DSI_IOCTL_AIN_BUF_CLEAR
int	dsi_ain_buf_clear_at_boundary	(int fd, int index, int verbose);
int	dsi_ain_buf_input				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_BUF_INPUT
int	dsi_ain_buf_overflow			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_BUF_OVERFLOW
int	dsi_ain_buf_threshold			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_BUF_THRESH
int	dsi_ain_buf_underflow			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_BUF_UNDERFLOW
int	dsi_ain_coupling				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_COUPLING
int	dsi_ain_filter					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER
int	dsi_ain_filter_00_03			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_00_03
int	dsi_ain_filter_04_07			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_04_07
int	dsi_ain_filter_08_11			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_08_11
int	dsi_ain_filter_12_15			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_12_15
int	dsi_ain_filter_16_19			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_16_19
int	dsi_ain_filter_20_23			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_20_23
int	dsi_ain_filter_24_27			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_24_27
int	dsi_ain_filter_28_31			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_28_31
int	dsi_ain_filter_sel				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_FILTER_SEL
int	dsi_ain_mode					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_MODE
int	dsi_ain_range					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE
int	dsi_ain_range_00_03				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_00_03
int	dsi_ain_range_04_07				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_04_07
int	dsi_ain_range_08_11				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_08_11
int	dsi_ain_range_12_15				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_12_15
int	dsi_ain_range_16_19				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_16_19
int	dsi_ain_range_20_23				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_20_23
int	dsi_ain_range_24_27				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_24_27
int	dsi_ain_range_28_31				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_28_31
int	dsi_ain_range_sel				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_AIN_RANGE_SEL
int	dsi_auto_calibrate				(int fd, int index, int verbose);						// DSI_IOCTL_AUTO_CALIBRATE

int	dsi_ch_grp_0_src				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_CH_GRP_0_SRC
int	dsi_ch_grp_1_src				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_CH_GRP_1_SRC
int	dsi_ch_grp_2_src				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_CH_GRP_2_SRC
int	dsi_ch_grp_3_src				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_CH_GRP_3_SRC
int	dsi_channel_group_source		(int fd, int index, int verbose, int chan, s32 set, s32* get);
int	dsi_channel_group_source_all	(int fd, int index, int verbose, s32 set);
int	dsi_channel_order				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_CHANNEL_ORDER
int	dsi_channel_ready				(int fd, int index, int verbose);
int	dsi_config_ai					(int fd, int index, int verbose, s32 fref, s32 fsamp);
int	dsi_count_boards(void);
int	dsi_coupling_mode_ac			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_COUPLING_MODE_AC
int	dsi_coupling_mode_dc			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_COUPLING_MODE_DC

int	dsi_data_format					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_DATA_FORMAT
int	dsi_data_width					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_DATA_WIDTH

int	dsi_external_clock_source		(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_EXT_CLK_SRC
int	dsi_external_trigger			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_EXT_TRIG

int	dsi_fref_compute				(int fd, int index, int verbose, s32* fref);
int	dsi_fsamp_compute				(int fd, int index, int verbose, int options, s32 fref, s32* fsamp, s32* nvco, s32* nref, s32* nrate, s32* ndiv);
int	dsi_fsamp_report				(int fd, int index, int verbose, int chan, s32* fref, s32* sps);
int	dsi_fsamp_report_all			(int fd, int index, int verbose, s32* fref);
int	dsi_fsamp_validate				(int fd, int index, int verbose, s32* fsamp);

int	dsi_gps_enable					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_GPS_ENABLE
int	dsi_gps_locked					(int fd, int index, int verbose, s32* get);				// DSI_IOCTL_GPS_LOCKED
int	dsi_gps_polarity				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_GPS_POLARITY
int	dsi_gps_rate_locked				(int fd, int index, int verbose, s32* get);				// DSI_IOCTL_GPS_RATE_LOCKED
int	dsi_gps_target_rate				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_GPS_TARGET_RATE
int	dsi_gps_tolerance				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_GPS_TOLERANCE

int	dsi_id_board					(int fd, int index, const char* desc);
int	dsi_init_mode					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_INIT_MODE
int	dsi_initialize					(int fd, int index, int verbose);						// DSI_IOCTL_INITIALIZE
int	dsi_irq_sel						(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_IRQ_SEL

int	dsi_pll_ref_freq_hz				(int fd, int index, int verbose, s32* get);				// DSI_IOCTL_PLL_REF_FREQ_HZ

int	dsi_query						(int fd, int index, int verbose, s32 query, s32* get);	// DSI_IOCTL_QUERY
int	dsi_query_fgen_range			(int fd, int index, int verbose, s32* min, s32* max);
int	dsi_query_fref_default			(int fd, int index, int verbose, s32* def);
int	dsi_query_frequency_control		(int fd, int index, int verbose, s32* pll);
int	dsi_query_fsamp_default			(int fd, int index, int verbose, s32* def);
int	dsi_query_fsamp_range			(int fd, int index, int verbose, s32* min, s32* max);
int	dsi_query_model					(int fd, int index, int verbose, s32* model);
int	dsi_query_ndiv_range			(int fd, int index, int verbose, s32* min, s32* max);
int	dsi_query_nrate_range			(int fd, int index, int verbose, s32* min, s32* max);
int	dsi_query_nref_range			(int fd, int index, int verbose, s32* min, s32* max);
int	dsi_query_nvco_range			(int fd, int index, int verbose, s32* min, s32* max);

int	dsi_rate_div_0_ndiv				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_DIV_0_NDIV
int	dsi_rate_div_1_ndiv				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_DIV_1_NDIV
int	dsi_rate_gen_a_nrate			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_A_NRATE
int	dsi_rate_gen_a_nref				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_A_NREF
int	dsi_rate_gen_a_nvco				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_A_NVCO
int	dsi_rate_gen_b_nrate			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_B_NRATE
int	dsi_rate_gen_b_nref				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_B_NREF
int	dsi_rate_gen_b_nvco				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RATE_GEN_B_NVCO
int	dsi_rate_gen_x_ndiv				(int fd, int index, int verbose, int gen, s32 set, s32* get);
int	dsi_rate_gen_x_ndiv_all			(int fd, int index, int verbose, s32 set);
int	dsi_rate_gen_x_nrate			(int fd, int index, int verbose, int gen, s32 set, s32* get);
int	dsi_rate_gen_x_nrate_all		(int fd, int index, int verbose, s32 set);
int	dsi_rate_gen_x_nref				(int fd, int index, int verbose, int gen, s32 set, s32* get);
int	dsi_rate_gen_x_nref_all			(int fd, int index, int verbose, s32 set);
int	dsi_rate_gen_x_nvco				(int fd, int index, int verbose, int gen, s32 set, s32* get);
int	dsi_rate_gen_x_nvco_all			(int fd, int index, int verbose, s32 set);

int	dsi_reg_list					(int fd, int detail);
int	dsi_reg_mod						(int fd, u32 reg, u32 value, u32 mask);					// DSI_IOCTL_REG_MOD
int	dsi_reg_read					(int fd, u32 reg, u32* value);							// DSI_IOCTL_REG_READ
int	dsi_reg_write					(int fd, u32 reg, u32 value);							// DSI_IOCTL_REG_WRITE

int	dsi_tx_inv_ext_trigger			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_TA_INV_EXT_TRIGGER
int	dsi_rx_io_abort					(int fd, int index, int verbose, s32* get);				// DSI_IOCTL_RX_IO_ABORT
int	dsi_rx_io_mode					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RX_IO_MODE
int	dsi_rx_io_overflow				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RX_IO_OVERFLOW
int	dsi_rx_io_timeout				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RX_IO_TIMEOUT
int	dsi_rx_io_underflow				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_RX_IO_UNDERFLOW

int	dsi_sw_sync						(int fd, int index, int verbose);						// DSI_IOCTL_SW_SYNC
int	dsi_sw_sync_mode				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_SW_SYNC_MODE

int	dsi_ta_inv_ext_trigger			(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_TA_INV_EXT_TRIGGER
int	dsi_thres_flag_cbl				(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_THRES_FLAG_CBL

int	dsi_wait_cancel					(int fd, int index, int verbose, gsc_wait_t* wait);		// DSI_IOCTL_WAIT_CANCEL
int	dsi_wait_event					(int fd, gsc_wait_t* wait);								// DSI_IOCTL_WAIT_EVENT
int	dsi_wait_status					(int fd, int index, int verbose, gsc_wait_t* wait);		// DSI_IOCTL_WAIT_STATUS

int	dsi_xcvr_type					(int fd, int index, int verbose, s32 set, s32* get);	// DSI_IOCTL_XCVR_TYPE

const gsc_reg_def_t*	dsi_reg_get_def_id		(u32 reg);
const gsc_reg_def_t*	dsi_reg_get_def_index	(int index);
const char*				dsi_reg_get_desc		(u32 reg);
const char*				dsi_reg_get_name		(u32 reg);



#endif
