// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_sleep.c $
// $Rev: 23594 $
// $Date: 2013-10-14 11:44:43 -0500 (Mon, 14 Oct 2013) $

#include <unistd.h>

#include "gsc_utils.h"



//*****************************************************************************
void os_sleep_ms(int ms)
{
	for (; ms > 100; ms -= 100)
		usleep(100000);

	for (; ms > 10; ms -= 10)
		usleep(10000);

	if (ms > 0)
		usleep((long) ms * 1000);
	else
		usleep(1000);
}


