// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_reg.c $
// $Rev: 34228 $
// $Date: 2016-01-08 17:53:09 -0600 (Fri, 08 Jan 2016) $

#include <stdio.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



// #defines *******************************************************************

#define	_GSC_REG(a)					"GSC " #a, DSI_GSC_##a
#define	_FIELD(b,e,eol,lst,desc)	gsc_reg_field_show(width + 10, 20, value,(b),(e),(eol),(lst),(desc))



//*****************************************************************************
static int _0x00_decode_bctlr(int fd, int supported, u32 value, int width)
{
	static const char*	acal[]	= { "Done",				"In Progress"		};
	static const char*	armx[]	= { "Unarmed",			"Armed"				};
	static const char*	clr[]	= { "Sync",				"Clear Buffer"		};
	static const char*	cupl[]	= { "DC",				"AC"				};
	static const char*	ext[]	= { "Group 0 Source",	"Rate generator A"	};
	static const char*	filt[]	= { "High Frequency",	"Low Frequency"		};
	static const char*	init[]	= { "Idle",				"Initializing"		};
	static const char*	irqf[]	= { "Inactive",			"Active"			};
	static const char*	offb[]	= { "Twos Compliment",	"Offset Binary"		};
	static const char*	pass[]	= { "Fail",				"Pass"				};
	static const char*	redy[]	= { "Not Ready",		"Ready"				};
	static const char*	syn0[]	= { "Synchronous",		"Asynchronous"		};
	static const char*	syn1[]	= { "Asynchronous",		"Synchronous"		};
	static const char*	sync[]	= { "Idle",				"Active"			};
	static const char*	targ[]	= { "Target",			"Initiator"			};
	static const char*	tflg[]	= { "Disabled",			"Enabled"			};
	static const char*	thrf[]	= { "Clear",			"Asserted"			};
	static const char*	ttl[]	= { "LVDS",				"TTL"				};
	static const char*	yes[]	= { "No",				"Yes"				};

	static const char*	ainm[]	=
	{
		"Differential",
		"Reserved",
		"Zero Test",
		"Vref Test"
	};

	static const char*	irq[]	=
	{
		"Initialization Completed",
		"Auto Cal Done",
		"Channels Ready",
		"Data Buffer Threshold flag, Low-to-High",
		"Data Buffer Threshold flag, High-to-Low",
		"Reserved",
		"Reserved",
		"Reserved"
	};

	static const char*	volt[]	=
	{
		"+-2.5 Volts",
		"+-2.5 Volts",
		"+-5 Volts",
		"+-10 Volts"
	};

	s32	coupling;
	s32	query;
	s32	v;

	// D31-D24
	_FIELD(31, 24, 1, NULL,	"Reserved");

	// D23
	dsi_query(fd, -1, 0, DSI_QUERY_D23_RATE_OUT, &query);
	dsi_query(fd, -1, 0, DSI_QUERY_D23_INV_EXT_TRIG, &v);

	if (v)
		_FIELD(23, 23, 1, yes,	"Invert Ext Trigger");
	else if (query)
		_FIELD(23, 23, 1, yes,	"ADC Rate Out");
	else
		_FIELD(23, 23, 1, NULL,	"Reserved");

	// D22
	dsi_query(fd, -1, 0, DSI_QUERY_D22_COUPLING, &coupling);
	dsi_query(fd, -1, 0, DSI_QUERY_D22_THR_FLAG, &query);

	if (coupling)
		_FIELD(22, 22, 1, cupl,	"Input Coupling");
	else if (query)
		_FIELD(22, 22, 1, tflg,	"Threshold Flag Out");
	else
		_FIELD(22, 22, 1, NULL,	"Reserved");

	// D21
	dsi_query(fd, -1, 0, DSI_QUERY_D21_EXT_TRIG, &query);

	if (query)
		_FIELD(21, 21, 1, armx,	"Arm Ext. Trigger");
	else
		_FIELD(21, 21, 1, NULL,	"Reserved");

	// D20
	dsi_query(fd, -1, 0, DSI_QUERY_D20_XCVR, &query);

	if (query)
		_FIELD(20, 20, 1, ttl,	"TTL Ext Sync I/O");
	else
		_FIELD(20, 20, 1, NULL,	"Reserved");

	// D19
	dsi_query(fd, -1, 0, DSI_QUERY_D19_FREQ_FILT, &query);

	if (query)
		_FIELD(19, 19, 1, filt,	"Low Freq Filter");
	else
		_FIELD(19, 19, 1, NULL,	"Reserved");

	// D18-D17
	_FIELD(18, 18, 1, ext,	"Ext. Clock Source");
	_FIELD(17, 17, 1, clr,	"Clear Buf On Sync");

	// D16
	dsi_query(fd, -1, 0, DSI_QUERY_D16_SYNC_1, &query);

	if (query)
		_FIELD(16, 16, 1, syn1, "Sync Scan");
	else
		_FIELD(16, 16, 1, syn0, "Async Scan");

	// D15-D13
	_FIELD(15, 15, 1, init,	"Initialize");
	_FIELD(14, 14, 1, thrf,	"Buf Thresh Flag");
	_FIELD(13, 13, 1, redy,	"Channels Ready");

	// D12
	dsi_query(fd, -1, 0, DSI_QUERY_AUTO_CAL_MS, &query);

	if (query)
		_FIELD(12, 12, 1, pass,	"Auto Cal Pass");
	else
		_FIELD(12, 12, 1, NULL,	"Reserved");

	// D11-D8
	_FIELD(11, 11, 1, irqf,	"Int Request Flag");
	_FIELD(10,  8, 1, irq,	"Interrupt");

	// D7
	dsi_query(fd, -1, 0, DSI_QUERY_AUTO_CAL_MS, &query);

	if (query)
		_FIELD(7, 7, 1, acal,	"Auto Cal");
	else
		_FIELD(7, 7, 1, NULL,	"Reserved");

	// D6-D4
	_FIELD( 6,  6, 1, sync,	"Software Sync");
	_FIELD( 5,  5, 1, targ,	"Initiator");
	_FIELD( 4,  4, 1, offb,	"Offset Binary");

	// D3-D2
	dsi_query(fd, -1, 0, DSI_QUERY_D2_3_RANGE, &query);

	if (query)
		_FIELD(3, 2, 1, volt,	"Range");
	else
		_FIELD(3, 2, 1, NULL,	"Reserved");

	// D1-D0
	dsi_query(fd, -1, 0, DSI_QUERY_D0_1_IN_MODE, &query);

	if (query)
		_FIELD(1, 0, 1, ainm,	"Analog Input Mode");
	else
		_FIELD(1, 0, 1, NULL,	"Reserved");

	return(0);
}



//*****************************************************************************
static int _0x04_decode_rcar(int fd, int supported, u32 value, int width)
{
	s32	pll;
	s32	qty;
	int	ret;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);

	if (supported)
	{
		// Single Rate Generator boards have a slightly different register here.
		ret	= (qty >= 2) ? 1 : 0;
	}
	else if (pll)
	{
		ret	= 0;
		_FIELD(31, 26, 1, NULL,	"Reserved");

		_FIELD(25, 16, 0, NULL,	"Ref Factor");
		v	= GSC_FIELD_DECODE(value, 25, 16);
		printf("%ld  (Nref = %ld)\n", (long) v, (long) v);

		_FIELD(15, 10, 1, NULL,	"Reserved");

		_FIELD( 9,  0, 0, NULL,	"VCO Factor");
		v	= GSC_FIELD_DECODE(value, 9, 0);
		printf("%ld  (Nvco = %ld)\n", (long) v, (long) v);
	}
	else
	{
		ret	= 0;
		_FIELD(31, 17, 1, NULL,	"Reserved");

		_FIELD(16,  0, 0, NULL,	"Rate Control");
		v	= GSC_FIELD_DECODE(value, 16, 0);
		printf("%ld  (Nrate = %ld)\n", (long) v, (long) v);
	}

	return(ret);
}



//*****************************************************************************
static int _0x04_decode_nref(int fd, int supported, u32 value, int width)
{
	s32	pll;
	s32	qty;
	int	ret;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);

	if (supported)
	{
		// Single rate generator, Non PLL boards use a different register.
		ret	= ((qty == 1) && (pll)) ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 10, 1, NULL,	"Reserved");

		_FIELD( 9,  0, 0, NULL,	"PLL Nref Factor");
		v	= GSC_FIELD_DECODE(value, 9, 0);
		printf("%ld  )Nref = %ld)\n", (long) v, (long) v);
	}

	return(ret);
}



//*****************************************************************************
static int _0x08_decode_rcbr(int fd, int supported, u32 value, int width)
{
	int	ret;

	ret	= _0x04_decode_rcar(fd, supported, value, width);
	return(ret);
}



//*****************************************************************************
static int _0x08_decode_nvco(int fd, int supported, u32 value, int width)
{
	s32	pll;
	s32	qty;
	int	ret;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);

	if (supported)
	{
		// Single rate generator, Non PLL boards use a different register.
		ret	= ((qty == 1) && (pll)) ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 10, 1, NULL,	"Reserved");

		_FIELD( 9,  0, 0, NULL,	"PLL VCO Factor");
		v	= GSC_FIELD_DECODE(value, 9, 0);
		printf("%ld  (Nvco = %ld)\n", (long) v, (long) v);
	}

	return(ret);
}



//*****************************************************************************
static int _0x08_decode_rcr(int fd, int supported, u32 value, int width)
{
	s32	pll;
	s32	qty;
	int	ret;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &qty);

	if (supported)
	{
		// Single rate generator, PLL boards use a different register.
		ret	= ((qty == 1) && (pll == 0)) ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 17, 1, NULL,	"Reserved");

		_FIELD(16,  0, 0, NULL,	"Rate Control");
		v	= GSC_FIELD_DECODE(value, 16, 0);
		printf("%ld  (Nrate = %ld)\n", (long) v, (long) v);
	}

	return(ret);
}



//*****************************************************************************
static int _0x0C_decode_rar(int fd, int supported, u32 value, int width)
{
	static const char*	rat1[]	=
	{
		"Rate Generator A",
		"Rate Generator B",
		"Reserved",
		"Reserved",
		"External Sample Clock",
		"Direct External Sample Clock",
		"Disabled",
		"Disabled"
	};

	static const char*	rat2[]	=
	{
		"Internal Rate Generator",
		"Reserved",
		"Reserved",
		"Reserved",
		"External Sample Clock",
		"Direct External Sample Clock",
		"Disabled",
		"Disabled"
	};

	s32	groups;

	dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);

	// D31-D16
	_FIELD(31, 16, 1, NULL,	"Reserved");

	// D15-D12

	if (groups > 2)
		_FIELD(15, 12,  1, rat2,	"Group 3");
	else
		_FIELD(15, 12,  1, NULL,	"Reserved");

	// D11-D8

	if (groups > 2)
		_FIELD(11,  8,  1, rat2,	"Group 2");
	else
		_FIELD(11,  8,  1, NULL,	"Reserved");

	// D7-D4
	_FIELD( 7,  4,  1, rat1,	"Group 1");

	// D3-D0
	_FIELD( 3,  0,  1, rat1,	"Group 0");

	return(0);
}



//*****************************************************************************
static int _0x10_decode_rdr(int fd, int supported, u32 value, int width)
{
	s32	qty;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &qty);

	// D31-D16
	_FIELD(31, 16, 1, NULL,	"Reserved");

	// D15-D12

	if (qty >= 2)
	{
		_FIELD(15, 8,  0, NULL,	"Rate Divisor 1");
		v	= GSC_FIELD_DECODE(value, 15, 8);
		printf("%ld  (Ndiv = %ld)\n", (long) v, (long) v);
	}
	else
	{
		_FIELD(15, 8,  1, NULL,	"Reserved");
	}

	// D7-D0

	if (qty > 1)
	{
		_FIELD(7, 0,  0, NULL,	"Rate Divisor 0");
		v	= GSC_FIELD_DECODE(value, 7, 0);
		printf("%ld  (Ndiv = %ld)\n", (long) v, (long) v);
	}
	else
	{
		_FIELD(7, 0,  0, NULL,	"Rate Divisor");
		v	= GSC_FIELD_DECODE(value, 7, 0);
		printf("%ld  (Ndiv = %ld)\n", (long) v, (long) v);
	}

	return(0);
}



//*****************************************************************************
static int _0x14_decode_icmr(int fd, int supported, u32 value, int width)
{
	static const char*	_dc_ac[]	= { "DC",	"AC"	};

	s32	icmr;
	int	ret;

	dsi_query(fd, -1, 0, DSI_QUERY_REG_ICMR, &icmr);

	if (supported)
	{
		ret	= icmr ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 12, 1, NULL,		"Reserved"				);
		_FIELD(11, 11, 1, _dc_ac,	"Channel 11 Coupling"	);
		_FIELD(10, 10, 1, _dc_ac,	"Channel 10 Coupling"	);
		_FIELD( 9,  9, 1, _dc_ac,	"Channel  9 Coupling"	);
		_FIELD( 8,  8, 1, _dc_ac,	"Channel  8 Coupling"	);
		_FIELD( 7,  7, 1, _dc_ac,	"Channel  7 Coupling"	);
		_FIELD( 6,  6, 1, _dc_ac,	"Channel  6 Coupling"	);
		_FIELD( 5,  5, 1, _dc_ac,	"Channel  5 Coupling"	);
		_FIELD( 4,  4, 1, _dc_ac,	"Channel  4 Coupling"	);
		_FIELD( 3,  3, 1, _dc_ac,	"Channel  3 Coupling"	);
		_FIELD( 2,  2, 1, _dc_ac,	"Channel  2 Coupling"	);
		_FIELD( 1,  1, 1, _dc_ac,	"Channel  1 Coupling"	);
		_FIELD( 0,  0, 1, _dc_ac,	"Channel  0 Coupling"	);
	}

	return(ret);
}



//*****************************************************************************
static int _0x18_decode_prfr(int fd, int supported, u32 value, int width)
{
	s32	pll;
	int	ret;

	dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);

	if (supported)
	{
		ret	= pll ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 0, 0, NULL,	"PLL Ref Freq");
		printf("~ %ld Hz\n", (long) value);
	}

	return(ret);
}



//*****************************************************************************
static int _0x1C_decode_gsr(int fd, int supported, u32 value, int width)
{
	static const char*	enab[]	= { "Disabled",		"Enabled"	};
	static const char*	lock[]	= { "Not Locked",	"Locked"	};
	static const char*	negs[]	= { "Positive",		"Negative"	};
	static const char*	toll[]	= { "Narrow",		"Wide"		};

	s32	gps;
	int	ret;
	u32	v;

	dsi_query(fd, -1, 0, DSI_QUERY_GPS_PRESENT, &gps);

	if (supported)
	{
		ret	= gps ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 26, 1, NULL,	"Reserved");
		_FIELD(25, 25, 1, lock,	"Sample Rate Lock");
		_FIELD(24, 24, 1, lock,	"GPS Lock");
		_FIELD(23, 23, 1, NULL,	"Reserved");
		_FIELD(22, 22, 1, negs,	"Negative GPS Sync");
		_FIELD(21, 21, 1, toll,	"Wide Tollerance");
		_FIELD(20, 20, 1, enab,	"Enable GPS Sync");

		_FIELD(19, 0, 0, NULL,	"Target Sample Rate");
		v	= GSC_FIELD_DECODE(value, 19, 0);
		printf("%ld S/S\n", (long) v);
	}

	return(ret);
}



//*****************************************************************************
static int _0x1C_decode_rfcr(int fd, int supported, u32 value, int width)
{
	static const char*	volt[]	=
	{
		"+-2.5 Volts",
		"+-2.5 Volts",
		"+-5 Volts",
		"+-10 Volts"
	};

	static const char*	enab[]	= { "Disabled",			"Enabled"	};
	static const char*	filt[]	= { "High Frequency",	"Low Frequency"		};

	s32	reg;
	int	ret;

	dsi_query(fd, -1, 0, DSI_QUERY_RFCR, &reg);

	if (supported)
	{
		ret	= reg ? 1 : 0;
	}
	else
	{
		ret	= 0;
		_FIELD(31, 30, 1, volt,	"Chan 28-31 Range");
		_FIELD(29, 28, 1, volt,	"Chan 24-17 Range");
		_FIELD(27, 26, 1, volt,	"Chan 20-23 Range");
		_FIELD(25, 24, 1, volt,	"Chan 16-19 Range");
		_FIELD(23, 22, 1, volt,	"Chan 12-15 Range");
		_FIELD(21, 20, 1, volt,	"Chan  8-11 Range");
		_FIELD(19, 18, 1, volt,	"Chan  4- 7 Range");
		_FIELD(17, 16, 1, volt,	"Chan  0- 3 Range");
		_FIELD(15, 10, 1, NULL,	"Reserved");
		_FIELD( 9,  9, 1, enab,	"Sel Range Control");
		_FIELD( 8,  8, 1, enab,	"Sel Filter Control");
		_FIELD( 7,  7, 1, filt,	"Chan 28-31 Filter");
		_FIELD( 6,  6, 1, filt,	"Chan 24-17 Filter");
		_FIELD( 5,  5, 1, filt,	"Chan 20-23 Filter");
		_FIELD( 4,  4, 1, filt,	"Chan 16-19 Filter");
		_FIELD( 3,  3, 1, filt,	"Chan 12-15 Filter");
		_FIELD( 2,  2, 1, filt,	"Chan  8-11 Filter");
		_FIELD( 1,  1, 1, filt,	"Chan  4- 7 Filter");
		_FIELD( 0,  0, 1, filt,	"Chan  0- 3 Filter");
	}

	return(ret);
}



//*****************************************************************************
static int _0x20_decode_ibcr(int fd, int supported, u32 value, int width)
{
	static const char*	size[]	=
	{
		"16-bits",
		"18-bits",
		"20-bits",
		"24-bits"
	};

	static const char*	cler[]	= { "No",		"Clearing"	};
	static const char*	enab[]	= { "Enabled",	"Disabled"	};
	static const char*	nyes[]	= { "No",		"Yes"		};

	u32	v;

	_FIELD(31, 26, 1, NULL,	"Reserved");
	_FIELD(25, 25, 1, nyes,	"Buffer Underflow");
	_FIELD(24, 24, 1, nyes,	"Buffer Overflow");
	_FIELD(23, 22, 1, NULL,	"Reserved");
	_FIELD(21, 20, 1, size,	"Data Width");
	_FIELD(19, 19, 1, cler,	"Clear Buffer");
	_FIELD(18, 18, 1, enab,	"Disable Buf Input");

	_FIELD(17,  0, 0, NULL,	"Threshold Level");
	v	= GSC_FIELD_DECODE(value, 17, 0);
	printf("%ld Samples\n", (long) v);
	return(0);
}



//*****************************************************************************
static int _0x24_decode_bcfgr(int fd, int supported, u32 value, int width)
{
	static const char*	freq[]	=
	{
		"270KHz",
		"Reserved",
		"Reserved",
		"Reserved"
	};

	static const char*	range[]	=
	{
		"+-10V",
		"+-5V",
		"+-2.5V",
		"Reserved"
	};

	static const char*	pllr[]	= { "No (Legacy)",	"Yes"			};
	static const char*	yesn[]	= { "No",			"Yes"			};

	char	buf[32];
	s32		d16;
	s32		d17;
	s32		device;
	s32		filt;
	s32		noise;
	s32		pll;
	s32		power;
	s32		rio;
	s32		temp;
	u32		v;

	// D31-D23
	_FIELD(31, 23, 1, NULL,	"Reserved");

	// D22
	dsi_query(fd, -1, 0, DSI_QUERY_D22_24DSI6LN, &noise);

	if (noise)
		_FIELD(22, 22, 1, yesn,	"24DSI6LN");
	else
		_FIELD(22, 22, 1, NULL,	"Reserved");

	// D21
	dsi_query(fd, -1, 0, DSI_QUERY_D21_EXT_TEMP, &temp);

	if (temp)
		_FIELD(21, 21, 1, yesn,	"Ext Temp Version");
	else
		_FIELD(21, 21, 1, NULL,	"Reserved");

	// D20-D19
	dsi_query(fd, -1, 0, DSI_QUERY_D19_20_FILT, &filt);

	if (filt)
	{
		// D20-D19
		_FIELD(20, 19, 1, freq,	"Image Freq Filter");
	}
	else
	{
		// D20
		dsi_query(fd, -1, 0, DSI_QUERY_D20_LOW_POWER, &power);

		if (power)
			_FIELD(20, 20, 1, yesn,	"Low Power Version");
		else
			_FIELD(20, 20, 1, NULL,	"Reserved");

		// D19
		dsi_query(fd, -1, 0, DSI_QUERY_D19_EXT_TEMP, &temp);

		if (temp)
			_FIELD(19, 19, 1, yesn,	"Ext Temp Version");
		else
			_FIELD(19, 19, 1, NULL,	"Reserved");
	}

	// D18-17
	dsi_query(fd, -1, 0, DSI_QUERY_D17_18_RANGE, &filt);

	if (filt)
	{
		// D18-17
		_FIELD(18, 17, 1, range,	"Voltage Range");
	}
	else
	{
		// D18
		dsi_query(fd, -1, 0, DSI_QUERY_D18_CF_FILT, &filt);
		dsi_query(fd, -1, 0, DSI_QUERY_D18_RIO, &rio);

		if (rio)
			_FIELD(18, 18, 1, yesn,	"Rear I/O Present");
		else if (filt)
			_FIELD(18, 18, 1, yesn,	"Custom Filter");
		else
			_FIELD(18, 18, 1, NULL,	"Reserved");

		// D17
		dsi_query(fd, -1, 0, DSI_QUERY_D17_CHANNELS, &d17);
		sprintf(buf, "%d Input Channels", (int) d17);

		if (d17)
			_FIELD(17, 17, 1, yesn,	buf);
		else
			_FIELD(17, 17, 1, NULL,	"Reserved");
	}

	// D16
	dsi_query(fd, -1, 0, DSI_QUERY_D16_CHANNELS, &d16);
	sprintf(buf, "%d Input Channels", (int) d16);

	if (d16)
		_FIELD(16, 16, 1, yesn,	buf);
	else
		_FIELD(16, 16, 1, NULL,	"Reserved");

	// D15
	dsi_query(fd, -1, 0, DSI_QUERY_D15_PLL, &pll);

	if (pll)
		_FIELD(15, 15, 1, pllr,	"PLL Rate Gen");

	// D15-D0
	dsi_query(fd, -1, 0, DSI_QUERY_DEVICE_TYPE, &device);

	switch (device)
	{
		default:

			printf("INTERNAL ERROR: %s, line %d\n", __FILE__, __LINE__);
			break;

		case GSC_DEV_TYPE_24DSI6:
		case GSC_DEV_TYPE_24DSI12:

			_FIELD(15, 0, 0, NULL,	"Firmware Revision");
			v	= GSC_FIELD_DECODE(value, 15, 0);
			printf("0x%04lX\n", (long) v);
			break;

		case GSC_DEV_TYPE_24DSI32:

			if (pll)
			{
				// D14-D12
				_FIELD(14, 12, 1, NULL,	"Reserved");
			}
			else
			{
				// D15-D12
				_FIELD(15, 12, 1, NULL,	"Reserved");
			}

			// D11-D0
			_FIELD(11, 0, 0, NULL,	"Firmware Revision");
			v	= GSC_FIELD_DECODE(value, 11, 0);
			printf("0x%03lX\n", (long) v);
	}

	return(0);
}



//*****************************************************************************
static int _0x28_decode_bsr(int fd, int supported, u32 value, int width)
{
	u32	v;

	_FIELD(31, 19, 1, NULL,	"Reserved");

	_FIELD(18,  0, 0, NULL,	"Sample Count");
	v	= GSC_FIELD_DECODE(value, 18, 0);
	printf("%ld Samples\n", (long) v);
	return(0);
}



//*****************************************************************************
static int _0x30_decode_ibdr(int fd, int supported, u32 value, int width)
{

	// D31-D29
	_FIELD(31, 29, 1, NULL,	"Reserved");

	// D28-D24
	_FIELD(28, 24, 1, NULL,	"Channel Tag");

	// D23-D0
	_FIELD(23, 0, 1, NULL,	"Channel Data");
	return(0);
}



// variables *******************************************************************

static gsc_reg_def_t	_gsc[]	=
{
	// name	reg			err	value	ask_support	decode				desc
	{ _GSC_REG(BCTLR),	0,	0,		0,			_0x00_decode_bctlr,	"Board Control Register"			},
	{ _GSC_REG(RCAR),	0,	0,		1,			_0x04_decode_rcar,	"Rate Control A Register"			},	// 24DSI12
	{ _GSC_REG(NREFCR),	0,	0,		1,			_0x04_decode_nref,	"PLL Nref Control Register"			},	// 24DSI32
	{ _GSC_REG(RCBR),	0,	0,		1,			_0x08_decode_rcbr,	"Rate Control B Register"			},	// 24DSI12
	{ _GSC_REG(NVCOCR),	0,	0,		1,			_0x08_decode_nvco,	"PLL Nvco Control Register"			},	// 24DSI32
	{ _GSC_REG(RCR),	0,	0,		1,			_0x08_decode_rcr,	"Rate Control Register"				},	// 24DSI32
	{ _GSC_REG(RAR),	0,	0,		0,			_0x0C_decode_rar,	"Rate Assignments Register"			},
	{ _GSC_REG(RDR),	0,	0,		0,			_0x10_decode_rdr,	"Rate Divisors Register"			},
	{ _GSC_REG(ICMR),	0,	0,		1,			_0x14_decode_icmr,	"Input Coupling Mode Register"		},
	{ _GSC_REG(PRFR),	0,	0,		1,			_0x18_decode_prfr,	"PLL Reference Frequency Register"	},
	{ _GSC_REG(GSR),	0,	0,		1,			_0x1C_decode_gsr,	"GPS Synchronization Register"		},	// 24DSI12
	{ _GSC_REG(RFCR),	0,	0,		1,			_0x1C_decode_rfcr,	"Range and Filter Control Register"	},	// 24DSI32
	{ _GSC_REG(IBCR),	0,	0,		0,			_0x20_decode_ibcr,	"Input Buffer Control Register"		},
	{ _GSC_REG(BCFGR),	0,	0,		0,			_0x24_decode_bcfgr,	"Board Configuration Register"		},
	{ _GSC_REG(BSR),	0,	0,		0,			_0x28_decode_bsr,	"Buffer Size Register"				},
	{ _GSC_REG(IDBR),	0,	0,		0,			_0x30_decode_ibdr,	"Input Data Buffer Register"		},

	{ NULL }
};



//*****************************************************************************
static const gsc_reg_def_t* _find_reg(u32 reg, const gsc_reg_def_t* list)
{
	const gsc_reg_def_t*	def	= NULL;
	int						i;

	for (i = 0; list[i].name; i++)
	{
		if (reg == list[i].reg)
		{
			def	= &list[i];
			break;
		}
	}

	return(def);
}



/*****************************************************************************
*
*	Function:	dsi_reg_get_def_id
*
*	Purpose:
*
*		Retrieve the register definition structure given the register id.
*
*	Arguments:
*
*		reg		The id of the register to access.
*
*	Returned:
*
*		NULL	The register id wasn't found.
*		else	A pointer to the register definition.
*
*****************************************************************************/

const gsc_reg_def_t* dsi_reg_get_def_id(u32 reg)
{
	const gsc_reg_def_t*	def;

	def	= _find_reg(reg, _gsc);
	return(def);
}



/*****************************************************************************
*
*	Function:	dsi_reg_get_def_index
*
*	Purpose:
*
*		Retrieve the register definition structure based on an index.
*
*	Arguments:
*
*		index	The index of the register to access.
*
*	Returned:
*
*		NULL	The index doesn't correspond to a known register.
*		else	A pointer to the register definition.
*
*****************************************************************************/

const gsc_reg_def_t* dsi_reg_get_def_index(int index)
{
	const gsc_reg_def_t*	def;

	if (index < 0)
		def	= NULL;
	else if (index >= ((int) ARRAY_ELEMENTS(_gsc) - 1))
		def	= NULL;
	else
		def	= &_gsc[index];

	return(def);
}



/*****************************************************************************
*
*	Function:	dsi_reg_get_desc
*
*	Purpose:
*
*		Retrieve the description of the specified register.
*
*	Arguments:
*
*		reg		The register whose description is desired.
*
*	Returned:
*
*		!NULL	The register's name.
*
*****************************************************************************/

const char* dsi_reg_get_desc(u32 reg)
{
	const gsc_reg_def_t*	def;
	const char*				desc;

	def	= _find_reg(reg, _gsc);

	if (def)
		desc	= def->desc;
	else
		desc	= "UNKNOWN";

	return(desc);
}



/*****************************************************************************
*
*	Function:	dsi_reg_get_name
*
*	Purpose:
*
*		Retrieve the name of the specified register.
*
*	Arguments:
*
*		reg		The register whose name is desired.
*
*	Returned:
*
*		!NULL	The register's name.
*
*****************************************************************************/

const char* dsi_reg_get_name(u32 reg)
{
	const gsc_reg_def_t*	def;
	const char*				name;

	def	= _find_reg(reg, _gsc);

	if (def)
		name	= def->name;
	else
		name	= "UNKNOWN";

	return(name);
}



/*****************************************************************************
*
*	Function:	dsi_reg_list
*
*	Purpose:
*
*		List the GSC registers and their values.
*
*	Arguments:
*
*		fd		The handle to access the device.
*
*		detail	List the register details?
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
*****************************************************************************/

int dsi_reg_list(int fd, int detail)
{
	int	errs;

	errs	= gsc_reg_list(fd, _gsc, detail, dsi_reg_read);
	return(errs);
}



