// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/linux/os_util_time.c $
// $Rev: 33904 $
// $Date: 2015-10-01 11:42:25 -0500 (Thu, 01 Oct 2015) $

#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include "gsc_utils.h"




//*****************************************************************************
size_t os_time_delta_ms(void)
{
	static	int				started	= 0;
	static	struct timeval	start;

	size_t					delta;
	size_t					sec;
	struct timeval			tv;
	size_t					us;

	if (started == 0)
	{
		gettimeofday(&start, NULL);
		started	= 1;
	}

	gettimeofday(&tv, NULL);
	sec		= tv.tv_sec - start.tv_sec;
	us		= 1000000L + tv.tv_usec - start.tv_usec;
	delta	= (sec * 1000) + (us / 1000);
	return(delta);
}


