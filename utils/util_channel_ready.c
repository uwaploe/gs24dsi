// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_channel_ready.c $
// $Rev: 25252 $
// $Date: 2014-02-26 10:58:19 -0600 (Wed, 26 Feb 2014) $

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/*****************************************************************************
*
*	Function:	dsi_channel_ready
*
*	Purpose:
*
*		Provide a visual feedback about the Channel Ready bit.
*
*	Arguments:
*
*		fd		The handle to the board.
*
*		index	The index of the board.
*
*		verbose	Work verbosely? If not, then quietly.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
*****************************************************************************/

int dsi_channel_ready(int fd, int index, int verbose)
{
	int	errs;

	if (verbose)
		gsc_label_index("Channels Ready", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_CHANNELS_READY, NULL);

	if (verbose)
		printf("%s\n", errs ? "FAIL <---" : "PASS");

	return(errs);

}


