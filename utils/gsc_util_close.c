// $URL: http://subversion:8080/svn/gsc/trunk/drivers/gsc_common/utils/gsc_util_close.c $
// $Rev: 33913 $
// $Date: 2015-10-07 14:21:02 -0500 (Wed, 07 Oct 2015) $

#include <fcntl.h>
#include <stdio.h>

#include "gsc_utils.h"



/******************************************************************************
*
*	Function:	gsc_dev_close
*
*	Purpose:
*
*		Perform a close on the device with the specified access handle.
*
*	Arguments:
*
*		index	The index of the board to accessed.
*
*		fd		The file descriptor used to access the device.
*
*	Returned:
*
*		None.
*
******************************************************************************/

void gsc_dev_close(unsigned int index, int fd)
{
	char	buf[80];
	int		i;

	sprintf(buf, "Closing #%u", index);
	gsc_label(buf);
	i	= close(fd);

	if (i == -1)
		printf("FAIL <---\n");
	else
		printf("PASS\n");
}



