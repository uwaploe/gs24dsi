// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_ain_buf_overflow.c $
// $Rev: 32099 $
// $Date: 2015-06-16 16:28:47 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_ain_buf_overflow
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_AIN_BUF_OVERFLOW IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_ain_buf_overflow(int fd, int index, int verbose, s32 set, s32* get)
{
	char	buf[128];
	int		errs	= 0;

	if (verbose)
		gsc_label_index("ADC Buffer Overflow", index);

	if (set == -1)
	{
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &set);
	}
	else
	{
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &set);
		set		= -1;
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &set);
	}

	if (set)
		strcpy(buf, "Yes");
	else
		strcpy(buf, "No");

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


