// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_irq_sel.c $
// $Rev: 32099 $
// $Date: 2015-06-16 16:28:47 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/******************************************************************************
*
*	Function:	dsi_irq_sel
*
*	Purpose:
*
*		Provide a visual wrapper for the DSI_IOCTL_IRQ_SEL IOCTL service.
*
*	Arguments:
*
*		fd		The handle to use to access the driver.
*
*		index	The index of the board to access. Ignore if < 0.
*
*		verbose	Work verbosely? If not, then work quietly.
*
*		set		The setting to apply.
*
*		get		The current setting is recorded here, if not NULL.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
******************************************************************************/

int	dsi_irq_sel(int fd, int index, int verbose, s32 set, s32* get)
{
	char	buf[128];
	int		errs;

	if (verbose)
		gsc_label_index("IRQ Select", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_IRQ_SEL, &set);

	switch (set)
	{
		default:

			sprintf(buf, "INVALID: %ld", (long) set);
			break;

		case DSI_IRQ_INIT_DONE:

			strcpy(buf, "Initialize Done");
			break;

		case DSI_IRQ_AUTO_CAL_DONE:

			strcpy(buf, "Auto-Calibration Done");
			break;

		case DSI_IRQ_CHAN_READY:

			strcpy(buf, "Channel Ready");
			break;

		case DSI_IRQ_AIN_BUF_THRESH_L2H:

			strcpy(buf, "Buffer Threshold Low-to-High");
			break;

		case DSI_IRQ_AIN_BUF_THRESH_H2L:

			strcpy(buf, "Buffer Threshold High-to-Low");
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


