// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_reg_write.c $
// $Rev: 32099 $
// $Date: 2015-06-16 16:28:47 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



/*******************************************************************************
*
*	Function:	dsi_reg_write
*
*	Purpose:
*
*		Provide a silent wrapper for the DSI_IOCTL_REG_WRITE IOCTL service.
*
*	Arguments:
*
*		fd		The handles to access all four channels.
*
*		reg		The register to access.
*
*		value	The value read goes here.
*
*	Returned:
*
*		>= 0	The number of errors encountered here.
*
*******************************************************************************/

int dsi_reg_write(int fd, u32 reg, u32 value)
{
	int			errs;
	int			i;
	gsc_reg_t	parm;

	parm.reg	= reg;
	parm.value	= value;
	parm.mask	= 0;	// Unused here.
	i			= ioctl(fd, DSI_IOCTL_REG_WRITE, &parm);
	errs		= (i == -1) ? 1 : 0;
	return(errs);
}


