// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/utils/util_channel_order.c $
// $Rev: 32099 $
// $Date: 2015-06-16 16:28:47 -0500 (Tue, 16 Jun 2015) $

#include <stdio.h>
#include <string.h>

#include "24dsi_utils.h"
#include "24dsi_dsl.h"



//*****************************************************************************
int	dsi_channel_order(int fd, int index, int verbose, s32 set, s32* get)
{
	char	buf[128];
	int		errs;

	if (verbose)
		gsc_label_index("Channel Order", index);

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_CHANNEL_ORDER, &set);

	switch (set)
	{
		default:

			sprintf(buf, "INVALID: %ld", (long) set);
			break;

		case DSI_CHANNEL_ORDER_ASYNC:

			strcpy(buf, "Asynchronous");
			break;

		case DSI_CHANNEL_ORDER_SYNC:

			strcpy(buf, "Synchronous");
			break;
	}

	if (verbose)
		printf("%s  (%s)\n", errs ? "FAIL <---" : "PASS", buf);

	if (get)
		get[0]	= set;

	return(errs);
}


