// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/thres_flag_cbl.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_THRES_FLAG_CBL,
			/* arg		*/	DSI_THRES_FLAG_CBL_OFF,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x400000,
			/* value	*/	0x000000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_THRES_FLAG_CBL,
			/* arg		*/	DSI_THRES_FLAG_CBL_OUT,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x400000,
			/* value	*/	0x400000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	= service_ioctl_set_get_list(fd, list);

	errs	= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	thres_flag_cbl_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_THRES_FLAG_CBL.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int thres_flag_cbl_test(int fd)
{
	int	errs;
	s32	d22;

	gsc_label("DSI_IOCTL_THRES_FLAG_CBL");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_D22_THR_FLAG, &d22);

	if (errs)
	{
	}
	else if (d22 == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


