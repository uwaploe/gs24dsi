// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/rate_div_0_ndiv.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(
	int	fd,
	s32	mask,
	s32	min,
	s32	max,
	s32	ndiv)
{
	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	min,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	mask,
			/* value	*/	min
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	ndiv,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	mask,
			/* value	*/	ndiv
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	max,
			/* reg		*/	DSI_GSC_RDR,
			/* mask		*/	mask,
			/* value	*/	max
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, s32 mask, s32 min, s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, mask, min, max, min + 1);
	errs	+= _service_subtest(fd, mask, min, max, (min + max) / 2);
	errs	+= _service_subtest(fd, mask, min, max, max - 1);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_div_0_ndiv_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_DIV_0_NDIV.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_div_0_ndiv_test(int fd)
{
	int	errs	= 0;
	s32	qty;
	s32	mask;
	s32	max;
	s32	min;

	gsc_label("DSI_IOCTL_RATE_DIV_0_NDIV");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_RATE_DIV_QTY, &qty);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MASK, &mask);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MAX, &max);
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_NDIV_MIN, &min);

	if (errs)
	{
	}
	else if (qty < 1)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, mask, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


