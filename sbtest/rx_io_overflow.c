// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/rx_io_overflow.c $
// $Rev: 25256 $
// $Date: 2014-02-26 11:01:39 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_CHECK,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	= service_ioctl_set_get_list(fd, list);

	errs	= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rx_io_overflow_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RX_IO_OVERFLOW.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rx_io_overflow_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_RX_IO_OVERFLOW");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


