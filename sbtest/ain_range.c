// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/ain_range.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd, s32 low_power)
{
	static const service_data_t	list_normal[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	DSI_AIN_RANGE_2_5V,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0xC,
			/* value	*/	0x4
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	DSI_AIN_RANGE_5V,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0xC,
			/* value	*/	0x8
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	DSI_AIN_RANGE_10V,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0xC,
			/* value	*/	0xC
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_low_power[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	DSI_AIN_RANGE_2_5V,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0xC,
			/* value	*/	0x4
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE,
			/* arg		*/	DSI_AIN_RANGE_5V,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0xC,
			/* value	*/	0x8
		},

		{ SERVICE_END_LIST }
	};

	int						errs	= 0;
	const service_data_t*	list;

	errs	+= dsi_initialize(fd, -1, 0);

	list	= low_power ? list_low_power : list_normal;
	errs	+= service_ioctl_set_get_list(fd, list);
	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_range_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_RANGE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_range_test(int fd)
{
	s32	d2_3;
	int	errs	= 0;
	s32	low_power;

	gsc_label("DSI_IOCTL_AIN_RANGE");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_LOW_POWER, &low_power);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_D2_3_RANGE, &d2_3);

	if (errs)
	{
	}
	else if (d2_3 == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, low_power);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


