// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/main.h $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi_dsl.h"
#include "24dsi_utils.h"



// #defines *******************************************************************

#define	ARRAY_ELEMENTS(a)	(sizeof((a))/sizeof((a)[0]))



// data types *****************************************************************

typedef enum
{
	SERVICE_END_LIST,	// Ends list of service_data_t structures.
	SERVICE_NORMAL,
	SERVICE_REG_MOD,
	SERVICE_REG_READ,
	SERVICE_REG_SHOW,
	SERVICE_REG_TEST,
	SERVICE_SLEEP,
	SERVICE_IOCTL_GET,
	SERVICE_IOCTL_SET
} service_t;

typedef struct
{
	service_t		service;
	int				cmd;	// IOCTL code
	s32				arg;	// The IOCTL data argument.
	u32				reg;	// The register to access. Use -1 to ignore.
	u32				mask;	// The register bits of interest.
	u32				value;	// The value expected for the bits of interest.
} service_data_t;



// prototypes *****************************************************************

int	adc_rate_out_test(int fd);
int	ain_buf_clear_test(int fd);
int	ain_buf_input_test(int fd);
int	ain_buf_overflow_test(int fd);
int	ain_buf_thresh_test(int fd);
int	ain_buf_underflow_test(int fd);
int	ain_coupling_test(int fd);
int	ain_filter_00_03_test(int fd);
int	ain_filter_04_07_test(int fd);
int	ain_filter_08_11_test(int fd);
int	ain_filter_12_15_test(int fd);
int	ain_filter_16_19_test(int fd);
int	ain_filter_20_23_test(int fd);
int	ain_filter_24_27_test(int fd);
int	ain_filter_28_31_test(int fd);
int	ain_filter_sel_test(int fd);
int	ain_filter_test(int fd);
int	ain_mode_test(int fd);
int	ain_range_sel_test(int fd);
int	ain_range_test(int fd);
int	ain_range_00_03_test(int fd);
int	ain_range_04_07_test(int fd);
int	ain_range_08_11_test(int fd);
int	ain_range_12_15_test(int fd);
int	ain_range_16_19_test(int fd);
int	ain_range_20_23_test(int fd);
int	ain_range_24_27_test(int fd);
int	ain_range_28_31_test(int fd);
int	auto_calibrate_test(int fd);

int	ch_grp_0_src_test(int fd);
int	ch_grp_1_src_test(int fd);
int	ch_grp_2_src_test(int fd);
int	ch_grp_3_src_test(int fd);
int	channel_order_test(int fd);

int	data_format_test(int fd);
int	data_width_test(int fd);

int	ext_clk_src_test(int fd);
int	ext_trig_test(int fd);

int	gps_enable_test(int fd);
int	gps_locked_test(int fd);
int	gps_polarity_test(int fd);
int	gps_rate_locked_test(int fd);
int	gps_target_rate_test(int fd);
int	gps_tolerance_test(int fd);

int	init_mode_test(int fd);
int	initialize_test(int fd);
int	irq_sel_test(int fd);

int	pll_ref_freq_hz_test(int fd);

int	query_test(int fd);

int	rate_div_0_ndiv_test(int fd);
int	rate_div_1_ndiv_test(int fd);
int	rate_gen_a_nrate_test(int fd);
int	rate_gen_a_nref_test(int fd);
int	rate_gen_a_nvco_test(int fd);
int	rate_gen_b_nrate_test(int fd);
int	rate_gen_b_nref_test(int fd);
int	rate_gen_b_nvco_test(int fd);
int	reg_mod_test(int fd);
int	reg_read_test(int fd);
int	reg_write_test(int fd);
int	rx_io_abort_test(int fd);
int	rx_io_mode_test(int fd);
int	rx_io_overflow_test(int fd);
int	rx_io_timeout_test(int fd);
int	rx_io_underflow_test(int fd);

int	service_ioctl_reg_get_list(int fd, const service_data_t* list);
int	service_ioctl_set_get_list(int fd, const service_data_t* list);
int	service_ioctl_set_reg_list(int fd, const service_data_t* list);
int	sw_sync_mode_test(int fd);
int	sw_sync_test(int fd);

int	thres_flag_cbl_test(int fd);

int	voltage_test(int fd);

int	wait_cancel_test(int fd);		// DSI_IOCTL_WAIT_CANCEL
int	wait_event_test(int fd);		// DSI_IOCTL_WAIT_EVENT
int	wait_status_test(int fd);		// DSI_IOCTL_WAIT_STATUS

int	xcvr_type_test(int fd);



#endif
