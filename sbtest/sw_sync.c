// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/sw_sync.c $
// $Rev: 25256 $
// $Date: 2014-02-26 11:01:39 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	// TBD
	return(0);
}



//*****************************************************************************
static int _function_test(int fd)
{
	#define	SW_SYNC			(1 << 6)
	#define	THRESHOLD_FLAG	(1 << 14)
	#define	FULL_MASK		0x3FFFF

	static const service_data_t	list[]	=
	{
		{				// Wait for the buffer to fill.
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	7,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{				// Verify that the threshold flag is set.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	THRESHOLD_FLAG,
			/* value	*/	THRESHOLD_FLAG
		},
		{				// Verify that the buffer is full.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BSR,
			/* mask		*/	FULL_MASK,
			/* value	*/	FULL_MASK
		},
		{				// Disable input to the buffer.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{				// SW Sync = Clear Buffer
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_SW_SYNC_MODE,
			/* arg		*/	DSI_SW_SYNC_MODE_CLR_BUF,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{				// SW Sync to clear the buffer
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_SW_SYNC,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	SW_SYNC,
			/* value	*/	0
		},
		{				// Verify that the threshold flag is clear.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	THRESHOLD_FLAG,
			/* value	*/	0
		},
		{				// Verify that the buffer is empty.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BSR,
			/* mask		*/	FULL_MASK,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_reg_list(fd, list);

	return(errs);
}



/******************************************************************************
*
*	Function:	sw_sync_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_SW_SYNC.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int sw_sync_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_SW_SYNC");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


