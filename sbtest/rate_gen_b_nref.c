// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/rate_gen_b_nref.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_subtest(int fd, s32 min, s32 max, s32 nref)
{
	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_B_NREF,
			/* arg		*/	min,
			/* reg		*/	DSI_GSC_RCBR,
			/* mask		*/	0x3FF0000,
			/* value	*/	min << 16
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_B_NREF,
			/* arg		*/	nref,
			/* reg		*/	DSI_GSC_RCBR,
			/* mask		*/	0x3FF0000,
			/* value	*/	nref << 16
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_B_NREF,
			/* arg		*/	max,
			/* reg		*/	DSI_GSC_RCBR,
			/* mask		*/	0x3FF0000,
			/* value	*/	max << 16
		},

		{ SERVICE_END_LIST }
	};

	int	errs	= 0;

	errs	+= service_ioctl_set_reg_list(fd, list);
	errs	+= service_ioctl_reg_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _service_test(int fd, s32 min, s32 max)
{
	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= _service_subtest(fd, min, max, min + 1);
	errs	+= _service_subtest(fd, min, max, (min + max) / 2);
	errs	+= _service_subtest(fd, min, max, max - 1);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	rate_gen_b_nref_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RATE_GEN_B_NREF.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rate_gen_b_nref_test(int fd)
{
	int	errs	= 0;
	s32	gens	= 0;
	s32	max;
	s32	min;
	s32	pll		= 0;
	s32	rcabr;

	gsc_label("DSI_IOCTL_RATE_GEN_B_NREF");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MAX, &max);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_NREF_MIN, &min);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gens);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RCAR_RCBR, &rcabr);

	if (errs)
	{
	}
	else if ((pll == 0) || (gens < 2) || (rcabr == 0))
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, min, max);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


