// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/reg_read.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	reg_read_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_REG_READ.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int reg_read_test(int fd)
{
	static const struct
	{
		u32	reg;
		u32	value;
	} list[]	=
	{
		// reg					value
		{ DSI_GSC_BCTLR,		0		},
		{ DSI_GSC_RAR,			0		},
		{ DSI_GSC_RDR,			0		},
		{ DSI_GSC_PRFR,			0		},
		{ DSI_GSC_GSR,			0		},
		{ DSI_GSC_IBCR,			0		},
		{ DSI_GSC_BCFGR,		0		},
		{ DSI_GSC_BSR,			0		},
		{ DSI_GSC_AVR,			0		},
		{ DSI_GSC_IDBR,			0		},
		{ DSI_GSC_RCAR,			0		},
		{ DSI_GSC_RCBR,			0		},
		{ DSI_GSC_NREFCR,		0		},
		{ DSI_GSC_NVCOCR,		0		},
		{ DSI_GSC_RFCR,			0		},

		{ GSC_PCI_9080_VIDR,	0x10B5	},
		{ GSC_PCI_9080_DIDR,	0		},
		{ GSC_PCI_9080_SVID,	0x10B5	},
		{ GSC_PCI_9080_SID,		0		},

		{ GSC_PLX_9080_VIDR,	0x10B5	},
		{ GSC_PLX_9080_DIDR,	0		}
	};

	int			errs	= 0;
	int			i;
	int			j;
	gsc_reg_t	parm;

	gsc_label("DSI_IOCTL_REG_READ");

	for (i = 0; i < (int) ARRAY_ELEMENTS(list); i++)
	{
		parm.reg	= list[i].reg;
		parm.value	= 0xDEADBEEF;
		parm.mask	= 0xBEEFDEAD;
		j			= ioctl(fd, DSI_IOCTL_REG_READ, (void*) &parm);

		if (j)
		{
			errs	= 1;
			printf(	"FAIL <---  (%d. i %d, ioctl() failure, errno %d)\n",
					__LINE__,
					i,
					errno);
			break;
		}

		if (parm.value == 0xDEADBEEF)
		{
			errs	= 1;
			printf("FAIL <---  (%d. i %d, value not changed)\n", __LINE__, i);
			break;
		}

		if (parm.mask != 0xBEEFDEAD)
		{
			errs	= 1;
			printf("FAIL <---  (%d. i %d, mask changed)\n", __LINE__, i);
			break;
		}

		if ((list[i].value) && (parm.value != list[i].value))
		{
			errs	= 1;
			printf(	"FAIL <---  (%d. i %d, got 0x%lX, expected 0x%lX)\n",
					__LINE__,
					i,
					(long) parm.value,
					(long) list[i].value);
			break;
		}
	}

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


