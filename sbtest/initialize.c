// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/initialize.c $
// $Rev: 25256 $
// $Date: 2014-02-26 11:01:39 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_INITIALIZE,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x8000,
			/* value	*/	0x0000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_reg_list(fd, list);

	errs	+= service_ioctl_set_reg_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{				// Adjust the input buffer threshold.
			/* service	*/	SERVICE_REG_MOD,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	0xFFFF,
			/* value	*/	0x9999
		},
		{				// Make sure the value is correct.
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	0xFFFF,
			/* value	*/	0x9999
		},
		{				// Perform the initialization.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_INITIALIZE,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	0xFFFF,
			/* value	*/	0xFFFE
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_reg_list(fd, list);

	return(errs);
}



/******************************************************************************
*
*	Function:	initialize_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_INITIALIZE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int initialize_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_INITIALIZE");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


