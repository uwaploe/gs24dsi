// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/rx_io_abort.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <semaphore.h>
#include <stdio.h>
#include <string.h>

#include "main.h"



// variables *******************************************************************

static	sem_t	sem_enter;
static	sem_t	sem_exit;



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no persistent bits to test with this service.
	return(0);
}



//*****************************************************************************
static int _abort_none_test(int fd)
{
	int	errs	= 0;
	s32	get;

	errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RX_IO_ABORT, &get);

	if ((errs == 0) && (get))
	{
		errs++;
		printf(	"FAIL <---  (%d. got %ld, expected %ld)\n",
				__LINE__,
				(long) get,
				(long) 0);
	}

	return(errs);
}



//*****************************************************************************
static int _rx_thread(void* arg)
{
	char	buffer[1024];
	int		fd		= (int) (long) arg;

	sem_post(&sem_enter);
	read(fd, buffer, sizeof(buffer));
	sem_post(&sem_exit);

	return(0);
}



//*****************************************************************************
static int _abort_one_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int			errs	= 0;
	s32			get;
	int			i;
	os_thread_t	thread;

	errs	+= service_ioctl_set_reg_list(fd, list);
	sem_init(&sem_enter, 0, 0);
	sem_init(&sem_exit, 0, 0);
	memset(&thread, 0, sizeof(thread));
	errs	+= os_thread_create(
				&thread,
				"Rx I/O Abort",
				_rx_thread,
				(void*) (long) fd);

	// Wait for the thread to execute.
	sem_wait(&sem_enter);

	if (errs == 0)
	{
		for (i = 0; i < 20; )
		{
			errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RX_IO_ABORT, &get);

			if ((errs) || (get))
				break;

			sleep(1);
		}

		if (errs)
		{
		}
		else if (get == 0)
		{
			errs++;
			printf("FAIL <---  (%d. abort not detected)\n", __LINE__);
		}
	}

	// Wait for the thread to finish.
	sem_wait(&sem_exit);

	// Finish up.
	errs	+= os_thread_destroy(&thread);
	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	int errs	= 0;

	errs	+= _abort_none_test(fd);
	errs	+= _abort_one_test(fd);
	return(errs);
}



/******************************************************************************
*
*	Function:	rx_io_abort_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_RX_IO_ABORT.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int rx_io_abort_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_RX_IO_ABORT");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


