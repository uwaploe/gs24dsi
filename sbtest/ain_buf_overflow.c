// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/ain_buf_overflow.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no persistent bits to test with this service.
	return(0);
}



//*****************************************************************************
static int _function_test(int fd)
{
	#undef	MASK
	#define	MASK	0x1000000

	service_data_t	pll_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	75,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	64,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	service_data_t	legacy_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	100000L,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	service_data_t	list_cause[]	=
	{
		{					// Enable input to the buffer.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_ENABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{					// Wait while data enters.
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	2,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{					// Disable input to the buffer.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{					// Verify that we've got an overflow
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	-1,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	MASK,
			/* value	*/	MASK
		},

		{ SERVICE_END_LIST }
	};

	service_data_t	list_clear[]	=
	{
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{					// Needed for some firmware versions.
			/* service	*/	SERVICE_REG_READ,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IDBR,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_OVERFLOW,
			/* arg		*/	DSI_AIN_BUF_OVERFLOW_CLEAR,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	0x100000,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_REG_TEST,
			/* cmd		*/	0,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	MASK,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs		= 0;
	s32	overflow;
	s32	pll;
	int	ret;

	errs	+= dsi_initialize(fd, -1, 0);
	pll		= DSI_QUERY_PLL_PRESENT;
	errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_QUERY, &pll);

	if (pll)
		errs	+= service_ioctl_set_reg_list(fd, pll_setup);
	else
		errs	+= service_ioctl_set_reg_list(fd, legacy_setup);

	errs	+= service_ioctl_set_reg_list(fd, list_cause);

	if (errs == 0)
	{
		// Verify that there is an overflow.
		overflow	= DSI_AIN_BUF_OVERFLOW_TEST;
		ret			= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &overflow);

		if (ret == -1)
		{
			errs	= 1;
			printf("FAIL <---  (ioctl() error, errno %d)\n", errno);
		}
		else if (overflow == 0)
		{
			errs	= 1;
			printf("FAIL <---  (overflow did not occur.)\n");
		}
	}

	if (errs == 0)
		errs	+= service_ioctl_set_reg_list(fd, list_clear);

	if (errs == 0)
	{
		// Verify that there is NOT an overflow.
		overflow	= DSI_AIN_BUF_OVERFLOW_TEST;
		ret			= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &overflow);

		if (ret == -1)
		{
			errs	= 1;
			printf("FAIL <---  (ioctl() error, errno %d)\n", errno);
		}
		else if (overflow)
		{
			errs	= 1;
			printf("FAIL <---  (overflow not expected.)\n");
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	ain_buf_overflow_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_BUF_OVERFLOW.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_buf_overflow_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_AIN_BUF_OVERFLOW");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


