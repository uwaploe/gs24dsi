// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/auto_calibrate.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AUTO_CALIBRATE,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x1080,
			/* value	*/	0x1000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_reg_list(fd, list);

	errs	+= service_ioctl_set_reg_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// This would require additional equipment.
	return(0);
}



/******************************************************************************
*
*	Function:	auto_calibrate_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AUTO_CALIBRATE. We can't
*		verify that an auto-calibration was performed, so we simply check that
*		the respective bits are correct afterwards.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int auto_calibrate_test(int fd)
{
	int	errs	= 0;
	s32	query;

	gsc_label("DSI_IOCTL_AUTO_CALIBRATE");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_AUTO_CAL_MS, &query);

	if (errs)
	{
	}
	else if (query == 0)
	{
		printf("SKIPPED  (Not supported on this board.\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


