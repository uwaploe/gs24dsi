// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/ain_range_sel.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE_SEL,
			/* arg		*/	DSI_AIN_RANGE_SEL_DISABLE,
			/* reg		*/	DSI_GSC_RFCR,
			/* mask		*/	0x200,
			/* value	*/	0x000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_RANGE_SEL,
			/* arg		*/	DSI_AIN_RANGE_SEL_ENABLE,
			/* reg		*/	DSI_GSC_RFCR,
			/* mask		*/	0x200,
			/* value	*/	0x200
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_range_sel_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_RANGE_SEL.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_range_sel_test(int fd)
{
	int	errs;
	s32	rfcr;

	gsc_label("DSI_IOCTL_AIN_RANGE_SEL");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_RFCR, &rfcr);

	if (errs)
	{
	}
	else if (rfcr == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


