// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/pll_ref_freq_hz.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	// Nothing to test here.
	return(0);
}



//*****************************************************************************
static int _function_test(int fd)
{
	long	dif;
	int		errs	= 0;
	s32		freq;
	long	max;
	u32		prfr;

	errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_PLL_REF_FREQ_HZ, &freq);
	errs	+= dsi_reg_read(fd, DSI_GSC_PRFR, &prfr);

	if (errs == 0)
	{
		// The accuracy should be .02%.
		max	= (long) (0.00041 * freq);
		dif	= (long) freq - (long) prfr;
		dif	= (dif > 0) ? dif : -dif;

		if (dif > max)
		{
			errs	= 1;
			printf(	"FAIL <---  (deviation is %ld Hz, max is %ld Hz)\n",
					dif,
					max);
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	pll_ref_freq_hz_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_PLL_REF_FREQ_HZ.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int pll_ref_freq_hz_test(int fd)
{
	int		errs	= 0;
//	s32		pll;

	gsc_label("DSI_IOCTL_PLL_REF_FREQ_HZ");
//	errs	= dsi_query(fd, -1, 0, DSI_QUERY_PLL_PRESENT, &pll);

//	if (errs)
//	{
//	}
//	else if (pll == 0)
//	{
//		printf("SKIPPED  (Not supported on this board.)\n");
//	}
//	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


