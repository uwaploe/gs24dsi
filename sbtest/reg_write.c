// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/reg_write.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	reg_write_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_REG_WRITE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int reg_write_test(int fd)
{
	static const struct
	{
		u32	reg;
		u32	value;
		u32	mask;
	} list[]	=
	{
		// reg					value		mask
		{ DSI_GSC_BCTLR,	0x0000013F,	0x0000073F	},
		{ DSI_GSC_BCTLR,	0x00000000,	0x0000073F	},

		{ DSI_GSC_RAR,		0x00000022,	0x000000FF	},
		{ DSI_GSC_RAR,		0x00000000,	0x000000FF	},

		{ DSI_GSC_RDR,		0x00000001,	0x000000FF	},
		{ DSI_GSC_RDR,		0x00000005,	0x000000FF	},

		{ DSI_GSC_IBCR,		0x00009999,	0x0003FFFF	},
		{ DSI_GSC_IBCR,		0x0003FFFE,	0x0003FFFF	},
	};

	int			errs	= 0;
	int			i;
	int			j;
	gsc_reg_t	parm;

	gsc_label("DSI_IOCTL_REG_WRITE");

	for (i = 0; i < (int) ARRAY_ELEMENTS(list); i++)
	{
		parm.reg	= list[i].reg;
		parm.value	= list[i].value;
		parm.mask	= 0xBEEFDEAD;
		j			= ioctl(fd, DSI_IOCTL_REG_WRITE, (void*) &parm);

		if (j)
		{
			errs	= 1;
			printf(	"FAIL <---  (%d. i %d, ioctl() failure, errno %d)\n",
					__LINE__,
					i,
					errno);
			break;
		}

		if (parm.mask != 0xBEEFDEAD)
		{
			errs	= 1;
			printf("FAIL <---  (%d. i %d, mask changed)\n", __LINE__, i);
			break;
		}

		// Now read the register and verify the bits of interest.
		j	= ioctl(fd, DSI_IOCTL_REG_READ, (void*) &parm);

		if (j)
		{
			errs	= 1;
			printf(	"FAIL <---  (%d. i %d, ioctl() failure, errno %d)\n",
					__LINE__,
					i,
					errno);
			break;
		}

		if ((list[i].value & list[i].mask) != (parm.value & list[i].mask))
		{
			errs	= 1;
			printf(	"FAIL <---  (%d. i %d, mask 0x%lX, got 0x%lX, expected 0x%lX)\n",
					__LINE__,
					i,
					(long) list[i].mask,
					(long) parm.value,
					(long) list[i].value);
			break;
		}
	}

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


