// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/init_mode.c $
// $Rev: 25256 $
// $Date: 2014-02-26 11:01:39 -0600 (Wed, 26 Feb 2014) $

#include <stdio.h>
#include <time.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_INIT_MODE,
			/* arg		*/	DSI_INIT_MODE_TARGET,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x20,
			/* value	*/	0x00
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_INIT_MODE,
			/* arg		*/	DSI_INIT_MODE_INITIATOR,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x20,
			/* value	*/	0x20
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	+= service_ioctl_set_get_list(fd, list);

	errs	+= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	init_mode_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_INIT_MODE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int init_mode_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_INIT_MODE");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


