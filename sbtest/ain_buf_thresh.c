// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/ain_buf_thresh.c $
// $Rev: 25256 $
// $Date: 2014-02-26 11:01:39 -0600 (Wed, 26 Feb 2014) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_THRESH,
			/* arg		*/	0,
			/* reg		*/	DSI_GSC_IBCR,
			/* mask		*/	0x3FFFF,
			/* value	*/	0x00000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;
	int	i;

	errs	+= dsi_initialize(fd, -1, 0);

	for (i = 0; i <= 17; i++)
	{
		// Walking one.
		list[0].arg		= 1 << i;
		list[0].value	= 1 << i;

		errs	+= service_ioctl_set_reg_list(fd, list);
		errs	+= service_ioctl_reg_get_list(fd, list);

		errs	+= service_ioctl_set_reg_list(fd, list);
		errs	+= service_ioctl_reg_get_list(fd, list);

		// Walking zero.
		list[0].arg		= 0xFFFF ^ (1 << i);
		list[0].value	= 0xFFFF ^ (1 << i);

		errs	+= service_ioctl_set_reg_list(fd, list);
		errs	+= service_ioctl_reg_get_list(fd, list);

		errs	+= service_ioctl_set_reg_list(fd, list);
		errs	+= service_ioctl_reg_get_list(fd, list);
	}

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ain_buf_thresh_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_AIN_BUF_THRESH.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ain_buf_thresh_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_AIN_BUF_THRESH");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}


