// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/wait_event.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <errno.h>
#include <semaphore.h>
#include <stdio.h>
#include <string.h>

#include "main.h"



// #defines *******************************************************************

#define	_1_SEC		(1000L)
#define	_15_SEC		(15L * 1000L)
#define	_30_SEC		(30L * 1000L)



// variables *******************************************************************

static	int			rx_stat;
static	sem_t		sem_enter;
static	sem_t		sem_exit;
static	gsc_wait_t	wait_dst;
static	int			wait_fd;



//*****************************************************************************
static int _service_test(int fd)
{
	// There are no settings bits to test here.
	return(0);
}



//*****************************************************************************
static int _wait_thread(void* arg)
{
	int	status;

	sem_post(&sem_enter);
	status	= ioctl(wait_fd, DSI_IOCTL_WAIT_EVENT, &wait_dst);

	if (status == -1)
		wait_dst.flags	= 0xFFFFFFFF;

	sem_post(&sem_exit);
	return(0);
}



//*****************************************************************************
static int	_wait_main_dma(int fd, int supported)
{
	static const service_data_t	pll_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	75,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	64,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t legacy_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	100000,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list[]	=
	{
		{					// Select DMA mode.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_MODE,
			/* arg		*/	GSC_IO_MODE_DMA,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		// Fill the FIFO.

		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_ENABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{				// Wait to see if we get any data.
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	2,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	u32	buf[1024];
	int	errs	= 0;
	s32	pll;
	int	ret;
	s32	sts		= 0;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	+= dsi_initialize(fd, -1, 0);
		pll		= DSI_QUERY_PLL_PRESENT;
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_QUERY, &pll);

		// Verify that the status is an overflow.

		if (pll)
			errs	+= service_ioctl_set_reg_list(fd, pll_setup);
		else
			errs	+= service_ioctl_set_reg_list(fd, legacy_setup);

		errs	+= service_ioctl_set_reg_list(fd, list);

		if (errs == 0)
		{
			sts	= -1;
			ret	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &sts);

			if (ret)
			{
				errs++;
			}
			else if (sts != DSI_AIN_BUF_OVERFLOW_YES)
			{
				errs++;
				printf(	"FAIL <---  (%d. overflow did not occur but was expected)\n",
						__LINE__);
			}
		}

		// Now perform a DMA read to trigger the wait.

		if (errs == 0)
		{
			sts		= dsi_dsl_read(fd, buf, ARRAY_ELEMENTS(buf));
			errs	+= (sts == ARRAY_ELEMENTS(buf)) ? 0 : 1;
		}

		ret	= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_main_gsc(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_CHAN_READY,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_gsc_init_done(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_INIT_DONE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_INITIALIZE,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_gsc_auto_cal_done(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_AUTO_CAL_DONE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AUTO_CALIBRATE,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	s32	ms;
	int	ret;

	if (supported)
	{
		errs	= dsi_query(fd, -1, 0, DSI_QUERY_AUTO_CAL_MS, &ms);
		ret		= errs ? errs : (ms ? -1 : 0);
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_gsc_channel_ready(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_CHAN_READY,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_gsc_buf_thresh_l2h(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_AIN_BUF_THRESH_L2H,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_gsc_buf_thresh_h2l(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_IRQ_SEL,
			/* arg		*/	DSI_IRQ_AIN_BUF_THRESH_H2L,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int	errs;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	= service_ioctl_set_reg_list(fd, list);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_io_rx_done(int fd, int supported)
{
	static const service_data_t	pll_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NVCO,
			/* arg		*/	75,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NREF,
			/* arg		*/	64,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_DIV_0_NDIV,
			/* arg		*/	0,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	legacy_setup[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RATE_GEN_A_NRATE,
			/* arg		*/	100000,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list[]	=
	{
		{					// Select PIO mode.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_MODE,
			/* arg		*/	GSC_IO_MODE_PIO,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		// Fill the FIFO.

		{				// Wait to see if we get any data.
			/* service	*/	SERVICE_SLEEP,
			/* cmd		*/	0,
			/* arg		*/	2,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	u32	buf[1024];
	int	errs	= 0;
	s32	pll;
	int	ret;
	s32	sts		= 0;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	+= dsi_initialize(fd, -1, 0);
		pll		= DSI_QUERY_PLL_PRESENT;
		errs	+= dsi_dsl_ioctl(fd, DSI_IOCTL_QUERY, &pll);

		// Verify that the status is an overflow.

		if (pll)
			errs	+= service_ioctl_set_reg_list(fd, pll_setup);
		else
			errs	+= service_ioctl_set_reg_list(fd, legacy_setup);

		errs	+= service_ioctl_set_reg_list(fd, list);

		if (errs == 0)
		{
			sts	= DSI_AIN_BUF_OVERFLOW_TEST;
			ret	= dsi_dsl_ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &sts);

			if (ret)
			{
				errs++;
			}
			else if (sts != DSI_AIN_BUF_OVERFLOW_YES)
			{
				errs++;
				printf(	"FAIL <---  (%d. overflow did not occur but was expected)\n",
						__LINE__);
			}
		}

		// Now perform a read to trigger the wait.

		if (errs == 0)
		{
			sts		= dsi_dsl_read(fd, buf, ARRAY_ELEMENTS(buf));
			errs	+= (sts == ARRAY_ELEMENTS(buf)) ? 0 : 1;
		}

		errs	+= dsi_initialize(fd, -1, 0);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_io_rx_error(int fd, int supported)
{
	int	errs	= 0;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		// Now perform a read to trigger the wait.
		ret	= read(fd, NULL, 3);

		if (ret != -1)
		{
			errs++;
			printf("FAIL <---  (%d. expected %d, got %d)\n", __LINE__, -1, ret);
		}

		ret	= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_io_rx_timeout(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{					// Select PIO mode.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_MODE,
			/* arg		*/	GSC_IO_MODE_PIO,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	u32	buf[1024];
	int	errs	= 0;
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	+= dsi_initialize(fd, -1, 0);

		// Verify that the status is an overflow.
		errs	+= service_ioctl_set_reg_list(fd, list);

		// Now perform a read to trigger the wait.

		if (errs == 0)
		{
			ret	= read(fd, buf, ARRAY_ELEMENTS(buf));

			if (ret != 0)
			{
				errs++;
				printf(	"FAIL <---  (%d. expected %d, got %d)\n",
						__LINE__,
						0,
						ret);
			}
		}

		errs	+= dsi_initialize(fd, -1, 0);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int _read_thread(void* arg)
{
	u32		buf[1024];
	int		fd	= (int) (long) arg;
	long	status;

	status	= read(fd, buf, sizeof(buf));

	if (status == 0)
	{
		rx_stat	= 1;
	}
	else
	{
		rx_stat	= 2;
		printf("FAIL <---  (%d. read returned %ld, expected 0)\n",
				__LINE__,
				(long) status);
	}

	return(0);
}



//*****************************************************************************
static int	_wait_io_rx_abort(int fd, int supported)
{
	static const service_data_t	list[]	=
	{
		{					// Select PIO mode.
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_MODE,
			/* arg		*/	GSC_IO_MODE_PIO,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_OVERFLOW,
			/* arg		*/	DSI_IO_OVERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_UNDERFLOW,
			/* arg		*/	DSI_IO_UNDERFLOW_IGNORE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_RX_IO_TIMEOUT,
			/* arg		*/	10,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_INPUT,
			/* arg		*/	DSI_AIN_BUF_INPUT_DISABLE,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_AIN_BUF_CLEAR,
			/* arg		*/	1,
			/* reg		*/	0,
			/* mask		*/	0,
			/* value	*/	0
		},

		{ SERVICE_END_LIST }
	};

	int			errs	= 0;
	int			i;
	int			ret;
	s32			set;
	os_thread_t	thread;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		errs	+= dsi_initialize(fd, -1, 0);

		// Verify that the status is an overflow.
		errs	+= service_ioctl_set_reg_list(fd, list);

		// Now start the read thread so we can do the abort.
		memset(&thread, 0, sizeof(thread));

		if (errs == 0)
		{
			errs	+= os_thread_create(	&thread,
											"Rx",
											_read_thread,
											(void*) (long) fd);
		}

		for (i = 0; (errs == 0) && (i < 100); i++)
		{
			set		= 0;
			errs	= dsi_dsl_ioctl(fd, DSI_IOCTL_RX_IO_ABORT, &set);

			if (errs)
				break;

			if (set)
				break;

			usleep(100000L);
		}

		// Wait for the thread to report its status.

		for (i = 0; i < 100; i++)
		{
			if (errs)
				break;

			if (rx_stat == 0)
			{
				usleep(100000);
				continue;
			}

			if (rx_stat == 1)
				break;

			errs++;
			break;
		}

		errs	+= os_thread_destroy(&thread);
		errs	+= dsi_initialize(fd, -1, 0);
		ret		= errs;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_timeout(int fd, int supported)
{
	int	ret;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		sleep(2);
		ret	= 0;
	}

	return(ret);
}



//*****************************************************************************
static int	_wait_cancel(int fd, int supported)
{
	int			errs;
	int			ret;
	gsc_wait_t	wait;

	if (supported)
	{
		ret	= -1;
	}
	else
	{
		wait.flags			= 0;
		wait.main			= 0xFFFFFFFF;
		wait.gsc			= 0xFFFFFFFF;
		wait.alt			= 0xFFFFFFFF;
		wait.io				= 0xFFFFFFFF;
		wait.timeout_ms		= 0;
		wait.count			= 0;
		errs	= dsi_dsl_ioctl(wait_fd, DSI_IOCTL_WAIT_CANCEL, &wait);
		ret		= errs;
	}

	return(ret);
}



// variables *******************************************************************

static const struct
{
	const char*	name;
	int			(*function)(int fd, int supported);
	gsc_wait_t	wait;
} wait_src[]	=
{
	// name						function					flags			main								...			timeout_ms	count
	{ "DMA0/DMA1",				_wait_main_dma,				{ 0,			GSC_WAIT_MAIN_DMA0 |
																			GSC_WAIT_MAIN_DMA1,					0, 0, 0,	_15_SEC,	0	}	},
	{ "GSC",					_wait_main_gsc,				{ 0,			GSC_WAIT_MAIN_GSC,					0, 0, 0,	_15_SEC,	0	}	},

	// name						function					flags ...		gsc									...			timeout_ms
	{ "Initialization Done",	_wait_gsc_init_done,		{ 0, 0,			DSI_WAIT_GSC_INIT_DONE,				0, 0,		_15_SEC,	0	}	},
	{ "Auto-Cal Done",			_wait_gsc_auto_cal_done,	{ 0, 0,			DSI_WAIT_GSC_AUTO_CAL_DONE,			0, 0,		_15_SEC,	0	}	},
	{ "Channel Ready",			_wait_gsc_channel_ready,	{ 0, 0,			DSI_WAIT_GSC_CHAN_READY,			0, 0,		_15_SEC,	0	}	},
	{ "Buf Threshold Low->Hi",	_wait_gsc_buf_thresh_l2h,	{ 0, 0,			DSI_WAIT_GSC_AIN_BUF_THRESH_L2H,	0, 0,		_15_SEC,	0	}	},
	{ "Buf Threshold Hi->Low",	_wait_gsc_buf_thresh_h2l,	{ 0, 0,			DSI_WAIT_GSC_AIN_BUF_THRESH_H2L,	0, 0,		_15_SEC,	0	}	},

	// name						function					flags ...		io												timeout_ms
	{ "Rx Done",				_wait_io_rx_done,			{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_DONE,							_15_SEC,	0	}	},
	{ "Rx Error",				_wait_io_rx_error,			{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_ERROR,							_15_SEC,	0	}	},
	{ "Rx Timeout",				_wait_io_rx_timeout,		{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_TIMEOUT,							_15_SEC,	0	}	},
	{ "Rx Abort",				_wait_io_rx_abort,			{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_ABORT,							_15_SEC,	0	}	},

	// name						function					flags ...		io												timeout_ms
	{ "Wait Timeout",			_wait_timeout,				{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_DONE,							_1_SEC,		0	}	},
	{ "Wait Cancel",			_wait_cancel,				{ 0, 0, 0, 0,	GSC_WAIT_IO_RX_DONE,							_30_SEC,	0	}	},

	// terminate list
	{ NULL, NULL, { 1, 0, 0, 0,	0, 0, 0	} }
};



//*****************************************************************************
static int _process_thread(int index)
{
	char		buf[64];
	int			errs	= 0;
	int			i;
	int			status;
	os_thread_t	thread;
	gsc_wait_t	wait;

	for (;;)	// A convenience loop.
	{
		sem_init(&sem_enter, 0, 0);
		sem_init(&sem_exit, 0, 0);
		memset(&thread, 0, sizeof(thread));
		sprintf(buf, "wait event %d", index);
		wait_dst	= wait_src[index].wait;
		errs		+= os_thread_create(&thread, buf, _wait_thread, NULL);

		// Wait for the thread to become waiting.
		sem_wait(&sem_enter);

		if (errs == 0)
		{
			wait	= wait_src[index].wait;

			for (i = 0; i < 100; i++)
			{
				status		= ioctl(wait_fd, DSI_IOCTL_WAIT_STATUS, &wait);

				if (status == -1)
				{
					printf(	"FAIL <---  (%d. status request %d: errno %d)\n",
							__LINE__,
							index,
							errno);
					errs++;
					break;
				}

				if (wait.count == 0)
				{
					usleep(100000);
					continue;
				}

				if (wait.count == 1)
					break;

				errs++;
				printf(	"FAIL <---  (%d. invalid wait count %ld)\n",
						__LINE__,
						(long) wait.count);
				break;
			}
		}

		// Initiate the respective event.
		errs	+= wait_src[index].function(wait_fd, 0);

		if (errs)
			break;		// There was an error.

		// Verify that the wait ended as expected.

		if (wait_src[index].wait.flags == 0xFFFFFFFF)
			break;		// There was an error.

		// Wait for the resumed thread to run.
		sem_wait(&sem_exit);

		for (i = 0;; i++)
		{
			if (wait_dst.flags)
				break;

			if (i >= 100)
			{
				printf(	"FAIL <---  (%d. thread failed to resume)\n",
						__LINE__);
				errs++;
				break;
			}

			usleep(100000);
		}

		if (errs)
			break;		// There was an error.

		if (wait_src[index].wait.timeout_ms == _1_SEC)
		{
			if (wait_dst.flags != GSC_WAIT_FLAG_TIMEOUT)
			{
				printf(	"FAIL <---  (%d. flag: expect 0x%lX, got 0x%lX)\n",
						__LINE__,
						(long) GSC_WAIT_FLAG_TIMEOUT,
						(long) wait_dst.flags);
				errs++;
			}

			break;
		}

		if (wait_src[index].wait.timeout_ms == _15_SEC)
		{
			if (wait_dst.flags != GSC_WAIT_FLAG_DONE)
			{
				printf(	"FAIL <---  (%d. flag: expect 0x%lX, got 0x%lX)\n",
						__LINE__,
						(long) GSC_WAIT_FLAG_DONE,
						(long) wait_dst.flags);
				errs++;
			}

			break;
		}

		if (wait_src[index].wait.timeout_ms == _30_SEC)
		{
			if (wait_dst.flags != GSC_WAIT_FLAG_CANCEL)
			{
				printf(	"FAIL <---  (%d. flag: expect 0x%lX, got 0x%lX)\n",
						__LINE__,
						(long) GSC_WAIT_FLAG_TIMEOUT,
						(long) wait_dst.flags);
				errs++;
			}

			break;
		}

		printf("FAIL <---  (%d. INTERNAL ERROR)\n", __LINE__);
		errs++;
		break;
	}

	// Force termination, just in case of an error.
	wait.flags			= 0;
	wait.main			= 0xFFFFFFFF;
	wait.gsc			= 0xFFFFFFFF;
	wait.alt			= 0xFFFFFFFF;
	wait.io				= 0xFFFFFFFF;
	wait.timeout_ms		= 0;
	wait.count			= 0;
	errs	+= dsi_dsl_ioctl(wait_fd, DSI_IOCTL_WAIT_CANCEL, &wait);
	errs	+= os_thread_destroy(&thread);

	if (errs == 0)
		printf("PASS\n");

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	int	errs	= 0;
	int	i;
	int	ret;

	printf("\n");
	gsc_label_level_inc();

	// Create threads awaiting each of the source crtiteria
	// and verify that it resumes when the criteria is met.

	for (i = 0; wait_src[i].wait.flags == 0; i++)
	{
		gsc_label(wait_src[i].name);
		ret	= wait_src[i].function(wait_fd, 1);

		if (ret > 0)
		{
		}
		else if (ret == 0)
		{
			printf("SKIPPED   (Not supported on this board.)\n");
		}
		else
		{
			wait_fd	= fd;
			memset(&wait_dst, 0, sizeof(wait_dst));
			errs	+= _process_thread(i);
		}
	}

	gsc_label_level_dec();
	return(errs);
}



/******************************************************************************
*
*	Function:	wait_event_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_WAIT_EVENT.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int wait_event_test(int fd)
{
	int	errs	= 0;

	gsc_label("DSI_IOCTL_WAIT_EVENT");
	errs	+= _service_test(fd);
	errs	+= _function_test(fd);

	return(errs);
}


