// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/xcvr_type.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd)
{
	static const service_data_t	list[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_XCVR_TYPE,
			/* arg		*/	DSI_XCVR_TYPE_LVDS,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x100000,
			/* value	*/	0x000000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_XCVR_TYPE,
			/* arg		*/	DSI_XCVR_TYPE_TTL,
			/* reg		*/	DSI_GSC_BCTLR,
			/* mask		*/	0x100000,
			/* value	*/	0x100000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	errs	= service_ioctl_set_get_list(fd, list);

	errs	= service_ioctl_set_get_list(fd, list);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	xcvr_type_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_XCVR_TYPE.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int xcvr_type_test(int fd)
{
	int	errs;
	s32	d20;

	gsc_label("DSI_IOCTL_XCVR_TYPE");
	errs	= dsi_query(fd, -1, 0, DSI_QUERY_D20_XCVR, &d20);

	if (errs)
	{
	}
	else if (d20 == 0)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


