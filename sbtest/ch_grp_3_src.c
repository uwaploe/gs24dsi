// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sbtest/ch_grp_3_src.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <stdio.h>

#include "main.h"



//*****************************************************************************
static int _service_test(int fd, s32 gens, s32 rar0)
{
	static const service_data_t	list_a[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_A,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x0000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x4000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DIR_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x5000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x6000
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_ab[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_A,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x0000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_GEN_B,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x1000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x4000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DIR_EXTERN,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x5000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x6000
		},

		{ SERVICE_END_LIST }
	};

	static const service_data_t	list_alt[]	=
	{
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_ENABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x5000
		},
		{
			/* service	*/	SERVICE_NORMAL,
			/* cmd		*/	DSI_IOCTL_CH_GRP_3_SRC,
			/* arg		*/	DSI_CH_GRP_SRC_DISABLE,
			/* reg		*/	DSI_GSC_RAR,
			/* mask		*/	0xF000,
			/* value	*/	0x6000
		},

		{ SERVICE_END_LIST }
	};

	int errs	= 0;

	errs	+= dsi_initialize(fd, -1, 0);

	if (rar0)
		errs	+= service_ioctl_set_reg_list(fd, list_alt);
	else if (gens == 1)
		errs	+= service_ioctl_set_reg_list(fd, list_a);
	else
		errs	+= service_ioctl_reg_get_list(fd, list_ab);

	return(errs);
}



//*****************************************************************************
static int _function_test(int fd)
{
	// TBD
	return(0);
}



/******************************************************************************
*
*	Function:	ch_grp_3_src_test
*
*	Purpose:
*
*		Perform a test of the IOCTL service DSI_IOCTL_CH_GRP_3_SRC.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int ch_grp_3_src_test(int fd)
{
	int	errs	= 0;
	s32	gens;
	s32	groups;
	s32	rar0;

	gsc_label("DSI_IOCTL_CH_GRP_3_SRC");
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CHANNEL_GPS, &groups);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_RATE_GEN_QTY, &gens);
	errs	+= dsi_query(fd, -1, 0, DSI_QUERY_CH_GRP_0_RAR, &rar0);

	if (errs)
	{
	}
	else if (groups < 4)
	{
		printf("SKIPPED  (Not supported on this board.)\n");
	}
	else
	{
		errs	+= _service_test(fd, gens, rar0);
		errs	+= _function_test(fd);

		if (errs == 0)
			printf("PASS\n");
	}

	return(errs);
}


