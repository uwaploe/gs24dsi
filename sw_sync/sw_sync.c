// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/sw_sync/sw_sync.c $
// $Rev: 25253 $
// $Date: 2014-02-26 10:59:15 -0600 (Wed, 26 Feb 2014) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



/******************************************************************************
*
*	Function:	sw_sync
*
*		Repetatively generate a SW Sync pulse.
*
*	Arguments:
*
*		fd		The handle to the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

int sw_sync(int fd)
{
	int	errs	= 0;
	int	i;
	int	qty		= 10;

	for (i = 0; (i < qty) && (errs == 0); i++)
		errs	+= dsi_sw_sync(fd, -1, 1);

	return(errs);
}



