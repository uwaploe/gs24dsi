// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/mbsync/main.h $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi_dsl.h"
#include "24dsi_utils.h"



// #defines *******************************************************************

#define	SAMPLE_RATE				(100000L)
#define	INPUT_FREQUENCY			(5000L)

#define	DATA_SET_SIZE			(250L * 1024)										// -> 256,000
// The data set size is chosen to be just under the overall size of the input
// FIFO.

#define	EXAMINE_OFFSET			(DATA_SET_SIZE / 5)									// -> 51,200
// The first 20% of the buffer is skipped as the system may not have settled.
// This equates to about 50K as follows.
// (250K / 5) = 50K

#define	SAMPLES_NEEDED			((DATA_SET_SIZE * 8/10) / 32)						// -> 5760
// This is the minimum number of channel zero data samples expected in the
// data to be examined. The overall data volume is divided by 32 as the board
// may have 32 channels.

#define	CYCLES_MIN				(SAMPLES_NEEDED  * INPUT_FREQUENCY / SAMPLE_RATE)	// ->288
// This is the minimum number of sine wave cycles expected in the examined
// data. The ratio of (SAMPLES_NEEDED / SAMPLE_RATE) is the fraction of a
// second represented by the volume of data examined. That period of time
// multiplied by the input signal frequency is the number of sine waves
// expected in the examined data.

#define	PERIOD_MIN				(SAMPLES_NEEDED / CYCLES_MIN)
// This is the minimum number of data samples expected for each cycle of the
// input size wave.



// data types *****************************************************************

typedef struct
{
	u32		data[DATA_SET_SIZE];
} read_data_t;

typedef struct
{
	int		ignore;
	long	ch_0_cycles;
	float	ch_0_period;
	long	ch_0_samples;
	float	freq;			// frequency of input signal
	s32		fref;			// default
	long	fsamp;			// calculated
	float	vmin;
	float	vmax;
	float	phase_ns;
	float	phase_deg;
} volt_data_t;



// prototypes *****************************************************************

int		multi_board_sync(int qty, const int* i_list, const int* fd_list, int star, int errs, int* lp);
int		validate_sync(int qty, const int* fd_list, int errs, int lp);

int		service_ain_range				(int qty, const int* fd_list, int arg);
int		service_auto_cal				(int qty, const int* fd_list);
int		service_clear					(int qty, const int* fd_list);
int		service_close					(int qty, int* fd_list);
int		service_config					(int qty, const int* fd_list, s32 fsamp);
int		service_channel_group_source	(int qty, const int* fd_list, int init, int targ);
int		service_channel_order			(int qty, const int* fd_list, int arg);
int		service_channel_ready			(int qty, const int* fd_list, int init, int targ);
int		service_data_format				(int qty, const int* fd_list, int arg);
int		service_data_rate_cycles		(int qty, volt_data_t* v_data);
int		service_data_rate_examine		(int qty, const read_data_t* data, volt_data_t* v_data);
int		service_data_rate_frequency		(int qty, const int* fd_list, volt_data_t* v_data);
int		service_data_rate_fsamp			(int qty, const int* fd_list, volt_data_t* v_data);
int		service_data_rate_period		(int qty, volt_data_t* v_data);
int		service_data_rate_phase			(int qty, const read_data_t* data, volt_data_t* v_data, int lp);
int		service_data_width				(int qty, const int* fd_list, int arg);
int		service_extern_clock_source		(int qty, const int* fd_list, int init, int targ);
int		service_init_mode				(int qty, const int* fd_list, int init, int targ);
void	service_list_init				(int qty, const int* i_list);
int		service_low_power				(int qty, const int* fd_list, int* lp);
int		service_open					(int qty, const int* i_list, int* fd_list);
int		service_read					(int qty, const int* fd_list, read_data_t* data);
int		service_rx_io_overflow			(int qty, const int* fd_list, int init, int targ);
int		service_sw_sync					(int qty, const int* fd_list, int init, int targ);
int		service_sw_sync_mode			(int qty, const int* fd_list, int init, int targ);
int		service_voltage_range_examine	(int qty, const read_data_t* data, volt_data_t* v_data, int lp);
int		service_voltage_range_samples	(int qty, volt_data_t* v_data);
int		service_voltage_range_vmax		(int qty, volt_data_t* v_data, int lp);
int		service_voltage_range_vmin		(int qty, volt_data_t* v_data, int lp);



#endif
