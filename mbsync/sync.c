// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/mbsync/sync.c $
// $Rev: 32581 $
// $Date: 2015-07-04 17:20:56 -0500 (Sat, 04 Jul 2015) $

#include <errno.h>
#include <stdio.h>

#include "main.h"



//*****************************************************************************
int multi_board_sync(int qty, const int* i_list, const int* fd_list, int star, int errs, int* lp)
{
	s32	fsamp	= SAMPLE_RATE;
	int	range;

	//*****************************************************
	gsc_label("Configuration");
	printf("\n");
	gsc_label_level_inc();

	if (errs)
	{
		printf("\t SKIPPED DUE TO ERRORS.\n");
	}
	else
	{
		errs	+= service_config			(qty, fd_list, fsamp);
		errs	+= service_rx_io_overflow	(qty, fd_list, DSI_IO_OVERFLOW_IGNORE, DSI_IO_OVERFLOW_IGNORE);
		errs	+= service_low_power		(qty, fd_list, lp);
		range	= lp[0] ? DSI_AIN_RANGE_5V : DSI_AIN_RANGE_10V;
		errs	+= service_ain_range		(qty, fd_list, range);
		errs	+= service_data_width		(qty, fd_list, DSI_DATA_WIDTH_16);
		errs	+= service_data_format		(qty, fd_list, DSI_DATA_FORMAT_OFF_BIN);
		errs	+= service_auto_cal			(qty, fd_list);
		errs	+= service_clear			(qty, fd_list);

		if (star)
		{
			// Star Configuration									Initiator					Each Target
			errs	+= service_init_mode			(qty, fd_list,	DSI_INIT_MODE_INITIATOR,	-1							);
			errs	+= service_channel_group_source	(qty, fd_list,	DSI_CH_GRP_SRC_EXTERN,		DSI_CH_GRP_SRC_EXTERN		);
			errs	+= service_extern_clock_source	(qty, fd_list,	DSI_EXT_CLK_SRC_GEN_A,		DSI_EXT_CLK_SRC_GEN_A		);
			errs	+= service_channel_order		(qty, fd_list,	DSI_CHANNEL_ORDER_SYNC);
		}
		else
		{
			// Daisy Chain Configiration							Initiator					Each Target
			errs	+= service_init_mode			(qty, fd_list,	DSI_INIT_MODE_INITIATOR,	DSI_INIT_MODE_TARGET		);
			errs	+= service_channel_group_source	(qty, fd_list,	DSI_CH_GRP_SRC_GEN_A,		DSI_CH_GRP_SRC_DIR_EXTERN	);
			errs	+= service_extern_clock_source	(qty, fd_list,	DSI_EXT_CLK_SRC_GRP_0,		-1							);
			errs	+= service_channel_order		(qty, fd_list,	DSI_CHANNEL_ORDER_SYNC);
		}
	}

	gsc_label_level_dec();

	//*****************************************************
	gsc_label("Scan Synchronize");
	printf("\n");
	gsc_label_level_inc();

	if (errs)
	{
		printf("\t SKIPPED DUE TO ERRORS.\n");
	}
	else
	{
		//												Initiator					Each Target
		errs	+= service_sw_sync_mode	(qty, fd_list,	DSI_SW_SYNC_MODE_SW_SYNC,	DSI_SW_SYNC_MODE_SW_SYNC	);
		errs	+= service_sw_sync		(qty, fd_list,	0,							-1							);
		errs	+= service_channel_ready(qty, fd_list,	0,							0							);
	}

	gsc_label_level_dec();

	//*****************************************************
	gsc_label("Data Synchronize");
	printf("\n");
	gsc_label_level_inc();

	if (errs)
	{
		printf("\t SKIPPED DUE TO ERRORS.\n");
	}
	else
	{
		//												Initiator					Each Target
		errs	+= service_sw_sync_mode	(qty, fd_list,	DSI_SW_SYNC_MODE_CLR_BUF,	DSI_SW_SYNC_MODE_CLR_BUF	);
		errs	+= service_sw_sync		(qty, fd_list,	0,							-1							);
		errs	+= service_channel_ready(qty, fd_list,	0,							0							);
	}

	gsc_label_level_dec();

	return(errs);
}


