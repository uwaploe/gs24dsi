// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/mbsync/main.c $
// $Rev: 34186 $
// $Date: 2016-01-08 17:29:48 -0600 (Fri, 08 Jan 2016) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// variables ******************************************************************

static	int		_arg_boards		= 0;	// Quantiyy of boards listed on the command line.
static	int		_board_list[8]	= { 0, 0, 0, 0, 0, 0, 0, 0 };
static	int		_board_qty		= 0;	// The number of installed boards.
static	int		_continuous		= 0;
static	int		_fd_list[8]		= { -1, -1, -1, -1, -1, -1, -1, -1 };
static	int		_ignore_errors	= 0;
static	int		_lp				= 0;	// Is a Low Power board present?
static	long	_minute_limit	= 0;
static	int		_star			= 0;
static	int		_sync_qty		= 0;	// The number of boards to synchronize.
static	int		_test_limit		= -1;



//*****************************************************************************
static int _add_board_index(int index, int value)
{
	int	errs	= 0;
	int	i;

	for (;;)	// A convenience loop.
	{
		if (_sync_qty <= 0)
		{
			errs	= 1;
			printf(	"ERROR: argument %d is '%d'."
					" The number of boards to sync hasn't been set.\n",
					index,
					value);
			break;
		}

		if (value >= _board_qty)
		{
			errs	= 1;
			printf(	"ERROR: argument %d is '%d'."
					" There are only %d boards in the system.\n",
					index,
					value,
					_board_qty);
			break;
		}

		if (_arg_boards >= _sync_qty)
		{
			errs	= 1;
			printf(	"ERROR: argument %d is '%d'."
					" Too many board index numbers have been specified.\n",
					index,
					value);
			break;
		}

		for (i = 0; i < _arg_boards; i++)
		{
			if (_board_list[i] == value)
			{
				errs	= 1;
				printf(	"ERROR: argument %d is '%d'."
						" This index has already been specified.\n",
						index,
						value);
				break;
			}
		}

		_board_list[_arg_boards]	= value;
		_arg_boards++;
		break;
	}

	return(errs);
}



//*****************************************************************************
static int _validate_args(void)
{
	int	errs	= 0;

	if (_sync_qty <= 0)
	{
		errs	= 1;
		printf("ERROR: The -b# argument was not specified.\n");
	}
	else if (_arg_boards != _sync_qty)
	{
		errs	= 1;
		printf("ERROR: Too few board indexes were given.\n");
	}

	return(errs);
}



//*****************************************************************************
static int _parse_args(int argc, char** argv)
{
	char	buf[64];
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;

	printf("mbsync - Multi-Board Synchronization\n");
	printf("USAGE: mbsync <-b#> <-c> <-C> <-d> <-m#> <-n#> <-s> <initiator> <targets ...>\n");
	printf("  -b#        This is the number of boards to synchronize (2-8).\n");
	printf("  -c         Continue testing until an error occurs.\n");
	printf("  -C         Continue testing even if errors occur.\n");
	printf("  -d         The clock and sync signals are in a DAISY CHAIN configuration (default).\n");
	printf("  -m#        Run for at most # minutes.\n");
	printf("  -n#        Repeat test at most # times.\n");
	printf("  -s         The clock and sync signals are in a STAR configuration.\n");
	printf("  initiator  The zero based index of the initiator device to access.\n");
	printf("  targets    The zero based index of the target device to access.\n");

	gsc_label("Arguments");
	printf("(%d argument%s)\n", argc - 1, ((argc - 1) > 1) ? "s" : "");
	gsc_label_level_inc();

	for (i = 0; i < argc; i++)
	{
		sprintf(buf, "Argument %d", i);
		gsc_label(buf);
		printf("%s\n", argv[i]);
	}

	gsc_label_level_dec();

	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "0") == 0)
		{
			errs	= _add_board_index(i, 0);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "1") == 0)
		{
			errs	= _add_board_index(i, 1);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "2") == 0)
		{
			errs	= _add_board_index(i, 2);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "3") == 0)
		{
			errs	= _add_board_index(i, 3);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "4") == 0)
		{
			errs	= _add_board_index(i, 4);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "5") == 0)
		{
			errs	= _add_board_index(i, 5);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "6") == 0)
		{
			errs	= _add_board_index(i, 6);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "7") == 0)
		{
			errs	= _add_board_index(i, 7);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "8") == 0)
		{
			errs	= _add_board_index(i, 8);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "9") == 0)
		{
			errs	= _add_board_index(i, 9);

			if (errs)
				break;

			continue;
		}

		if (strcmp(argv[i], "-b2") == 0)
		{
			_sync_qty	= 2;
			continue;
		}

		if (strcmp(argv[i], "-b3") == 0)
		{
			_sync_qty	= 3;
			continue;
		}

		if (strcmp(argv[i], "-b4") == 0)
		{
			_sync_qty	= 4;
			continue;
		}

		if (strcmp(argv[i], "-b5") == 0)
		{
			_sync_qty	= 5;
			continue;
		}

		if (strcmp(argv[i], "-b6") == 0)
		{
			_sync_qty	= 6;
			continue;
		}

		if (strcmp(argv[i], "-b7") == 0)
		{
			_sync_qty	= 7;
			continue;
		}

		if (strcmp(argv[i], "-b8") == 0)
		{
			_sync_qty	= 8;
			continue;
		}

		if (strcmp(argv[i], "-c") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 0;
			continue;
		}

		if (strcmp(argv[i], "-C") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 1;
			continue;
		}

		if (strcmp(argv[i], "-d") == 0)
		{
			_star	= 0;
			continue;
		}

		if (strcmp(argv[i], "-s") == 0)
		{
			_star	= 1;
			continue;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'm') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_minute_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'n') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_test_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		errs	= 1;
		printf("ERROR: invalid argument: %s\n", argv[i]);
		break;
	}

	if (errs == 0)
		errs	= _validate_args();

	return(errs);
}



//*****************************************************************************
static int _perform_tests(int errs)
{
	errs	+= multi_board_sync(_sync_qty, _board_list, _fd_list, _star, errs, &_lp);
	errs	+= validate_sync(_sync_qty, _fd_list, errs, _lp);

	return(errs);
}



/******************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
******************************************************************************/

int main(int argc, char** argv)
{
	int			errs		= 0;
	time_t		exec		= time(NULL);
	long		failures	= 0;
	long		hours;
	long		mins;
	time_t		now;
	long		passes		= 0;
	const char*	psz;
	int			ret			= EXIT_FAILURE;
	long		secs;
	struct tm*	stm;
	time_t		t_limit;
	time_t		test;
	long		tests		= 0;
	time_t		tt;

	for (;;)
	{
		gsc_label_init(27);
		test	= time(NULL);
		errs	+= _parse_args(argc, argv);

		if (errs)
			break;


		time(&tt);
		stm	= localtime(&tt);
		psz	= asctime(stm);
		gsc_label("Performing Test");
		printf("%s", psz);

		_board_qty	= dsi_count_boards();

		if (_board_qty <= 0)
			break;

		os_id_host();
		errs	+= os_id_driver(DSI_BASE_NAME);

		if (errs)
			break;

		t_limit	= exec + (_minute_limit * 60);
		service_list_init(_sync_qty, _board_list);
		errs	+= service_open(_sync_qty, _board_list, _fd_list);
		errs	+= _perform_tests(errs);
		errs	+= service_close(_sync_qty, _fd_list);
		now		= time(NULL);
		tests++;

		if (errs == 0)
			ret		= EXIT_SUCCESS;

		if (errs)
		{
			failures++;
			printf(	"\nRESULTS: FAIL <---  (%d error%s)",
					errs,
					(errs == 1) ? "" : "s");
		}
		else
		{
			passes++;
			printf("\nRESULTS: PASS");
		}

		secs	= now - test;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(" (duration %ld:%ld:%02ld)\n", hours, mins, secs);

		secs	= now - exec;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(	"SUMMARY: tests %ld, pass %ld, fail %ld"
				" (duration %ld:%ld:%02ld)\n\n",
				tests,
				passes,
				failures,
				hours,
				mins,
				secs);

		if ((_test_limit > 0) && (_test_limit <= tests))
			break;

		if (_continuous == 0)
			break;

		if ((_ignore_errors == 0) && (errs))
			break;

		if ((_minute_limit) && (now >= t_limit))
			break;
	}

	return(ret);
}


