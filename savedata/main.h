// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/savedata/main.h $
// $Rev: 34691 $
// $Date: 2016-03-18 14:40:50 -0500 (Fri, 18 Mar 2016) $

#ifndef __MAIN_H__
#define __MAIN_H__

#include "24dsi_dsl.h"
#include "24dsi_utils.h"



// prototypes *****************************************************************

int	save_data(int fd, int io_mode);



#endif
