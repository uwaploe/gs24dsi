// $URL: http://subversion:8080/svn/gsc/trunk/drivers/LINUX/24DSI/24DSI_Linux_3.x_and_4.x_GSC_DN/savedata/main.c $
// $Rev: 34691 $
// $Date: 2016-03-18 14:40:50 -0500 (Fri, 18 Mar 2016) $

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"



// variables ******************************************************************

static	int		_continuous		= 0;
static	int		_def_index		= 0;
static	int		_ignore_errors	= 0;
static	int		_io_mode		= GSC_IO_MODE_PIO;
static	long	_minute_limit	= 0;
static	int		_test_limit		= -1;



//*****************************************************************************
static int _parse_args(int argc, char** argv)
{
	char	buf[64];
	char	c;
	int		errs	= 0;
	int		i;
	int		j;
	int		k;

	printf("savedata - Capture and Save Data to Disk\n");
	printf("USAGE: savedata <-c> <-C> <-dma> <-dmdma> <-m#> <-n#> <-pio> <index>\n");
	printf("  -c      Continue testing until an error occurs.\n");
	printf("  -C      Continue testing even if errors occur.\n");
	printf("  -dma    Transfer data using DMA.\n");
	printf("  -dmdma  Transfer data using DMDMA.\n");
	printf("  -m#     Run for at most # minutes (a decimal number).\n");
	printf("  -n#     Repeat test at most # times (a decimal number).\n");
	printf("  -pio    Transfer data using PIO.\n");
	printf("  index   The zero based index of the device to access.\n");

	gsc_label("Arguments");
	printf("(%d argument%s)\n", argc - 1, ((argc - 1) > 1) ? "s" : "");
	gsc_label_level_inc();

	for (i = 0; i < argc; i++)
	{
		sprintf(buf, "Argument %d", i);
		gsc_label(buf);
		printf("%s\n", argv[i]);
	}

	gsc_label_level_dec();

	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-c") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 0;
			continue;
		}

		if (strcmp(argv[i], "-C") == 0)
		{
			_continuous		= 1;
			_ignore_errors	= 1;
			continue;
		}

		if (strcmp(argv[i], "-dma") == 0)
		{
			_io_mode	= GSC_IO_MODE_DMA;
			continue;
		}

		if (strcmp(argv[i], "-dmdma") == 0)
		{
			_io_mode	= GSC_IO_MODE_DMDMA;
			continue;
		}

		if (strcmp(argv[i], "-pio") == 0)
		{
			_io_mode	= GSC_IO_MODE_PIO;
			continue;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'm') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_minute_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		if ((argv[i][0] == '-') && (argv[i][1] == 'n') && (argv[i][2]))
		{
			j	= sscanf(&argv[i][2], "%d%c", &k, &c);

			if ((j == 1) && (k > 0))
			{
				_test_limit	= k;
				continue;
			}

			errs	= 1;
			printf("ERROR: invalid argument: %s\n", argv[i]);
			break;
		}

		j	= sscanf(argv[i], "%d%c", &k, &c);

		if ((j == 1) && (k >= 0))
		{
			_def_index	= k;
			continue;
		}
		else
		{
			errs	= 1;
			printf("ERROR: invalid board selection: %s\n", argv[i]);
			break;
		}
	}

	return(errs);
}



/******************************************************************************
*
*	Function:	_perform_tests
*
*	Purpose:
*
*		Perform the appropriate testing.
*
*	Arguments:
*
*		fd		The handle for the board to access.
*
*	Returned:
*
*		>= 0	The number of errors encounterred.
*
******************************************************************************/

static int _perform_tests(int fd)
{
	int			errs	= 0;
	const char*	psz;
	struct tm*	stm;
	time_t		tt;

	time(&tt);
	stm	= localtime(&tt);
	psz	= asctime(stm);
	gsc_label("Performing Operation");
	printf("%s", psz);

	errs	+= os_id_driver(DSI_BASE_NAME);
	errs	+= dsi_id_board(fd, -1, NULL);

	errs	+= save_data(fd, _io_mode);

	return(errs);
}



/******************************************************************************
*
*	Function:	main
*
*	Purpose:
*
*		Control the overall flow of the application.
*
*	Arguments:
*
*		argc			The number of command line arguments.
*
*		argv			The list of command line arguments.
*
*	Returned:
*
*		EXIT_SUCCESS	We tested a device.
*		EXIT_FAILURE	We didn't test a device.
*
******************************************************************************/

int main(int argc, char *argv[])
{
	int		errs;
	time_t	exec		= time(NULL);
	long	failures	= 0;
	int		fd			= 0;
	long	hours;
	long	mins;
	time_t	now;
	long	passes		= 0;
	int		qty;
	int		ret			= EXIT_FAILURE;
	long	secs;
	time_t	t_limit;
	time_t	test;
	long	tests		= 0;

	for (;;)
	{
		gsc_label_init(26);
		test	= time(NULL);
		errs	= _parse_args(argc, argv);

		if (errs)
			break;

		os_id_host();
		t_limit	= exec + (_minute_limit * 60);
		qty		= dsi_count_boards();

		if (qty <= 0)
			break;

		gsc_label("Accessing Board Index");
		printf("%d\n", _def_index);
		fd	= gsc_dev_open(_def_index, DSI_BASE_NAME);

		if (fd == -1)
		{
			errs	= 1;
			printf(	"ERROR: Unable to access device %d.", _def_index);
		}

		if (errs == 0)
		{
			ret		= EXIT_SUCCESS;
			errs	= _perform_tests(fd);
		}

		gsc_dev_close(_def_index, fd);

		now	= time(NULL);
		tests++;

		if (errs)
		{
			failures++;
			printf(	"\nRESULTS: FAIL <---  (%d error%s)",
					errs,
					(errs == 1) ? "" : "s");
		}
		else
		{
			passes++;
			printf("\nRESULTS: PASS");
		}

		secs	= now - test;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(" (duration %ld:%ld:%02ld)\n", hours, mins, secs);

		secs	= now - exec;
		hours	= secs / 3600;
		secs	= secs % 3600;
		mins	= secs / 60;
		secs	= secs % 60;
		printf(	"SUMMARY: tests %ld, pass %ld, fail %ld"
				" (duration %ld:%ld:%02ld)\n\n",
				tests,
				passes,
				failures,
				hours,
				mins,
				secs);

		if ((_test_limit > 0) && (_test_limit <= tests))
			break;

		if (_continuous == 0)
			break;

		if ((_ignore_errors == 0) && (errs))
			break;

		if ((_minute_limit) && (now >= t_limit))
			break;
	}

	return(ret);
}



